from sys import argv
from objectManipulator import *
from node import *
from gamestats import *
import random
from graph import *
import math


MAX_HEIGHT_PULL_UP = 320
MAX_HEIGHT_WALL_SCALE = 480
DAMAGE = 540 
DEATH = 1000
WALL_RUN = 1000
JUMP_DISTANCE_NO_RUN = 400
JUMP_DISTANCE_RUN = 550
MAX_JUMP_DISTANCE_FALL = 1100

class LevelGenerator(object):
    
    def __init__(self):
        self.traversals = {0 : "cubeJump" , 1: "vault" , 2: "wallRun" , 3: "slide"}

        #define the origin. Perhaps based on command line args / config file?
        #initalize state. This includes fields like the probability vector for which traversal to do for a given node type, maxJump, maxClimb, etc.  

        #prob,  maxHJ, maxVJ, maxFall, maxWallRun
        self.stats = GameStats( (1,0,0),  10,   10,    10,      10)

        #height, location, size, objType(type of treversal),headings, parent, root,  end?
        self.origin = Node(10,   (0,0,0),  [10,10,10],    0,   [0],    self,  self, False)

        self.graph = Graph()
        self.graph.add(self.origin)

        self.generateLevel()


    def generateLevel(self):
        for heading in xrange(len(self.origin.headings)):
            print heading
            self.makePath(self.origin, heading)



    #makePath, starting at a node, and with the index of the heading of that node
    def makePath(self, curr, headIndex):
            
        #pick which traversal type
        #traversalType = random.randint(0,len(self.traversals))  #default = 0 = cube jump
        traversalType = 1
        #random variance based on type? This is in radians // PLAY WITH THIS!
        var = 0.5
        
        # traversal type 
        if traversalType == 0:         
            # if the traversal type is a jump cube 
            curr = self.cubeJump(curr, curr.headings[headIndex], var) 
        elif traversalType == 1:
            # if the traversal type is a vault cube 
            curr = self.vaultJump(curr,curr.headings[headIndex], var)
        elif traversalType == 2: 
            # if the traversal type is a wall run 
            #curr = self.wallRun(curr, curr.headings[headIndex], var)
            pass 
        elif traversalType == 3: 
            # if the traversal type is a slide 
            #curr = self.slide(curr, curr.headings[headIndex], var)
            pass 
            
        # build path until you reach certain length 
        if (not curr.end):
            for heading in xrange(len(curr.headings)):
                self.makePath(curr, heading)
        else:
            return


    def cubeJump(self, curr, heading, var):
        # when the traversal type is a cube jump
        result = self.reachableCube(curr, heading, var)
        self.graph.add(result)
        return result

    def swingToCube(self, curr, heading, var):
        # when the traversal type is a swing 
        swing = makeSwing(curr, heading, var)
        self.graph.add(swing)
        result = makeSwingCube(curr, swing, heading, var)
        self.graph.add(result)
        return result

    def vaultJump(self,curr,heading,var):
        # vault jump 

        #random dp (change in position) in range 1 to self.stats.maxHJump // PLAY WITH THIS!
        dp = random.uniform(1,self.stats.maxHJump)

        #random dy in a range defined as f(dp) // PLAY WITH THIS!
        dy = random.uniform(-2, dp/3.0)

        #heading is perturbed by some random function of var // PLAY WITH THIS!
        finalHeading = heading + random.uniform(0, var)

        # calculate the displacement given the parameters above
        disp = (dp*math.sin(finalHeading),dy,dp*math.cos(finalHeading))
        finalLoc = [curr.location[i] + disp[i] for i in xrange(3)]


        result = Node(curr.height + dy, finalLoc, curr.size, 0, [finalHeading], curr, self.origin, random.random() > .80)
        
        self.graph.add(result)
        return result

            
    def reachableCube(self, curr, heading, var):

        #random dp (change in position) in range 1 to self.stats.maxHJump // PLAY WITH THIS!
        dp = random.uniform(1,self.stats.maxHJump)

        #random dy in a range defined as f(dp) // PLAY WITH THIS!
        dy = random.uniform(-2, dp/3.0)

        #heading is perturbed by some random function of var // PLAY WITH THIS!
        finalHeading = heading + random.uniform(0, var)

        # calculate the displacement given the parameters above
        disp = (dp*math.sin(finalHeading),dy,dp*math.cos(finalHeading))
        finalLoc = [curr.location[i] + disp[i] for i in xrange(3)]

        res = Node(curr.height + dy, finalLoc, curr.size, 0, [finalHeading], curr, self.origin, random.random() > .80)
        return res

    def makeSwing(self, curr, heading, var):
        #swing bar is some small distance forward. perhaps will have to add reference frame from curr // PLAY WITH THIS!
        dp = random.uniform(0, 3)
        #How high do you have to jump to get to the bar? Could be random dy in a range defined as f(dp)? // PLAY WITH THIS!
        dy = random.uniform(self.stats.maxVJump / 2, self.stats.maxVJump)

        # calculate the displacement and final location given the parameters above
        disp = (dp*math.sin(heading),dy,dp*math.cos(heading))
        finalLoc = [curr.location[i] + disp[i] for i in xrange(3)]

        res = Node(curr.height + dy, finalLoc, curr.size, SWING, [heading], curr, self.origin, False)

        return res
    
    def makeSwingCube(self, curr, swing, heading, var):
        return res


    def to_obj(self):
        return self.graph.to_obj()



def main():
    meow = LevelGenerator()
    """while meow.graph.hasNext():
        print meow.graph.next_vert()
    meow.graph.reset_iter()"""
    print "size of graph: " + str(meow.graph.size())
    meow.to_obj().write("test.obj")
    print "done!"
    return

if __name__ == "__main__":
    main()
