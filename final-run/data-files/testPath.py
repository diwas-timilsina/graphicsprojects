import math
from building import *
GRID_SIZE = 10

#Code that tests whether a building can connect with another building

#Takes in a list of buildings
#Takes a world parameter (a three tuple bounding box of the world) 
#MUST NOT HAVE OVERLAPPING BUILDINGS!!!!
def generateGrid(buildings,world):
    grid = [] #A list of lists of lists
    curX = 0
    curY = 0
    curZ = 0
    while (curX <= world[0]):
        curX += GRID_SIZE
        levelX = []
        while (curY <= world[1]):
            curY += GRID_SIZE
            levelY = []
            while (curZ <= world[2]):
                curZ += GRID_SIZE
                val = False
                #Here we have an x, y, and z
                #Test if it is in any of our buildings
                for building in buildings:
                    if buildContains(curX,curY,curZ,building):
                        val = building
                levelY.append(val)
            curZ = 0
            levelX.append(levelY)
        curY = 0
        grid.append(levelX)
    return grid

#Takes in an x, y and z and returns the name of the building between them or False
def occupy(x,y,z,grid):
    gridX = int(x / GRID_SIZE)
    gridY = int(y / GRID_SIZE)
    gridZ = int(z / GRID_SIZE)

    try:
        return grid[gridX][gridY][gridZ]
    except:
        return False

#Takes in an x, y, and z and a building and returns if the building contains this point
#Assumes buildings have location parameter
# a length parameter
# a width parameter
# a height parameter
def buildContains(x,y,z,building):
    return (x in range(building.location[0] - (building.bounds[0]/2),building.location[0] + (building.bounds[0]/2))) and (y in range(building.location[1] - (building.bounds[1]/2),building.location[1] + (building.bounds[1]/2))) and (z in range(building.location[2] - (building.bounds[2]/2),building.location[2] + (building.bounds[2]/2)))

#Takes in two locations and returns whether a straight path can be drawn between them
#Buildings must have a location parameter (a three tuple point)
#Buildings must also have a name parameter (a unique identifer)
def tracePath(build1, build2, step, grid):
    if build1 == build2:
        return False

    dist = math.sqrt( (build1.location[0] - build2.location[0])**2 + 
                      (build1.location[1] - build2.location[1])**2 + 
                      (build1.location[2] - build2.location[2])**2)
    traveled = 0
    while traveled < dist:
        traveled += step
        x = build1.location[0] + (traveled/dist) * (build2.location[0] - build1.location[0])
        y = build1.location[1] + (traveled/dist) * (build2.location[1] - build1.location[1])
        z = build1.location[2] + (traveled/dist) * (build2.location[2] - build1.location[2])
        spotInfo = occupy(x,y,z, grid)
        if (spotInfo and spotInfo != build1 and spotInfo != build2): 
            return False
    return True
            


def makeTestBuildingsInLine(num):
    buildings = []
    for i in range(num):
        buildings.append( Building((50, 50 , 100*i),0,(100,100,100)) )
    return buildings

def computeNeighbors(buildings, step, grid):
    for build1 in buildings:
        for build2 in buildings:
            if tracePath(build1, build2, 10, grid): # step size of 10?... Make this dynamic.
                build1.neighbors.append(build2)    


def main():
    buildings = makeTestBuildingsInLine(5)
    grid = generateGrid(buildings, (100,100,100))
    computeNeighbors(buildings, 10, grid)
    for building in buildings:
        print building

if __name__ == "__main__":
    main()


