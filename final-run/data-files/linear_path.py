# Actual class to make path between two points 

from objectManipulator import *
from random import *
from lib import map 


class MakePath(object):
    
    def __init__(self, (x0,y0,z0), (x1,y1,z1), map, obj):
        # takes in origin, end and the map as parameter
       # self.(x0,y0,z0) = (x0,y0,z0) 
        #self.(x1,y1,z1) = (x1,y1,z1)
        self.m = map
        self.obj = obj
        
        obj2 = OBJ()
        obj2.load("Cube.obj","Cube.mtl")
        
        obj3 = OBJ()
        obj3.load("Cube.obj","Cube.mtl")
        
        # vary x or y 
        vary_x = True
        if (x1-x0) < 10:
            vary_x = False

        # constants 
        # no need to put anything in between points if the distance is less that 2
        max_climb = 3.2
        max_x = 6.4
        max_jump = 4.5  # this max is in horizontal direction
        max_height = 10 # maximum heights of the cubes
        max_length = 4 # maximum length of the cubes
        max_width = 4  # maximum width of the cubes

        l = uniform(3,max_length) 
        w = uniform(3,max_width)

        print "width = " + str(w)

        h = uniform(2,max_height)

        cubes = []
        cubes.append([0,0,0,l,w,h])
        
        if vary_x:
            
            print "I am in!"
            # the case when there is not much variance in y
            #print cubes
            while ((x1-x0)>2 and (z1-z0)>2):    
    
                distance = random()*3+5
                z = random()*max_climb 
                x = random()*(distance-1)+2
                y = uniform(-w,w)
    
                #while (x > max_jump + 2) :
                while ((abs((x1-x0+x)*3)>abs(z1-z0+z)) and x > max_jump + 2 ): 
                    print str(x0) + " " + str(z0)
                    distance = random()*3+5
                    z = random()*max_climb
                    #   x = random()*(distance-1)+1
                    x = uniform(random()*(distance-1)+2,4)
                    y = uniform(-w,w)
    
   
                l = uniform(3,max_length)
                w = uniform(3,max_width)
                h = uniform(2,max_height)
                x += l
                
                x0+=x
                y0+=y
                z0+=z
    
                cubes.append([x0,y0,z0,l,w,h])
                #print cubes[-1]
        
            # go back to the previously created cubes and see if we can squeeze any more 
            # treversals 
            # new list of cubes
            new_cubes = []

            new_cubes.append(cubes[0])
            self.obj.append(obj2.scale3(cubes[0][3],cubes[0][4],cubes[0][5]).translate(cubes[0][0],cubes[0][1],cubes[0][2]))
            self.m.addCube(100*cubes[0][3],100*cubes[0][4],100*cubes[0][5],100*cubes[0][0],100*cubes[0][1],100*cubes[0][2])

            for i in range(len(cubes)-1):
                current_cube = cubes[i]
                next_cube = cubes[i+1]
            
                new_cubes.append(next_cube)
                self.obj.append(obj2.scale3(next_cube[3],next_cube[4],next_cube[5]).translate(next_cube[0],next_cube[1],next_cube[2]))
                self.m.addCube(100*next_cube[3],100*next_cube[4],100*next_cube[5],100*next_cube[0],100*next_cube[1],100*next_cube[2])

                # randomly add some vault 
                if (uniform(0,1) < 0.1):
                    print "vault cube " + str(i)
                    vault_cube = [current_cube[0],current_cube[1],current_cube[2]+current_cube[5]/2+uniform(2,max_climb),current_cube[3]/3,current_cube[4], current_cube[5]/3]
                    new_cubes.append(vault_cube)
                    self.obj.append(obj2.scale3(vault_cube[3],vault_cube[4],vault_cube[5]).translate(vault_cube[0],vault_cube[1],vault_cube[2]))
                    self.m.addCube(100*vault_cube[3],100*vault_cube[4],100*vault_cube[5],100*vault_cube[0],100*vault_cube[1],100*vault_cube[2])

                # if x distance is more than maximum you can jump then you need to place a primitive
                # if z distance is more that the vertical height that you can jump, then you also need to 
                # place a primitive 
                # we don't really care abot the y direction 
            
                # how different are two consequetive cubes
                dx = next_cube[0]-next_cube[3]/2 - (current_cube[0]+current_cube[3]/2) 
                dz = (next_cube[2]+next_cube[5]/2) - (current_cube[2]+current_cube[5]/2)

                # if dx > max_jump_distance then 
                # if dz < some_small_value and  
                # then wall run or swing 
                # otherwise just put a cube that acts as a spring in the middle

                if dx > (max_jump):
                    if abs(dz) < max_climb/2:

                        if (uniform(0,1) < 0.4):
                            # wall run 

                            x = (next_cube[0]+current_cube[0])/2

                            # only placing wall run on one side
                            # take min to place on the other side
                            y = (max(next_cube[1],current_cube[1]) + max(next_cube[4]/2,current_cube[4]/2) + 1,
                                 max(next_cube[1],current_cube[1]) - max(next_cube[4]/2,current_cube[4]/2) - 1 )[randrange(0,2)==0]

                            z = max(next_cube[2],current_cube[2]) + max(next_cube[5]/2,current_cube[5]/2)

                            l = dx * 2
                            w = 0.25 
                            h = max(next_cube[5],current_cube[5]) +3

                            print "append  wall " + str(i)
                            new_cubes.append([x,y,z,l,w,h])
                     
                            self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                            self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                        else:
                            # swing  
                            x = (current_cube[0]+next_cube[0])/2
                            y = current_cube[1]
                            z = current_cube[2]+ current_cube[5]/2 + max_jump -0.75

                            l = 0.25
                            w = (current_cube[4]+next_cube[5])/2
                            h = 0.25

                            print "append siwng " + str(i)
                            new_cubes.append([x,y,z,l,w,h])

                            self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                            self.m.addSwing(100*x,100*(y-w/2),100*z,100*x,100*(y+w/2),100*z)

                    else:
                        # place a cube jump(maybe a spring cube...)
                        x = (next_cube[0]+current_cube[0])/2
                        y = (next_cube[1]+current_cube[1])/2
                        z = (next_cube[2]+current_cube[2])/2
                        l = (next_cube[3]+current_cube[3])/2
                        w = (next_cube[4]+current_cube[4])/2
                        h = (next_cube[5]+current_cube[5])/2

                        print "append s cube " + str(i)
                        new_cubes.append([x,y,z,l,w,h])

                        self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                        self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                if abs(dz) > max_climb:
                    # place a cube jump(maybe a spring cube...)
                    x = (next_cube[0]+current_cube[0])/2
                    y = (next_cube[1]+current_cube[1])/2
                    z = (next_cube[2]+current_cube[2])/2
                    l = (next_cube[3]+current_cube[3])/2
                    w = (next_cube[4]+current_cube[4])/2
                    h = (next_cube[5]+current_cube[5])/2
 
                    print "append s cube " + str(i)
                    new_cubes.append([x,y,z,l,w,h])
                    self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                    self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                    # cube vault 
                    z = z + h/2 + uniform(3,max_climb)
                    l = l/3
                    w = w
                    h = h/3
 
                    print "append vault cube " + str(i)
                    new_cubes.append([x,y,z,l,w,h])
                    self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                    self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)
                    #   obj.append(obj2.scale3(a[3],a[4],a[5]).translate(a[0],a[1],a[2]))

        else: 
            print "I am in too!"
            # if there is not much variance in x 
            #print cubes
            while ((y1-y0)>2 and (z1-z0)>2):    
    
                distance = random()*3+5
                z = random()*max_climb 
                y = random()*(distance-1)+2
                x = uniform(-w,w)
    
                while ((abs((y1-y0+y)*3)<abs(z1-z0+z)) and x > max_jump + 2) : 
                    print str(x0) + " " + str(z0)
                    distance = random()*3+5
                    z = random()*max_climb
                    #   x = random()*(distance-1)+1
                    y = uniform(random()*(distance-1)+2,4)
                    x = uniform(-w,w)
    
   
                w = uniform(3,max_length)
                l = uniform(3,max_width)
                h = uniform(2,max_height)
                y += w
                
                x0+=x
                y0+=y
                z0+=z
    
                cubes.append([x0,y0,z0,l,w,h])
                # print cubes
     
            # go back to the previously created cubes and see if we can squeeze any more 
            # treversals 
            # new list of cubes
            new_cubes = []

            new_cubes.append(cubes[0])
            self.obj.append(obj2.scale3(cubes[0][3],cubes[0][4],cubes[0][5]).translate(cubes[0][0],cubes[0][1],cubes[0][2]))
            self.m.addCube(100*cubes[0][3],100*cubes[0][4],100*cubes[0][5],100*cubes[0][0],100*cubes[0][1],100*cubes[0][2])

            for i in range(len(cubes)-1):
                current_cube = cubes[i]
                next_cube = cubes[i+1]
            
                new_cubes.append(next_cube)
                self.obj.append(obj2.scale3(next_cube[3],next_cube[4],next_cube[5]).translate(next_cube[0],next_cube[1],next_cube[2]))
                self.m.addCube(100*next_cube[3],100*next_cube[4],100*next_cube[5],100*next_cube[0],100*next_cube[1],100*next_cube[2])

                # randomly add some vault 
                if (uniform(0,1) < 0.1):
                    #print "vault cube " + str(i)
                    vault_cube = [current_cube[0],current_cube[1],current_cube[2]+current_cube[5]/2+uniform(2,max_climb),current_cube[3]/3,current_cube[4], current_cube[5]/3]
                    new_cubes.append(vault_cube)
                    self.obj.append(obj2.scale3(vault_cube[3],vault_cube[4],vault_cube[5]).translate(vault_cube[0],vault_cube[1],vault_cube[2]))
                    self.m.addCube(100*vault_cube[3],100*vault_cube[4],100*vault_cube[5],100*vault_cube[0],100*vault_cube[1],100*vault_cube[2])

                # if x distance is more than maximum you can jump then you need to place a primitive
                # if z distance is more that the vertical height that you can jump, then you also need to 
                # place a primitive 
                # we don't really care abot the y direction 
            
                # how different are two consequetive cubes
                dy = next_cube[1]-next_cube[3]/2 - (current_cube[1]+current_cube[3]/2) 
                dz = (next_cube[2]+next_cube[5]/2) - (current_cube[2]+current_cube[5]/2)

                # if dx > max_jump_distance then 
                # if dz < some_small_value and  
                # then wall run or swing 
                # otherwise just put a cube that acts as a spring in the middle
                
                print "dy = " + str(dy)
                print "max_jump = " + str(max_jump)
                if dy > (max_jump):
                    print "in"
                    if abs(dz) < max_climb/2:

                        if (uniform(0,1) < 0.4):
                            # wall run 

                            y = (next_cube[1]+current_cube[1])/2

                            # only placing wall run on one side
                            # take min to place on the other side
                            x = (max(next_cube[0],current_cube[0]) + max(next_cube[4]/2,current_cube[4]/2) + 1,
                                 max(next_cube[0],current_cube[0]) - max(next_cube[4]/2,current_cube[4]/2) - 1 )[randrange(0,2)==0]

                            z = max(next_cube[2],current_cube[2]) + max(next_cube[5]/2,current_cube[5]/2)

                            w = dy * 2
                            l = 0.25 
                            h = max(next_cube[5],current_cube[5]) +3

                            #print "append  wall " + str(i)
                            new_cubes.append([x,y,z,l,w,h])
                     
                            self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                            self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                        else:
                            # swing  
                            y = (current_cube[1]+next_cube[1])/2
                            x = current_cube[0]
                            z = current_cube[2]+ current_cube[5]/2 + max_jump -0.75

                            w = 0.25
                            l = (current_cube[4]+next_cube[5])/2
                            h = 0.25

                            #print "append siwng " + str(i)
                            new_cubes.append([x,y,z,l,w,h])

                            self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                            self.m.addSwing(100*(x-l/2),100*y,100*z,100*(x+l/2),100*y,100*z)

                    else:
                        # place a cube jump(maybe a spring cube...)
                        x = (next_cube[0]+current_cube[0])/2
                        y = (next_cube[1]+current_cube[1])/2
                        z = (next_cube[2]+current_cube[2])/2
                        w = (next_cube[3]+current_cube[3])/2
                        l = (next_cube[4]+current_cube[4])/2
                        h = (next_cube[5]+current_cube[5])/2

                        #print "append s cube " + str(i)
                        new_cubes.append([x,y,z,l,w,h])

                        self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                        self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                if abs(dz) > max_climb:
                    # place a cube jump(maybe a spring cube...)
                    x = (next_cube[0]+current_cube[0])/2
                    y = (next_cube[1]+current_cube[1])/2
                    z = (next_cube[2]+current_cube[2])/2
                    w = (next_cube[3]+current_cube[3])/2
                    l = (next_cube[4]+current_cube[4])/2
                    h = (next_cube[5]+current_cube[5])/2
 
                    #print "append s cube " + str(i)
                    new_cubes.append([x,y,z,l,w,h])
                    self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                    self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

                    # cube vault 
                    z = z + h/2 + uniform(3,max_climb)
                    w = w/3
                    l = l
                    h = h/3
 
                    #print "append vault cube " + str(i)
                    new_cubes.append([x,y,z,l,w,h])
                    self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                    self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)
                    #   obj.append(obj2.scale3(a[3],a[4],a[5]).translate(a[0],a[1],a[2]))
    
            
    def return_map(self):                
        # return the map 
        return self.m
    
    def return_obj(self):
        # return the object
        return self.obj
        
if __name__ == "__main__":
    
    m = map.Map()
    obj = OBJ()
    gen = MakePath((0,0,0),(0,500,500),m,obj)
    
    #m = gen.return_map()
    #obj = gen.return_obj()
    
    m.save("test.mir")
    m.writeT3D("test.t3d")
    obj.write("test.obj")

