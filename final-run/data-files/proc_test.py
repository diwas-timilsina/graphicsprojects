# this class is used to define a path between two points 

from objectManipulator import *
from random import *
#from Map import * 
from lib import map

# starting 
(x0,y0,z0) = (0,0,0)

# end point
(x1,y1,z1) = (100,50,50)

obj = OBJ()

# map for t3d 
m = map.Map()


obj2 = OBJ()
obj2.load("../data-files/Cube.obj","../data-files/Cube.mtl")

obj3 = OBJ()
obj3.load("../data-files/Cube2.obj","../data-files/Cube2.mtl")


# constants 
# no need to put anything in between points if the distance is less that 2
max_climb = 3.2
max_x = 6.4
max_jump = 4  # this max is in horizontal direction
max_height = 10 # maximum heights of the cubes
max_length = 4 # maximum length of the cubes
max_width = 4  # maximum width of the cubes


l = uniform(3,max_length) 
w = uniform(3,max_width)

print "width = " + str(w)

h = uniform(2,max_height)

cubes = []
cubes.append([0,0,0,l,w,h])
 
#print cubes
while ((x1-x0)>2 and (z1-z0)>2):    
    
    distance = random()*3+5
    z = random()*max_climb 
    x = random()*(distance-1)+1
    y = uniform(-w/2,w/2)
    
    while ((abs((x1-x0+x)*3)<abs(z1-z0+z)) and x > max_jump) : 
        print str(x0) + " " + str(z0)
        distance = random()*3+5
        z = random()*max_climb
        #   x = random()*(distance-1)+1
        x = uniform(random()*(distance-1)+1,4)
        y = uniform(-w/2,w/2)
    
   
    l = uniform(3,max_length)
    w = uniform(3,max_width)
    h = uniform(2,max_height)
    x += l
    
    x0+=x
    y0+=y
    z0+=z
    
    cubes.append([x0,y0,z0,l,w,h])
   # print cubes

# go back to the previously created cubes and see if we can squeeze any more 
# treversals 
# new list of cubes
new_cubes = []

new_cubes.append(cubes[0])
obj.append(obj2.scale3(cubes[0][3],cubes[0][4],cubes[0][5]).translate(cubes[0][0],cubes[0][1],cubes[0][2]))
m.addCube(100*cubes[0][3],100*cubes[0][4],100*cubes[0][5],100*cubes[0][0],100*cubes[0][1],100*cubes[0][2])

for i in range(len(cubes)-1):
    current_cube = cubes[i]
    next_cube = cubes[i+1]
    
    new_cubes.append(next_cube)
    obj.append(obj2.scale3(next_cube[3],next_cube[4],next_cube[5]).translate(next_cube[0],next_cube[1],next_cube[2]))
    m.addCube(100*next_cube[3],100*next_cube[4],100*next_cube[5],100*next_cube[0],100*next_cube[1],100*next_cube[2])

    # randomly add some vault 
    if (uniform(0,1) > 0.8):
        print "vault cube " + str(i)
        vault_cube = [current_cube[0],current_cube[1],current_cube[2]+current_cube[5]/2+uniform(2,max_climb),current_cube[3]/3,current_cube[4], current_cube[5]/3]
        new_cubes.append(vault_cube)
        obj.append(obj2.scale3(vault_cube[3],vault_cube[4],vault_cube[5]).translate(vault_cube[0],vault_cube[1],vault_cube[2]))
        m.addCube(100*vault_cube[3],100*vault_cube[4],100*vault_cube[5],100*vault_cube[0],100*vault_cube[1],100*vault_cube[2])

    # if x distance is more than maximum you can jump then you need to place a primitive
    # if z distance is more that the vertical height that you can jump, then you also need to 
    # place a primitive 
    # we don't really care abot the y direction 
    
    # how different are two consequetive cubes
    dx = next_cube[0]-next_cube[3]/2 - (current_cube[0]+current_cube[3]/2) 
    dz = (next_cube[2]+next_cube[5]/2) - (current_cube[2]+current_cube[5]/2)
    
    # if dx > max_jump_distance then 
       # if dz < some_small_value and  
       # then wall run or swing 
       # otherwise just put a cube that acts as a spring in the middle
    
    if dx > max_jump:
        if abs(dz) < max_climb/2:
            
            if (uniform(0,1) < 0.4):
                # wall run 

                x = (next_cube[0]+current_cube[0])/2
            
                # only placing wall run on one side
                # take min to place on the other side
                y = (max(next_cube[1],current_cube[1]) + max(next_cube[4]/2,current_cube[4]/2) + 1,
                     max(next_cube[1],current_cube[1]) - max(next_cube[4]/2,current_cube[4]/2) - 1 )[randrange(0,2)==0]
                 
                z = max(next_cube[2],current_cube[2]) + max(next_cube[5]/2,current_cube[5]/2)
             
                l = dx * 2
                w = 0.25 
                h = max(next_cube[5],current_cube[5]) +3

                print "append  wall " + str(i)
                new_cubes.append([x,y,z,l,w,h])
                
                obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

            else:
                # swing  
                x = (current_cube[0]+next_cube[0])/2
                y = current_cube[1]
                z = current_cube[2]+ current_cube[5]/2 + max_jump-0.5 
                
                l = 0.25
                w = (current_cube[4]+next_cube[5])/2
                h = 0.25
                
                print "append siwng " + str(i)
                new_cubes.append([x,y,z,l,w,h])
                
                obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                m.addSwing(100*x,100*(y-w/2),100*z,100*x,100*(y+w/2),100*z)

        else:
            # place a cube jump(maybe a spring cube...)
            x = (next_cube[0]+current_cube[0])/2
            y = (next_cube[1]+current_cube[1])/2
            z = (next_cube[2]+current_cube[2])/2
            l = (next_cube[3]+current_cube[3])/2
            w = (next_cube[4]+current_cube[4])/2
            h = (next_cube[5]+current_cube[5])/2
 
            print "append s cube " + str(i)
            new_cubes.append([x,y,z,l,w,h])

            obj.append(obj3.scale3(l,w,h).translate(x,y,z))
            m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

    if abs(dz) > max_climb:
        # place a cube jump(maybe a spring cube...)
        x = (next_cube[0]+current_cube[0])/2
        y = (next_cube[1]+current_cube[1])/2
        z = (next_cube[2]+current_cube[2])/2
        l = (next_cube[3]+current_cube[3])/2
        w = (next_cube[4]+current_cube[4])/2
        h = (next_cube[5]+current_cube[5])/2
 
        print "append s cube " + str(i)
        new_cubes.append([x,y,z,l,w,h])
        obj.append(obj3.scale3(l,w,h).translate(x,y,z))
        m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)

        # cube vault 
        z = z + h/2 + uniform(0,max_climb-1)
        l = l/3
        w = w
        h = h/3
 
        print "append vault cube " + str(i)
        new_cubes.append([x,y,z,l,w,h])
        obj.append(obj3.scale3(l,w,h).translate(x,y,z))
        m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z)
         
        #   obj.append(obj2.scale3(a[3],a[4],a[5]).translate(a[0],a[1],a[2]))

"""
# here create a t3d file for the cubes
m = map.Map()
for a in new_cubes:
    print a
    m.addCube(100*a[3],100*a[4],100*a[5],100*a[0],100*a[1],100*a[2])
    obj.append(obj2.scale3(a[3],a[4],a[5]).translate(a[0],a[1],a[2]))
    #obj.append(obj2.translate(a[0],a[1],a[2]))
"""
m.save("test.mir")
m.writeT3D("test.t3d")
    
#obj.scale_self(100)
obj.write("test.obj")
