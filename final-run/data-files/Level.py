# Level class
import Queue
import random
import math
from objectManipulator import *
from traversal import *
from building import *
from face import *

MAXIMUM_TRAVERSALS = 100

class Level(object):

    def __init__(self, buildings):
        self.buildings     = buildings
        self.extraElements = None
        self.finishTraversalGraph()

    def __str__(self):
        s = ""
        for build in self.buildings:
            s += str(build) + "\n"
            s += "\t"
            for face in build.faces:
                for trav in face.outs:
                    s += str(trav)
            s = s[:-1]
        return s

    def finishTraversalGraph(self):
        self.determinePossibleTraversals()
        self.defineTraversals()


    def defineTraversals(self):

        count = 0
        MAX_TRAVERSALS = 50 # ?????____________ what should we make this? or how to implement levels

        startBuilding = self.buildings[random.randint(0, len(self.buildings)-1)]
        x0 = startBuilding.location[0]
        y0 = startBuilding.location[1]
        z0 = startBuilding.location[2]

        queue = Queue.Queue()
        #our queue will be a 2-list of the face and the startPos on it.
        queue.put(  [startBuilding.faces[0], (x0, y0+(startBuilding.bounds[1]/2), z0 )]    ) 

        while (count < MAX_TRAVERSALS  and not queue.empty() ):
            cur = queue.get()
            curFace  = cur[0]
            startPos = cur[1]
            while len(curFace.possibleTraversals) > 0:
                #pick random traversal                
                traversal = curFace.possibleTraversals[random.randint(0, len(curFace.possibleTraversals) -1)]
                traversal.addStartAndEndPoints(startPos)
                curFace.outs.append(traversal)
                count = count + 1
                self.trimTraversals(traversal)
                queue.put(   [traversal.endFace, traversal.endPos]  ) #put the endpos in this. Will be startPos of next when it gets popped.
        if queue.empty():
            print "________ done defining traversals (empty queue) _____\n"            
        else:
            print "________ done defining traversals (max num, " + str(MAX_TRAVERSALS) + "_________\n"

        return

    '''
    def chooseTraversals(self, startFace):
        #print "" + str(startFace.building.location) + " face# = " + str(startFace.faceType) + " " + str(startFace.possibleTraversals)

        if not len(startFace.possibleTraversals) == 0:
            #return startFace.possibleTraversals[0]
            return startFace.possibleTraversals(random.randint(0,len(startFace.possibleTraversals)-1))

        # WE SHOULD NEVER GET HERE
        return Traversal(startFace,startFace, -1)
    '''
        
        #IMPLEMENT ME. RETURN DEGREES
    #dz/dx implemented here. Maybe backwards?_________________________
    def directionSlope(self, loc, loc2):
        try:

            if -.001 < (loc2[2] - loc[2]) < .001:
                return (0.5,-0.5)[loc2[0] < loc[0]]
            return float(loc2[2] - loc[2])/abs(float(loc2[0] -loc[0])) 
        except:
            #If we have an infinite slope, just use +/-, North or south
            return (1.5,-1.5)[loc2[2] < loc[2]]

    def determinePossibleTraversals(self):
        numB = 0
        for building in self.buildings:

            for faceType in range(1,len(building.faces)):
                building.faces[0].possibleTraversals.append(Traversal(building.faces[0],building.faces[faceType],4))
                
            
            for neighbor in building.neighbors:
                dirSlope = self.directionSlope(building.location, neighbor.location)
                # for each one, we also add the good roof to side traversal.
                #if slope is valid, and north face --> make a traversal to neighbor's south face
                if dirSlope > 1:
                    #print "made north to south " + str(building.location) + " " + str(neighbor.location)
                    building.faces[1].possibleTraversals.append(Traversal(building.faces[1],neighbor.faces[2],1))
                #if slope is valid, and south face --> south to north
                elif dirSlope < -1:
                    #print "made south to north" + str(building.location) + " " + str(neighbor.location)
                    building.faces[2].possibleTraversals.append(Traversal(building.faces[2],neighbor.faces[1],1))
                    building.faces[0].possibleTraversals.append(Traversal(building.faces[2],neighbor.faces[1],3))
                #if slope is valid, and east face --> make a traversal to neighbor's west face
                elif dirSlope > 0 and dirSlope != 1:
                    #print "made east to west" + str(building.location) + " " + str(neighbor.location)
                    building.faces[3].possibleTraversals.append(Traversal(building.faces[3],neighbor.faces[4],1))
                    building.faces[0].possibleTraversals.append(Traversal(building.faces[2],neighbor.faces[4],3))
                #if slope is valid, and west face --> make a traversal to neighbor's east face
                elif dirSlope < 0 and dirSlope != -1:
                    #print "made west to east" + str(building.location) + " " + str(neighbor.location)
                    building.faces[4].possibleTraversals.append(Traversal(building.faces[4],neighbor.faces[3],1))
                    building.faces[0].possibleTraversals.append(Traversal(building.faces[2],neighbor.faces[3],3))

                #now do stuff explicitly for roofs. If the roof of the neighbor is lower, add the possible roof to roof.
                if neighbor.location[1] < building.location[1]:
                    building.faces[0].possibleTraversals.append(Traversal(building.faces[0],neighbor.faces[0],2))
        print "_________ done determining possible traversals _________\n"

    # We should no longer explicitly generate moves to a side from a different face.
    def trimTraversals(self, input_traversal):
        #if Building to other building
        #   DONE -> then remove all other traversals between the buildings.
        #   and remove any traversal to the end face (if it's not a roof.)
        #if Building to self.
        #   if roof to side, remove all other traversals into that side. Remove side to roof.
        #   if side to roof, remove the self roof to side. 
        
        #always remove self
        try:
            input_traversal.startFace.possibleTraversals.remove(input_traversal)
        except:
            pass
        
        if not input_traversal.startFace.building == input_traversal.endFace.building:
            #Deleting all other traversals between these two buildings
            startBuilding = input_traversal.startFace.building
            endBuilding = input_traversal.endFace.building

            for face in startBuilding.faces:
                removeThese = []
                for trav in face.possibleTraversals:
                    #If this traversal involves both faces
                    if (trav.startFace.building == startBuilding and trav.endFace.building == endBuilding) or (trav.startFace.building == endBuilding and trav.endFace.building == startBuilding):
                        removeThese.append(trav)
                for trav in removeThese:
                    try:
                        face.possibleTraversals.remove(trav)
                    except:
                        pass

            for face in endBuilding.faces:
                removeThese = []
                for trav in face.possibleTraversals:
                    #If this traversal involves both faces
                    if (trav.startFace.building== startBuilding and trav.endFace.building == endBuilding) or (trav.startFace.building == endBuilding and trav.endFace.building== startBuilding):
                        removeThese.append(trav)
                for trav in removeThese:
                    try:
                        face.possibleTraversals.remove(trav)
                    except:
                        pass
                        
            #Now we must delete any traversal to the end face, if not a roof
            if not input_traversal.endFace.faceType == 0:
                for building in self.buildings:
                    for face in building.faces:
                        removeThese = []
                        for trav in face.possibleTraversals:
                            if trav.endFace == input_traversal.endFace:
                                removeThese.append(trav)
                        for trav in removeThese:
                            try:
                                face.possibleTraversals.remove(trav)
                            except:
                                pass

        #elif Building to same building.
        elif input_traversal.startFace.building == input_traversal.endFace.building:

        #   if roof to side, remove all other traversals into that side. Remove side to roof.
            if input_traversal.startFace.faceType == 0  and input_traversal.endFace.faceType != 0:
                for building in self.buildings:
                    for face in building.faces:
                        removeThese = []
                        for traversal in face.possibleTraversals:
                            if traversal.endFace == input_traversal.endFace:
                                removeThese.append(traversal)
                        for traversal in removeThese:
                            try:
                                face.possibleTraversals.remove(traversal)
                            except:
                                pass
                removeThese = []
                for trav in input_traversal.endFace.possibleTraversals:
                    if trav.endFace == input_traversal.startFace:
                        removeThese.append(trav)
                for trav in removeThese:
                    try:
                        input_traversal.endFace.possibleTraversals.remove(trav)
                    except:
                        pass

            #   if side to roof, remove the self roof to side.         
            elif input_traversal.startFace.faceType != 0  and input_traversal.endFace.faceType == 0:
                removeThese = []
                for trav in input_traversal.endFace.possibleTraversals:
                    if trav.endFace == input_traversal.startFace:
                        removeThese.append(trav)
                for trav in removeThese:
                    try:
                        input_traversal.endFace.possibleTraversals.remove(trav)
                    except:
                        pass

    # determine if you can reach from one building to all other buildings
    def isTotallyConnected_perBuilding(self,start_building):
                
        # list to keep track of visited faces and visited buildings 
        visited_faces = []
        visited_buildings = []
        visited_buildings.append(start_building)
 
        #queue of unvisited faces
        unvisited = Queue.Queue()
        
        # get the faces from the buiding 
        faces = start_building.faces 
        for f in faces: unvisited.put(f)
        
        while not unvisited.empty():
            neighbour_face = unvisited.get()
            neighbour_building = neighbour_face.building

            if neighbour_face not in visited_faces:
                if neighbour_building not in visited_buildings:
                    visited_buildings.append(neighbour_building)
                visited_faces.append(neighbour_face)
                traversals = neighbour_face.outs
                for t in traversals: unvisited.put(t.endFace)
        
        return len(visited_buildings) == len(self.buildings)

    
    # main isTotallyConnected function
    def isTotallyConnected(self):
        for building in self.buildings:
            if not self.isTotallyConnected_perBuilding(building): return False 
        return True

    def to_obj(self):
        main_obj  = OBJ()
        return main_obj


        
    def distance (self, point1, point2):
        # get the distance between two points
        dx = math.pow(point2[0] - point1[0],2)
        dy = math.pow(point2[1] - point1[1],2)
        dz = math.pow(point2[3] - point1[2],2)
        return math.pow(dx+dy+dz, 0.5)
 

    def getTraversals(self):
        traversals = []
        for building in self.buildings:
            for face in building.faces:
                for traversal in face.outs:
                    traversals.append(traversal)

        return traversals

        
