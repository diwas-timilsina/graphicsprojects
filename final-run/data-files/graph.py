from objectManipulator import *

# graph class 

# current graph
current_graph = None
count = 0

class Graph(object):

    def __init__(self):
        #initialize the graph
        global current_graph 
        current_graph = {}

    def add(self,vertex):
        # if the vertex is not already in the graph,
        # then add the vertex in the graph dictionary
        global current_graph 
        if vertex not in current_graph:
            current_graph[vertex] = []

    def add_edge(self,edge):
        # add an edge between two vertices
        # edge could be a list or a tuple or a set
        edge =  set(edge)
        (node1,node2) = tuple(edge)
        global current_graph

        if node1 in current_graph:
            current_graph[node1].append(node2)
        else:
            current_graph[node1] = [node2]
            
    def size(self):
        #size of a graph
        return len(self.vertices())
            
    def edges(self):
        # get all the edges in the graph
        
        edges = []
        global current_graph

        for  vertex in current_graph:
            for neighbour in current_graph[vertex]:
                if {neighbour,vertex} not in edge:
                    edges.append({vertex,neighbour})
        return edges
        
    def vertices(self):
        # get all the vertices of a graph
        global current_graph
        return list(current_graph.keys())

    def __str__(self):
        # similar to toString function in java
        str = "Vertices: "
        global current_graph
        for vertex in current_graph:
            str += repr(vertex) + " "
        str += "\nEdges:"    
        for edges in self.edges():
            str += repr(edge)+ " "
        return str
            
    def next_vert(self):
        # next element
        vertices_coll = self.vertices() 
        global count 
        result = vertices_coll[count]
        
        if not vertices_coll[count] == None:
            count += 1
            
        return result
            
    def hasNext(self):
                
        # check to see of the there are any vertices left to iterate
        global count
        return count < self.size()

    def reset_iter(self):
        # reset the counter 
        global count 
        count = 0


    def to_obj(self):
        """convert the graph to obj,using objectManipulator
           returns an OBJ object"""

        main_obj = OBJ()
        while self.hasNext():
            main_obj.append(self.next_vert().to_obj())
        return main_obj



if __name__ == "__main__":
    
    g = Graph()
    g.add(2)
    g.add(3)
    g.add(4)
    g.add(5)


    print (g.next_vert())
    print (g.next_vert())
    print (g.next_vert())
    print (g.next_vert())


    




    

