class Primitive:
    CUBE = "models/cube/Cube.obj"

    #We initialize a primitive with:
    # - An obj file to load from
    # - A scale in x,y,z
    # - A center point in world space 
    # - A yaw rotation 
    def __init__(self, file, scales, center, rotation):
        self.objFile = file
        self.size = scales
        self.centerPoint = center
        self.yawRotation = rotation
     
    # Each method returns an array of primitives that it creates.
    def cube(self, center, angle, scale):
        cube = Primitive(CUBE, scale, center, angle)
        return [cube]

    def wallRun(self,center, angle, landingLength, wallLength):
        if wallLength > 1000:
            difference = wallLength - 1000
            landingLength += (difference/2)
            wallLength = 1000
            
    
    cube  
    wallRun (0)
    stairwayToHeaven (pipe) (inf)
    vault (0)
    flipJump (Have not done the math)
    simpleStairs (inf - distance used)
    wall (500)
    
