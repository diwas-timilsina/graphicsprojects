# class to convert world space tp face space
# convert face space to world space

import math
from Level import *

def faceToWorld(point,face):
# pre: point is a list of three floats has to be on the face
# post: return a list[x,y,z] with the world position

    faceX = point[0]
    faceY = point[1]

    # get the building properties from the face
    building = face.building
    building_x = building.location[0]
    building_y = building.location[1]
    building_z = building.location[2]
    building_length = building.bounds[0]
    building_height = building.bounds[1]
    building_width = building.bounds[2]
    angle = building.rotation


    # get the origin of the face i.e the top left coordinate
    # point on the world space

    face_type = face.faceType

    # for the top face
    if face_type == 0:
        x = building_x - building_length/2+faceX
        y =  building_y + building_height/2
        z =  building_z - building_width/2+faceY

        return [x*cos(angle)+z*sin(angle), y, -x*sin(angle)+z*cos(angle)]

    # for north face
    elif face_type == 1:

        x = building_x + buildig_length/2 + faceX
        y = building_y + building_height/2 + faceY
        z =  building_z + building_width/2

        return [x*cos(angle)+z*sin(angle), y, -x*sin(angle)+z*cos(angle)]

    # for east face
    elif face_type == 3:

        x = building_x + building_length/2 + faceX
        y = building_y + building_height/2 + faceY
        z = building_z - building_width/2

        return [x*cos(angle)+z*sin(angle), y, -x*sin(angle)+z*cos(angle)]

    # for south face
    elif face_type == 2:

        x = building_x - building_length/2 + faceX
        y = building_y + building_height/2 + faceY
        z =  building_z - building_width/2

        return [x*cos(angle)+z*sin(angle), y, -x*sin(angle)+z*cos(angle)]

    # for west face
    elif face_type == 4:

        x = building_x - building_length/2 + faceX
        y = building_y + building_height/2 + faceY
        z = building_z + building_width/2

        return [x*cos(angle)+z*sin(angle), y, -x*sin(angle)+z*cos(angle)]

dirRotation = {T:0,N:0,E:90,S:180,W:270}

def worldToFace(level,face,point):
    #Assumes point is a list of 3 values.
    #Assumes that the point is on the face
    # ^ Will fail miserably and silently if not true...
    worldX = point[0]
    worldY = point[1]
    worldZ = point[2]

    #Let's get the face's upperLeft corner and some other stuff
    building = face.building
    buildingCenter = building.location
    rotation = building.rotation + dirRotation.get(face.faceType)
    faceOrigin = faceToWorld([0,0],face)

    #Project point onto face plane
    vec = [point[0] - faceOrigin[0], point[1] - faceOrigin[1], point[2] - faceOrigin[2]]
    normal = [(faceOrigin[0] - buildingCenter[0]) /bound[0], (faceOrigin[1] - buildingCenter[1]) / bound[1], (faceOrigin[2] - buildingCenter[2]) /bound[2]]
    dist = vec[0] * normal[0] + vec[1] * normal[1] + vec[2] * normal[2]

    proj_point = [point[0] - (dist * normal[0]), point[1] - (dist * normal[1]), point[2] - (dist * normal[2])]

    #rotate points to north face to make math simple
    rotFaceOrigin = [0,0,0]
    rotFaceOrigin[0] = cos(rotation) * faceOrigin[0] - sin(rotation) * faceOrigin[2]
    rotFaceOrigin[1] = faceOrigin[1]
    rotFaceOrigin[2] = sin(rotation) * faceOrigin[0] + cos(rotation) * faceOrigin[2]

    rotPoint = [0,0,0]
    rotPoint[0] = cos(rotation) * proj_point[0] - sin(rotation) * proj_point[2]
    rotPoint[1] = proj_point[1]
    rotPoint[2] = sin(rotation) * proj_point[0] + cos(rotation) * proj_point[2]

    if face.faceType == "T":
        facePos = [0,0]
        facePos[0] = rotPoint[0] - rotFaceOrigin[0]
        facePos[1] = rotPoint[2] - rotFaceOrigin[2]
        return facePos
    else:
        facePos = [0,0]
        facePos[0] = rotFaceOrigin[0] - rotPoint[0]
        facePos[1] = rotFaceOrigin[1] - rotPoint[1]
        return facePos
