from building import *
from face import *
from traversal import *
from Level import *
from objectManipulator import *
from voronoi import *

from map import *
from makePath import *
import random

def generateBuildings(numBuildings):
    buildings = []
    for i in xrange(numBuildings):
        #generate parameters for building
        location = (400*i, 0, 400*i) 
        rotation = 0
        bounds   = (200,300 + random.randint(12,60),200) 
        building = Building(location, rotation, bounds)
        buildings.append(building)
    
    return buildings

def generateVoronoi(m):
    # the map is the map we want to write to 
    
    buildings = []

    city = voronoi(800,600,1,1,"foo",1,1)                           
    for cityBuilding in city.getBuildingList():
        argList = [float(cityBuilding.get(i)) for i in ['length','width','height','x','z','y']]
        b = Building( (argList[3],argList[2]/2,argList[4]),(argList[0],argList[1],argList[2])) 
        buildings.append(b)
        
        # need to addCube represenation of the buildings to the map
        # eg, m.addCube(l,w,h,x,y,z, material="M_Rooftops.M_C_RooftopsMat_01")
        
    return buildings

def distance(loc, loc2):
    return sqrt( (loc[0]-loc2[0])**2 + (loc[1]-loc2[1])**2 + (loc[2]-loc2[2])**2)


GRID_SIZE = 300

#Code that tests whether a building can connect with another building

#Takes in a list of buildings
#Takes a world parameter (a three tuple bounding box of the world) 
#MUST NOT HAVE OVERLAPPING BUILDINGS!!!!
def generateGrid(buildings,world):
    grid = [] #A list of lists of lists
    curX = 0
    curY = 0
    curZ = 0
    while (curX <= world[0]):
        print str(curX) + " " + str(world[0])
        curX += GRID_SIZE
        levelX = []
        while (curY <= world[1]):
            print "   " + str(curY) + " " + str(world[1])
            curY += GRID_SIZE
            levelY = []
            while (curZ <= world[2]):
                curZ += GRID_SIZE
                val = False
                #Here we have an x, y, and z
                #Test if it is in any of our buildings
                for building in buildings:
                    if buildContains(curX,curY,curZ,building):
                        val = building
                levelY.append(val)
            curZ = 0
            levelX.append(levelY)
        curY = 0
        grid.append(levelX)
    print "________ done generating grid ________\n"
    return grid

#Takes in an x, y and z and returns the name of the building between them or False
def occupy(x,y,z,grid):
    gridX = int(x / GRID_SIZE)
    gridY = int(y / GRID_SIZE)
    gridZ = int(z / GRID_SIZE)

    try:
        return grid[gridX][gridY][gridZ]
    except:
        return False

#Takes in an x, y, and z and a building and returns if the building contains this point
#Assumes buildings have location parameter
# a length parameter
# a width parameter
# a height parameter
def buildContains(x,y,z,building):
    return (x in range(building.location[0] - (building.bounds[0]/2),building.location[0] + (building.bounds[0]/2))) and (y in range(building.location[1] - (building.bounds[1]/2),building.location[1] + (building.bounds[1]/2))) and (z in range(building.location[2] - (building.bounds[2]/2),building.location[2] + (building.bounds[2]/2)))

#Takes in two locations and returns whether a straight path can be drawn between them
#Buildings must have a location parameter (a three tuple point)
#Buildings must also have a name parameter (a unique identifer)
def tracePath(build1, build2, step, grid):
    if build1 == build2:
        return False

    dist = math.sqrt( (build1.location[0] - build2.location[0])**2 + 
                      (build1.location[1] - build2.location[1])**2 + 
                      (build1.location[2] - build2.location[2])**2)
    traveled = 0
    while traveled < dist:
        traveled += step
        x = build1.location[0] + (traveled/dist) * (build2.location[0] - build1.location[0])
        y = build1.location[1] + (traveled/dist) * (build2.location[1] - build1.location[1])
        z = build1.location[2] + (traveled/dist) * (build2.location[2] - build1.location[2])
        spotInfo = occupy(x,y,z, grid)
        if (spotInfo and spotInfo != build1 and spotInfo != build2): 
            return False
    return True
            


def makeTestBuildingsInLine(num):
    buildings = []
    for i in range(num):
        buildings.append( Building(   (200*i,50,100*i)   ,(100,100,100)) )
    return buildings




def computeNeighbors(buildings, step, grid):
    i = 0
    siz = len(buildings)**2
    for build1 in buildings:
        for build2 in buildings:
            i += 1
            print str(i) + "   " + str(siz)
            if tracePath(build1, build2, 10, grid): # step size of 10?... Make this dynamic?
                build1.neighbors.append(build2)    
    print "________ done computing neighbors ________\n"


# convert the traversals to an actual path and write it to t3d file
def makePathsFromTraversals(level,m):
    traversals = level.getTraversals()
    
    for trav in traversals:
        # starting and ending position on the face
        startPoint = trav.startPos
        endPoint = trav.endPos
        print trav

        # if the traversal is side to self roof 
        if trav.traversalType == 0:
            
            # here 4.5 is the maximum height that you can climb from a point
            if (endPoint[1] - startPoint[1]) > 450:
                # can't climb so need to add a ladder, but wait we need to
                # know the yaw rotation of the ladder as well, the value that I use is just a guess

                
                # get the end face and its type from the traversal
                endFace = trav.endFace
                building = endFace.building
                endType = endFace.faceType
                
                if endType  == 1:
                    m.addLadder(endPoint[0],endPoint[2],endPoint[1]+30,
                                startPoint[0],startPoint[2],startPoint[1],270) 
                elif endType == 2:
                    m.addLadder(endPoint[0],endPoint[2],endPoint[1]-30,
                                startPoint[0],startPoint[2],startPoint[1],90)
                elif endType == 3:
                    m.addLadder(endPoint[0]+30,endPoint[2],endPoint[1],
                                startPoint[0],startPoint[2],startPoint[1],180)
                elif endType == 4:
                    m.addLadder(endPoint[0]-30,endPoint[2],endPoint[1],
                                startPoint[0],startPoint[2],startPoint[1],0)
                    
                
        # if the traversal is from side to side 
        elif trav.traversalType == 1:
            # ignore the OBJ() constructor, I just used to see how things look in g3D
            
           
            gen = MakePath((startPoint[0],startPoint[2],startPoint[1]),
                     (endPoint[0],endPoint[2],endPoint[1]),OBJ())
            
            gen.writeMap(m)

        # if the traversal is from roof top to roof top 
        elif trav.traversalType == 2:
            
            x_diff = startPoint[0]-endPoint[0]
            y_diff = startPoint[1]-endPoint[1]
            z_diff = startPoint[2]-endPoint[2]
            
            if (abs(z_diff) + abs(y_diff) + abs(z_diff) >  1500):
                # if it is less than 15 then you can just jump 
                
                if z_diff > 0 :
                    # we need to be standing on a higher building to zipline
                    if ((startPoint[2]/endPoint[2])+abs(x_diff)+abs(y_diff)) > 2000:
                        # 19 = 8+8+3, 8 = x_diff,y_diff and 3 = height ratio
                        # play around with this constant until we get it right 
                        
                        # now 80% of the time we have a zip line and 
                        # remaining 20% time we have normal path0
                        if (random.random() < 0.80):
                            m.addCube(400,500,300,startPoint[0],startPoint[2],startPoint[1])
                            m.addCube(400,500,300,endPoint[0],endPoint[2],endPoint[1])
                            m.addZipline(startPoint[0],startPoint[2],startPoint[1]+350+300,
                                         endPoint[0],endPoint[2],endPoint[1]+350+300)
                        else:
                            gen4 = MakePath((startPoint[0],startPoint[2],startPoint[1]),
                                     (endPoint[0],endPoint[2], endPoint[1]),OBJ())
                            gen4.writeMap(m)
                else: 
                    # otherwise just have a path from start point to end point
                    gen2 =MakePath((startPoint[0],startPoint[2],startPoint[1]),
                             (endPoint[0],endPoint[2], endPoint[1]),OBJ())
                    
                    gen2.writeMap(m)
                    
        # if the traversal is from roof to side
        elif trav.traversalType == 3: 
             gen3 = MakePath((startPoint[0],startPoint[2],startPoint[1]),
                      (endPoint[0],endPoint[2], endPoint[1]),OBJ())
             
             gen3.writeMap(m)

        # if the traversal is from root to self side
        elif trav.traversalType == 4:
            # just have a cube at the endPoint, make sure that you can 
            # actually stand on the cube
            
            # get the end face and its type from the traversal
            endFace = trav.endFace
            building = endFace.building
            endType = endFace.faceType
          
            print endPoint
            print startPoint

            if (trav.startFace.building.bounds[1] - endPoint[1]) > 540:
                if endType  == 1:
                    m.addLadder(endPoint[0],endPoint[2],endPoint[1]+30,
                                startPoint[0],startPoint[2],startPoint[1],270)
                elif endType == 2:
                    m.addLadder(endPoint[0],endPoint[2],endPoint[1]-30,
                                startPoint[0],startPoint[2],startPoint[1],90)
                elif endType == 3:
                    m.addLadder(endPoint[0]+30,endPoint[2],endPoint[1],
                                startPoint[0],startPoint[2],startPoint[1],180)
                elif endType == 4:
                    m.addLadder(endPoint[0]-30,endPoint[2],endPoint[1],
                                startPoint[0],startPoint[2],startPoint[1],0)
            


            m.addCube(500,550,100,endPoint[0],endPoint[2],endPoint[1]) 
            # the scaling is arbitary, see what works 
            
           
                
    print "_______done making paths from traversals______"
# -----------------------------------------------

'''
def makeTestBuildings1():
    buildings = []

    height = random.randint(50,70)
    buildings.append( Building(   ( 100 ,height/2,100  )       ,       (30  , height ,30  ) ) )

    height = random.randint(200,1000)
    buildings.append( Building(   ( 100 ,height/2,600  )       ,       (30  , height ,30  ) ) )

    height = random.randint(200,1000)
    buildings.append( Building(   ( 300 ,height/2,400  )       ,       (30  , height ,30  ) ) )

    height = random.randint(200,1000)
    buildings.append( Building(   ( 500 ,height/2,200  )       ,       (30  , height ,30  ) ) )

    height = random.randint(200,1000)
    buildings.append( Building(   ( 600 ,height/2,500  )       ,       (30  , height ,30  ) ) )

    return buildings
'''



def makeTestBuildings2():
    buildings = []

    height = random.randint(3500, 4500)
    buildings.append( Building(   ( 1500 ,height/2, 2000  )       ,       (2000  , height , 2500  ) ) )

    height = random.randint(3500, 4500)
    buildings.append( Building(   ( 6500 ,height/2, 3000  )       ,       (2000  , height , 2500  ) ) )

    height = random.randint(3500, 4500)
    buildings.append( Building(   ( 15500 ,height/2, 5500  )       ,       (2000  , height , 2500  ) ) )

    height = random.randint(3500, 4500)
    buildings.append( Building(   ( 75000 ,height/2, 7500  )       ,       (2000  , height , 2500  ) ) )

    return buildings


def main():
    
    m = Map()
    m.setDimension(200000,200000)
    #buildings = makeTestBuildings2()
    #for building in buildings:
    #    m.addCube(building.bounds[0], building.bounds[2], building.bounds[1], building.location[0],  building.location[2],  building.location[1])
    #buildings = generateVoronoi(m)
    buildings = []
    city = voronoi(800,600,2.4,10.0,"foo",1,1)
    for building in city.getBuildingList():
        argList = [float(building.get(i))*30 for i in ['length','width','height','x','z','y']]
        argList[0]*=1.8
        argList[1]*=1.8
        argList[2]*=3
        argList[5] = argList[2]/2
        print argList
        m.addCube(*argList,material="i_SubWay.SubWayTunnelRide_02.M_TrainRideLamp_01_ColourA")
        buildings.append(Building((int(argList[3]),int(argList[5]),int(argList[4])),(int(argList[0]),int(argList[2]),int(argList[1]))))

    grid = generateGrid(buildings, (50000,7000,40000))
    computeNeighbors(buildings, 100, grid)
    '''for i in range(len(buildings)):
        print buildings[i]
    '''
    level = Level(buildings)
    makePathsFromTraversals(level,m)
    m.writeT3D("meow.t3d")
    print "_____done writin t3d, wooo!_____"



if __name__ == "__main__":
    main()
