import sys 
import Queue
import random 
import math 

from objectManipulator import *

# haven't really used all these constants yet
MIN_DIS = 1
CUBE_BOUND = 5 
CUBE_DIM = 300
CUBE_JUMP = 500 
JUST_JUMP = 200
VAR_Y = 2
MIN_Y = 100
VAR_X = 200
MIN_X = 100
X_STEP = 2


# information about the primitives
primitive_loc = []

def distance (point1, point2):
    # get the distance between two points
    dx = math.pow(point2[0] - point1[0],2)
    dy = math.pow(point2[1] - point1[1],2)
    dz = math.pow(point2[2] - point1[2],2)
    return math.pow(dx+dy+dz, 0.5)
 
def h_distance (point1,point2):
    # get the horizontal distance between two points
    dx = point2[0] - point1[0]
    return dx
    
def v_distance (point1, point2):
    # get the vertical distance bewteen two points
    dy = point2[1] - point1[1]
    return abs(dy)


def heigherPoint(point1,point2):
    #return the point with larger vertical height
    dy = point2[1]-point1[1]
    if dy >= 0: return [point1,point2] 
    else: return [point2,point1] 

def get_y(pointA,pointB,x):
    # get the y coordinate of point x on the line from point A to point B
    # slope 
    m = (pointB[1]-pointA[1])/(pointB[0]-pointA[0])
    return m*x - pointA[0]+pointA[1]


def generatePath(pointA, pointB):
    # generate a path from pointA to pointB, pointA and pointB 
    # are points in two dimension
    
    # get the heigher point
    h = heigherPoint(pointA,pointB)
    pointA = h[0] # lower point 
    pointB = h[1] # heigher point 
    
    if distance(pointA,pointB) < MIN_DIS: exit()

    # choose x based on {min_x,max_x}
    # then chose y on the line from pointA to pointB 
    # add variance to y, i.e  y +/- previous_y + previous_height/2
    # height = scale by random(1,max_possible)
    # weight = scale by random(smallext, largest possible)

    # number of possible cubes that we can place between two faces
    #possible_cubes = h_distance(pointA,pointB)/CUBE_BOUND

    #------------- temp values, replace these values with actual values ----#
    
    #maximum and minimum possible width of the cube
    bounding_box_w_min = 2
    bounding_box_w_max = 4
    
    # maximum and minimum possible height of the cube
    bounding_box_h_min = 1
    bounding_box_h_max = 3

    # maximum and minimum possible step in x and y direction
    x_step_min= 4
    x_step_max= 5.5

    y_step_min= 2
    y_step_max= 4
    
    # maximum height and width
    max_height = 10
    max_width = 10
    min_width = 2 

    #--------------------------------------------------#
   
    # (x,y,z,h,w)
    x = pointA[0]
    p_y = pointA[1]
    p_h = 2 
    p_w = 2
    p_l = 4

    global primitive_loc

    primitive_loc.append((x,p_y,0,p_l,p_w,p_h))
    
    while True:
        
        # step away from point A 
        x = x + random.uniform(x_step_min,x_step_max) 

        # get the y coordinate of x on the line
        y = get_y(pointA, pointB, x) 
        dy = p_y + p_h/2
        y = y + random.uniform(-dy,dy)
        p_y = y
        
        # height of the cube
        h = random.uniform(bounding_box_h_min,bounding_box_h_max)
        w = random.uniform(bounding_box_w_min,bounding_box_w_max)        
        l = random.uniform(bounding_box_w_min,bounding_box_w_max)        
        
        if x > pointB[0]: break
        
        primitive_loc.append((x,y,0,l,w,h))    

    primitive_loc.append((pointB[0],pointB[1],0,p_l,p_w,p_h))
   
   
if __name__ == "__main__":
    #    print (get_y([0,0],[1,1],5))
    generatePath([0,0,0],[50,100,0])
    
    obj = OBJ()
    for (x,y,z,l,w,h) in primitive_loc:
        print (x,y,z)
        obj2 = OBJ()
        obj2.load("Cube.obj","Cube.mtl")
        obj2 = obj2.translate(x,y,z).scaleAxis(l,h,w)
        obj.append(obj2)
    obj.write("test.obj")
    print "Done!"
