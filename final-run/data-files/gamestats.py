import math
import random


class GameStats(object):

    def __init__(self, prob, maxHJump, maxVJump, maxFall, maxWallRun):
        self.prob = prob
        self.maxHJump = maxHJump
        self.maxVJump = maxVJump
        self.maxFall = maxFall
        self.maxWallRun = maxWallRun
        
