from objectManipulator import *
from random import *

obj = OBJ()

(x0,y0,z0) = (0,0,0)
(x1,y1,z1) = (100,0,50)

obj2 = OBJ()
obj2.load("../data-files/Cube.obj","../data-files/Cube.mtl")
obj2 = obj2.scale3(1,1,0.05)

cubes = []
cubes.append([0,0,0])
print cubes

while ((x1-x0)>1 and (z1-z0)>1):
    distance = random()*4+2
    z = random()*3
    x = random()*(distance-1)+1
    while (abs((x1-x0-x)*3)<abs(z1-z0-z)):
        print str(x0) + " " + str(z0)
        distance = random()*4+2
        z = random()*3
        x = random()*(distance-1)+1
    #y = (distance**2-x**2)**0.5
    y = 0
    x0+=x
    y0+=y
    z0+=z
    cubes.append([x0,y0,z0])
    print cubes
    
for a in cubes:
    obj.append(obj2.translate(a[0],a[1],a[2]))

obj.scale_self(100)
obj.writeT3D("test.t3d")
