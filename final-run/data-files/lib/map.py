from objectManipulator import *
from voronoi import *

def getLength(x1,y1,z1,x2,y2,z2):
    return ((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)**0.5

def getYaw(x1,y1,z1,x2,y2,z2):
    l = ((x1-x2)**2+(y1-y2)**2)**0.5
    a = asin(abs(y1-y2)/l)
    return int(a/(2*pi)*65536)

def getPitch(x1,y1,z1,x2,y2,z2):
    l = ((x1-x2)**2+(y1-y2)**2+(z1-z2)**2)**0.5
    a = asin(abs(z1-z2)/l)
    return int(a/(2*pi)*65536)
    

class Brush:
    def __init__(self,t,config):
        self.t = t
        self.config = config
        self.obj = self.createOBJ()
    
    def __str__(self):
        s = self.t
        for c in self.config:
            s += " " + str(c)
        return s

    def createOBJ(self):
        obj = OBJ()
        cfg = self.config
        if self.t == "cube":
            (x,y,z) = (cfg[0],cfg[1],cfg[2])
        elif self.t == "swing":
            x = 250
            y = getLength(*cfg)
            z = 300
        elif self.t == "zipline":
            x = getLength(*cfg)
            y = 300
            z = 400
        elif self.t == "ladder":
            x = 80
            y = 80
            z = cfg[5]-cfg[2]
        elif self.t == "balance":
            x = getLength(*cfg)
            y = 80
            z = 150

        obj.load("../data-files/Cube.obj","../data-files/Cube.mtl")

        if self.t == "zipline":
            return obj.scale3(x,y,z).rotate(getYaw(*cfg)/65536.0*360,0,getPitch(*cfg)/65536.0*360)

        return obj.scale3(x,y,z)

    def toT3D(self,i):
        if self.t == "cube":
            return self.cubeT3D(i)
        if self.t == "swing":
            return self.swingT3D(i)
        if self.t == "zipline":
            return self.zipT3D(i)
        if self.t == "ladder":
            return self.ladderT3D(i)
        if self.t == "balance":
            return self.balanceT3D(i)
    
    def cubeT3D(self,i):
        s = ""
        s+=("      Begin Actor Class=Brush Name=Brush_"+str(i)+" Archetype=Brush'Engine.Default__Brush'\n")
        
        s+=("         CsgOper=CSG_Add\n" )
        
        s+=("         Begin Brush Name=Model_" + str(i) + "\n")
        s+=("            Begin PolyList\n")
        
        s+= self.obj.createPolyList()

        s+=("            End PolyList\n")
        s+=("         End Brush\n")
        s+=("         Brush=Model'Model_" + str(i) + "'\n")
        s+=("         Location=(X="+fV(self.config[3])+",Y="+fV(self.config[4])+",Z="+fV(self.config[5])+")\n")
        s+=("      End Actor\n")
        return s

    def swingT3D(self,i):
        cfg = self.config
        x = (cfg[0]+cfg[3])/2
        y = (cfg[1]+cfg[4])/2
        z = (cfg[2]+cfg[5])/2
        yaw = getYaw(*cfg)

        s = ""
        s+=("      Begin Actor Class=TdSwingVolume Name=TdSwingVolume_"+str(i)+" Archetype=TdSwingVolume'TdGame.Default__TdSwingVolume'\n")
        s+=("         Begin Brush Name=Model_" + str(i) + "\n")
        s+=("            Begin PolyList\n")
        
        s+= self.obj.createPolyList()
        
    	s+=("            End PolyList\n")
    	s+=("         End Brush\n")
    	s+=("         Brush=Model'Model_" + str(i) + "'\n")
        s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
        s+=("         Rotation=(Pitch=0" + ",Yaw="+str(yaw+16384)+",Roll=0"+")\n")
    	s+=("      End Actor\n")
        
        for j in [0,1]:
            s+=("      Begin Actor Class=StaticMeshActor Name=StaticMeshActor_"+str(i)+"_"+str(j)+" Archetype=StaticMeshActor'Engine.Default__StaticMeshActor'\n")
            s+=("         Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0 ObjName=StaticMeshComponent_"+str(i)+"_"+str(j) +" Archetype=StaticMeshComponent'Engine.Default__StaticMeshActor:StaticMeshComponent0'\n")
            s+=("            StaticMesh=StaticMesh'P_RunnerObjects.SwingPole_01.S_SwingPole_01c'\n")
            s+=("            LightingChannels=(bInitialized=True,Static=True)\n")
            s+=("         End Object\n")
            s+=("         StaticMeshComponent=StaticMeshComponent'StaticMeshComponent_"+str(i)+"_"+str(j)+"'\n")
            s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
            s+=("         Rotation=(Pitch=0" + ",Yaw="+str(yaw+j*32768)+",Roll=0"+")\n")
            s+=("         DrawScale3D=(X="+fV(0.8*getLength(*cfg)/200)+",Y="+fV(1)+",Z="+fV(1)+")\n")
            s+=("      End Actor\n")

        return s

    def zipT3D(self,i):
        cfg = self.config
        x = (cfg[0]+cfg[3])/2
        y = (cfg[1]+cfg[4])/2
        z = (cfg[2]+cfg[5])/2
        yaw = getYaw(*cfg)
        pitch = getPitch(*cfg)

        s = ""
        s+=("      Begin Actor Class=TdZiplineVolume Name=TdZiplineVolume_"+str(i)+" Archetype=TdZiplineVolume'TdGame.Default__TdZiplineVolume'\n")
        s+=("         Start=(X="+fV(cfg[0])+",Y="+fV(cfg[1])+",Z="+fV(cfg[2])+")\n")
        s+=("         End=(X="+fV(cfg[3])+",Y="+fV(cfg[4])+",Z="+fV(cfg[5])+")\n")
        s+=("         Middle=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
        s+=("         bAllowSplineControl=True\n")

        s+=("         Begin Brush Name=Model_" + str(i) + "\n")
        s+=("            Begin PolyList\n")
        
        s+= self.obj.createPolyList()
        
    	s+=("            End PolyList\n")
    	s+=("         End Brush\n")
    	s+=("         Brush=Model'Model_" + str(i) + "'\n")
        s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
    	s+=("      End Actor\n")
        
        for j in [0,1]:
            k = 1
            if j == 1: k = -1
            s+=("      Begin Actor Class=StaticMeshActor Name=StaticMeshActor_"+str(i)+"_"+str(j)+" Archetype=StaticMeshActor'Engine.Default__StaticMeshActor'\n")
            s+=("         Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0 ObjName=StaticMeshComponent_"+str(i)+"_"+str(j) +" Archetype=StaticMeshComponent'Engine.Default__StaticMeshActor:StaticMeshComponent0'\n")
            s+=("            StaticMesh=StaticMesh'P_RunnerObjects.SwingPole_01.S_SwingPole_01c'\n")
            s+=("            LightingChannels=(bInitialized=True,Static=True)\n")
            s+=("            CollideActors=False\n")
            s+=("            BlockRigidBody=False\n")
            s+=("         End Object\n")
            s+=("         StaticMeshComponent=StaticMeshComponent'StaticMeshComponent_"+str(i)+"_"+str(j)+"'\n")
            s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
            s+=("         Rotation=(Pitch="+str(pitch*k)+",Yaw="+str(yaw+j*32768)+",Roll="+")\n")
            s+=("         DrawScale3D=(X="+fV(0.8*getLength(*cfg)/200)+",Y="+fV(1)+",Z="+fV(1)+")\n")
            s+=("      End Actor\n")

        return s
    
    def ladderT3D(self,i):
        cfg = self.config
        x = (cfg[0]+cfg[3])/2
        y = (cfg[1]+cfg[4])/2
        z = (cfg[2]+cfg[5])/2
        yaw = cfg[2]/360*65536

        s = ""
        s+=("      Begin Actor Class=TdLadderVolume Name=TdLadderVolume_"+str(i)+" Archetype=TdLadderVolume'TdGame.Default__TdLadderVolume'\n")
        s+=("         LadderType=LT_Pipe\n")
        s+=("         Begin Brush Name=Model_" + str(i) + "\n")
        s+=("            Begin PolyList\n")
        
        s+= self.obj.createPolyList()
        
    	s+=("            End PolyList\n")
    	s+=("         End Brush\n")
    	s+=("         Brush=Model'Model_" + str(i) + "'\n")
        s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
        s+=("         Rotation=(Pitch=0" + ",Yaw="+str(yaw)+",Roll=0"+")\n")
    	s+=("      End Actor\n")
        
        s+=("      Begin Actor Class=StaticMeshActor Name=StaticMeshActor_"+str(i)+" Archetype=StaticMeshActor'Engine.Default__StaticMeshActor'\n")
        s+=("         Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0 ObjName=StaticMeshComponent_"+str(i)+" Archetype=StaticMeshComponent'Engine.Default__StaticMeshActor:StaticMeshComponent0'\n")
        s+=("            StaticMesh=StaticMesh'P_RunnerObjects.SwingPole_01.S_SwingPole_01c'\n")
        s+=("            LightingChannels=(bInitialized=True,Static=True)\n")
        s+=("         End Object\n")
        s+=("         StaticMeshComponent=StaticMeshComponent'StaticMeshComponent_"+str(i)+"'\n")
        s+=("         Location=(X="+fV(cfg[3])+",Y="+fV(cfg[4])+",Z="+fV(cfg[5])+")\n")
        s+=("         Rotation=(Pitch=16384"+",Yaw=0"+",Roll=0"+")\n")
        s+=("         DrawScale3D=(X="+fV(0.8*getLength(*cfg[:6])/100)+",Y="+fV(3)+",Z="+fV(3)+")\n")
        s+=("      End Actor\n")

        return s
        return ""

    def balanceT3D(self,i):
        cfg = self.config
        x = (cfg[0]+cfg[3])/2
        y = (cfg[1]+cfg[4])/2
        z = (cfg[2]+cfg[5])/2
        yaw = getYaw(*cfg)

        s = ""
        s+=("      Begin Actor Class=TdBalanceWalkVolume Name=TdBalanceWalkVolume_"+str(i)+" Archetype=TdBalanceWalkVolume'TdGame.Default__TdBalanceWalkVolume'\n")
        s+=("         Begin Brush Name=Model_" + str(i) + "\n")
        s+=("            Begin PolyList\n")
        
        s+= self.obj.createPolyList()
        
    	s+=("            End PolyList\n")
    	s+=("         End Brush\n")
    	s+=("         Brush=Model'Model_" + str(i) + "'\n")
        s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z+75)+")\n")
        s+=("         Rotation=(Pitch=0" + ",Yaw="+str(yaw)+",Roll=0"+")\n")
    	s+=("      End Actor\n")
        
        for j in [0,1]:
            s+=("      Begin Actor Class=StaticMeshActor Name=StaticMeshActor_"+str(i)+"_"+str(j)+" Archetype=StaticMeshActor'Engine.Default__StaticMeshActor'\n")
            s+=("         Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0 ObjName=StaticMeshComponent_"+str(i)+"_"+str(j) +" Archetype=StaticMeshComponent'Engine.Default__StaticMeshActor:StaticMeshComponent0'\n")
            s+=("            StaticMesh=StaticMesh'P_RunnerObjects.SwingPole_01.S_SwingPole_01c'\n")
            s+=("            LightingChannels=(bInitialized=True,Static=True)\n")
            s+=("         End Object\n")
            s+=("         StaticMeshComponent=StaticMeshComponent'StaticMeshComponent_"+str(i)+"_"+str(j)+"'\n")
            s+=("         Location=(X="+fV(x)+",Y="+fV(y)+",Z="+fV(z)+")\n")
            s+=("         Rotation=(Pitch=0" + ",Yaw="+str(yaw+j*32768)+",Roll=0"+")\n")
            s+=("         DrawScale3D=(X="+fV(0.8*getLength(*cfg)/200)+",Y="+fV(1.2)+",Z="+fV(1.2)+")\n")
            s+=("      End Actor\n")

        return s

class Map:
    def __init__(self):
        self.brushes = []
    
    def addCube(self,x,y,z,xs,ys,zs,material=""):
        self.brushes.append(Brush("cube",[x,y,z,xs,ys,zs,material]))

    def addSwing(self,x1,y1,z1,x2,y2,z2):
        self.brushes.append(Brush("swing",[x1,y1,z1,x2,y2,z2]))

    def addZipline(self,x1,y1,z1,x2,y2,z2):
        self.brushes.append(Brush("zipline",[x1,y1,z1,x2,y2,z2]))

    def addLadder(self,x1,y1,z1,x2,y2,z2,yaw):
        self.brushes.append(Brush("ladder",[x1,y1,z1,x2,y2,z2,yaw]))

    def addBalance(self,x1,y1,z1,x2,y2,z2):
        self.brushes.append(Brush("balance",[x1,y1,z1,x2,y2,z2]))

    def save(self,filename):
        with open(filename, 'w') as f:
            for brush in self.brushes:
                f.write(str(brush) + "\n")

    def writeT3D(self,filename):
        with open(filename, 'w') as f:
            f.write("Begin Map\n")
            f.write("   Begin Level NAME=PersistentLevel\n")
            f.write("      Begin Actor Class=WorldInfo Name=WorldInfo_11 Archetype=WorldInfo'Engine.Default__WorldInfo'\n")
            f.write("         bPathsRebuilt=True\n")
            f.write("         DefaultPostProcessSettings=(Curves=(Ms[0]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[1]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[2]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[3]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[4]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[5]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[6]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[7]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[8]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[9]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[10]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[11]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[12]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[13]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[14]=(X=1.000000,Y=1.000000,Z=1.000000),Ms[15]=(X=1.000000,Y=1.000000,Z=1.000000)))\n")
            f.write("         TimeSeconds=0.000000\n")
            f.write("         RealTimeSeconds=0.000000\n")
            f.write("         AudioTimeSeconds=0.000000\n")
            f.write("         Tag=\"WorldInfo\"\n")
            f.write("         Name=\"WorldInfo_11\"\n")
            f.write("         ObjectArchetype=WorldInfo'Engine.Default__WorldInfo'\n")
            f.write("      End Actor\n")
            f.write("      Begin Actor Class=Brush Name=Brush_0 Archetype=Brush'Engine.Default__Brush'\n")
            f.write("      End Actor\n")

			#---------------------Actual Code Here#
            for i in range(len(self.brushes)):
                f.write(self.brushes[i].toT3D(i+1))

            f.write("   End Level\n")
            f.write("Begin Surface\n")
            f.write("End Surface\n")
            f.write("End Map\n")

            f.close()

    def open(self,filename):
        with open(filename, 'r') as f:
            a = f.readlines()
    
def main():
    m = Map()
    #city = voronoi(800,600,1,1,"foo",1,1)
    #for building in city.getBuildingList():
    #    argList = [float(building.get(i))*30 for i in ['length','width','height','x','z','y']]
    #    argList[5] = argList[2]/2
    #    print argList
    #    m.addCube(*argList)

    #for i in range(1,50):
        #m.addCube(200,200,10,250*sin(i),250*cos(i),400*i)
    #for i in range(1,100):
        #m.addSwing(i*200,1000,i*50,i*200,-1000,i*50)
    #m.addZipline(200,4000,1000,200,0,0)
    #m.addLadder(0,0,0,0,0,1000,0)
    #m.addBalance(200,700,0,200,0,0)
    m.addCube(100,100,100,0,0,0)
    m.save("test.mir")
    m.writeT3D("test.t3d")

    

if __name__ == "__main__": main()

