# Actual class to make path between two points 

from objectManipulator import *
from random import *
from map import *


class MakePath(object):
    
    def __init__(self, (x0,y0,z0), (x1,y1,z1), obj=OBJ()):
        flag = False
        if x0 > x1:
            (t1,t2,t3) = (x0,y0,z0)
            (x0,y0,z0) = (x1,y1,z1)
            (x1,y1,z1) = (t1,t2,t3)
            flag = True

        self.x0 = x0
        self.y0 = y0
        self.z0 = z0
        x0/=100.0
        y0/=100.0
        z0/=100.0
        x1/=100.0
        y1/=100.0
        z1/=100.0

        self.yaw = asin((y1-y0)*1.0/(((x1-x0)**2+(y1-y0)**2)**0.5))

        x1 = x0 + ((x1-x0)**2+(y1-y0)**2)**0.5
        y1 = y0

        # takes in origin, end and the map as parameter
       # self.(x0,y0,z0) = (x0,y0,z0) 
        #self.(x1,y1,z1) = (x1,y1,z1)
        seed()
        self.m = Map()
        self.obj = obj
        
        obj2 = OBJ()
        obj2.load("Cube.obj","Cube.mtl")
        
        obj3 = OBJ()
        obj3.load("Cube.obj","Cube.mtl")
        
        # constants 
        # no need to put anything in between points if the distance is less that 2
        max_climb = 3.2
        max_x = 6.4
        max_jump = 4.5  # this max is in horizontal direction
        max_height = 10 # maximum heights of the cubes
        max_length = 4 # maximum length of the cubes
        max_width = 4  # maximum width of the cubes

        l = uniform(3,max_length) 
        w = uniform(3,max_width)

        #print "width = " + str(w)

        h = uniform(2,max_height)

        cubes = []
        disp = []
        #cubes.append([0,0,0,l,w,h])

        (startX,startY,startZ) = (x0,y0,z0)

        xTotal = x1-x0
        zTotal = z1-z0
        expectedK = float(zTotal)/xTotal

        posLimit = 5/max_jump
        negLimit = 5/max_jump

        while (x1-x0)>7:
            l = uniform(2,max_length)
            w = uniform(2,max_width)
            h = uniform(1,max_height)
            
            k = (z1-z0)/(x1-x0)

            x = random()*max_jump + l
            y = 0
            #print k
            if (k-expectedK)/expectedK < 0.3+0.3/(abs(x1-x0)/xTotal+0.3)*1.1 and abs(x1-x0)*8>abs(z1-z0):
                #print "inTrack"
                dZ = -1000
                while dZ > posLimit or dZ < -negLimit:
                    dZ = gauss(expectedK,0.7)
                #dZ = uniform(-negLimit,posLimit)+expectedK
                if abs(dZ)<max_climb/2 and random()>0.4:
                    x += uniform(max_jump-2,max_jump)
                    if random()>0.8:
                        dZ = 0

            else:
                #print "need adjustment"
                if k < expectedK:
                    dZ = -negLimit
                else:
                    dZ = posLimit
                f = abs(z1-z0)/abs(x1-x0-4)
                if f > 2:
                    x/=f
                    #print str(x) + "!!!"
            z = max_jump*dZ

            x0 += x
            y0 += y
            z0 += z


            disp.append([x,y,z,l,w,h])
            #cubes.append([x0-l/2,y0,z0-h/2,l,w,h])
        while (x1-x0)<5 and len(disp)>0:
            a = disp[-1]
            x0 -= a[0]
            z0 -= a[2]
            disp.pop()

        while (z1-z0)>3:
            disp.append([1.0,0.0,3.0,3.0,3.0,2.0])
            x0 += 1
            z0 += 3
        if x1-x0 > 0:
            disp.append([(x1-x0)/2,0,(z1-z0)/2,3.0,3.0,2.0])
        #print z0
        shuffle(disp)
        ydisp = [uniform(-4,4) for i in range(len(disp)/2)]
        ydisp += [-n for n in ydisp]
        if len(ydisp)<len(disp):
            ydisp.append(0)
        shuffle(ydisp)
        (x0,y0,z0) = (startX,startY,startZ)
        for i in range(len(disp)):
            d = disp[i]
            x0 += d[0]
            y0 += ydisp[i]
            z0 += d[2]
            cubes.append([x0-d[3]/2.0,y0,z0-d[5]/2.0,d[3],d[4],d[5]])

        if flag: 
            cubes = cubes[::-1]
        
        for c in cubes:
            pass

        # the case when there is not much variance in y
        #print cubes
        #while ((x1-x0)>2 and (z1-z0)>2):    
    
        #    distance = random()*3+5
        #    z = random()*max_climb 
        #    x = random()*(distance-1)+2
        #    y = uniform(-w,w)
    
        #    #while (x > max_jump + 2) :
        #    while ((abs((x1-x0+x)*(3-2*(x1-x0)/(x1-startX)))<abs(z1-z0+z)) or x > max_jump + 2 ): 
        #        #print str(x0) + " " + str(z0)
        #        distance = random()*3+5
        #        z = random()*max_climb
        #        #   x = random()*(distance-1)+1
        #        x = uniform(random()*(distance-1)+2,4)
        #        y = uniform(-w,w)
        #    print (x1-x0+x)
        #    print (z1-z0+z)
        #    print (abs((x1-x0+x)*3)-abs(z1-z0+z))
        #    print "-----------"
    
   
        #    l = uniform(3,max_length)
        #    w = uniform(3,max_width)
        #    h = uniform(2,max_height)
        #    x += l
        #        
        #    x0+=x
        #    y0+=y
        #    z0+=z
    
        #    cubes.append([x0,y0,z0,l,w,h])
        #print cubes[-1]
        #for c in cubes:
            #print c

        """
        # if there is not much variance in x 
        #print cubes
        while ((y1-y0)>2 and (z1-z0)>2):    
    
            distance = random()*3+5
            z = random()*max_climb 
            y = random()*(distance-1)+2
            x = uniform(-w,w)
    
            while ((abs((y1-y0+y)*3)<abs(z1-z0+z)) and x > max_jump + 2) : 
                print str(x0) + " " + str(z0)
                distance = random()*3+5
                z = random()*max_climb
                #   x = random()*(distance-1)+1
                y = uniform(random()*(distance-1)+2,4)
                x = uniform(-w,w)
    
   
            w = uniform(3,max_length)
            l = uniform(3,max_width)
            h = uniform(2,max_height)
            y += w
                
            x0+=x
            y0+=y
            z0+=z
    
            cubes.append([x0,y0,z0,l,w,h])
            # print cubes
        """
        
        # texture to chose from 
        texture1 = ("i_Maintenance.BSPMaterials.MI_ConcreteCeiling_01_Red","i_Factory.BSPMaterials.MI_FactoryConcreteWall_01")[randrange(0,2)==0]
        texture2 = ("i_Factory.FactoryLip_01.M_FactoryLip_01_Orange","i_Factory.BSPMaterials.MI_FactoryConcreteFloor_01_Orange")[randrange(0,2)==0]
        texture = (texture1, texture2)[randrange(0,2)==0]

        print texture

        # go back to the previously created cubes and see if we can squeeze any more 
        # treversals 
        # new list of cubes
        new_cubes = []

        new_cubes.append(cubes[0])
        self.obj.append(obj2.scale3(cubes[0][3],cubes[0][4],cubes[0][5]).translate(cubes[0][0],cubes[0][1],cubes[0][2]))
        self.m.addCube(100*cubes[0][3],100*cubes[0][4],100*cubes[0][5],100*cubes[0][0],100*cubes[0][1],100*cubes[0][2],texture)

        for i in range(len(cubes)-1):
            current_cube = cubes[i]
            next_cube = cubes[i+1]
      
            texture1 = ("i_Maintenance.BSPMaterials.MI_ConcreteCeiling_01_Red","i_Factory.BSPMaterials.MI_FactoryConcreteWall_01")[randrange(0,2)==0]
            texture2 = ("i_Factory.FactoryLip_01.M_FactoryLip_01_Orange","i_Factory.BSPMaterials.MI_FactoryConcreteFloor_01_Orange")[randrange(0,2)==0]
            texture = (texture1, texture2)[randrange(0,2)==0]
            
            print texture

            new_cubes.append(next_cube)
            self.obj.append(obj2.scale3(next_cube[3],next_cube[4],next_cube[5]).translate(next_cube[0],next_cube[1],next_cube[2]))
            self.m.addCube(100*next_cube[3],100*next_cube[4],100*next_cube[5],100*next_cube[0],100*next_cube[1],100*next_cube[2],texture)

        # randomly add some vault 
            if (uniform(0,1) < 0.1):
                #print "vault cube " + str(i)
                vault_cube = [current_cube[0],current_cube[1],current_cube[2]+current_cube[5]/2+uniform(2,max_climb),current_cube[3]/3,current_cube[4], current_cube[5]/3]
                new_cubes.append(vault_cube)
                self.obj.append(obj2.scale3(vault_cube[3],vault_cube[4],vault_cube[5]).translate(vault_cube[0],vault_cube[1],vault_cube[2]))
                self.m.addCube(100*vault_cube[3],100*vault_cube[4],100*vault_cube[5],100*vault_cube[0],100*vault_cube[1],100*vault_cube[2],"i_Boat.BoatFloor_01.M_BoatFloor_02")

            # if x distance is more than maximum you can jump then you need to place a primitive
            # if z distance is more that the vertical height that you can jump, then you also need to 
            # place a primitive 
            # we don't really care abot the y direction 

            # how different are two consequetive cubes
            dx = next_cube[0]-next_cube[3]/2 - (current_cube[0]+current_cube[3]/2) 
            dz = (next_cube[2]+next_cube[5]/2) - (current_cube[2]+current_cube[5]/2)

            # if dx > max_jump_distance then 
            # if dz < some_small_value and  
            # then wall run or swing 
            # otherwise just put a cube that acts as a spring in the middle

            if dx > (max_jump):
                print dx
                if abs(dz) < max_climb/2:
                    if dz == 0:
                        self.m.addBalance(current_cube[0]*100+current_cube[3]*100/2,current_cube[1]*100,current_cube[2]*100+current_cube[5]*100/2,next_cube[0]*100-next_cube[3]*100/2,next_cube[1]*100,next_cube[2]*100+next_cube[5]*100/2)
                    elif (uniform(0,1) < 0.2) or dx > 6.5:
                        # wall run 
                        x = (next_cube[0]+current_cube[0])/2

                        # only placing wall run on one side
                        # take min to place on the other side
                        y = (max(next_cube[1],current_cube[1]) + max(next_cube[4]/2,current_cube[4]/2) + 1,
                             max(next_cube[1],current_cube[1]) - max(next_cube[4]/2,current_cube[4]/2) - 1 )[randrange(0,2)==0]

                        z = max(next_cube[2],current_cube[2]) + max(next_cube[5]/2,current_cube[5]/2)

                        l = dx * 2
                        w = 0.25 
                        h = max(next_cube[5],current_cube[5]) +3

                        #print "append  wall " + str(i)
                        new_cubes.append([x,y,z,l,w,h])
                     
                        self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                        self.m.addCube(100*l,100*w,100*h*0.5, 100*x, 100*y, 100*z,"i_Office.OfficeDoorFrame_02.M_OfficeForstedGlassMasked_01")

                    else:
                        # swing  
                        x = (current_cube[0]+next_cube[0])/2
                        y = current_cube[1]
                        z = current_cube[2]+ current_cube[5]/2 + max_jump - 1.00

                        l = 0.25
                        w = (current_cube[4]+next_cube[5])/2
                        h = 0.25

                        #print "append siwng " + str(i)
                        new_cubes.append([x,y,z,l,w,h])

                        self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                        self.m.addSwing(100*x,100*(y-w/2),100*z,100*x,100*(y+w/2),100*z)

                else:
                    # place a cube jump(maybe a spring cube...)
                    x = (next_cube[0]+current_cube[0])/2
                    y = (next_cube[1]+current_cube[1])/2
                    z = (next_cube[2]+current_cube[2])/2
                    l = (next_cube[3]+current_cube[3])/2
                    w = (next_cube[4]+current_cube[4])/2
                    h = (next_cube[5]+current_cube[5])/2

                    #print "append s cube " + str(i)
                    new_cubes.append([x,y,z,l,w,h])

                    self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                    self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z,"i_Boat.BoatFloor_01.M_BoatFloor_02")

            if abs(dz) > max_climb:
                # place a cube jump(maybe a spring cube...)
                x = (next_cube[0]+current_cube[0])/2
                y = (next_cube[1]+current_cube[1])/2
                z = (next_cube[2]+current_cube[2])/2
                l = (next_cube[3]+current_cube[3])/2
                w = (next_cube[4]+current_cube[4])/2
                h = (next_cube[5]+current_cube[5])/2
 
                #print "append s cube " + str(i)
                new_cubes.append([x,y,z,l,w,h])
                self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z, "i_Boat.BoatFloor_01.M_BoatFloor_02")

                # cube vault 
                z = z + h/2 + uniform(3,max_climb)
                l = l/3
                w = w
                h = h/3
 
                #print "append vault cube " + str(i)
                new_cubes.append([x,y,z,l,w,h])
                self.obj.append(obj3.scale3(l,w,h).translate(x,y,z))
                self.m.addCube(100*l,100*w,100*h, 100*x, 100*y, 100*z,"i_Boat.BoatFloor_01.M_BoatFloor_02")
                #   obj.append(obj2.scale3(a[3],a[4],a[5]).translate(a[0],a[1],a[2]))
    
            
    def return_map(self):                
        # return the map 
        return self.m

    def writeMap(self,m):
        for brush in self.m.brushes:
            if brush.t == "cube":
                x = brush.config[3]
                y = brush.config[4]
                brush.config[3] = self.x0 + (x-self.x0)*cos(self.yaw) - (y-self.y0)*sin(self.yaw)
                brush.config[4] = self.y0 + (x-self.x0)*sin(self.yaw) + (y-self.y0)*cos(self.yaw)
                brush.config[7] = self.yaw/(2*pi)*360
            if brush.t == "swing" or brush.t == "balance":
                x = brush.config[0]
                y = brush.config[1]
                brush.config[0] = self.x0 + (x-self.x0)*cos(self.yaw) - (y-self.y0)*sin(self.yaw)
                brush.config[1] = self.y0 + (x-self.x0)*sin(self.yaw) + (y-self.y0)*cos(self.yaw)
                x = brush.config[3]
                y = brush.config[4]
                brush.config[3] = self.x0 + (x-self.x0)*cos(self.yaw) - (y-self.y0)*sin(self.yaw)
                brush.config[4] = self.y0 + (x-self.x0)*sin(self.yaw) + (y-self.y0)*cos(self.yaw)

            m.add(brush.t,brush.config)

    
    def return_obj(self):
        # return the object
        return self.obj
        
if __name__ == "__main__":
    
    m = Map()
    obj = OBJ()
    gen = MakePath((0,0,1000),(50000,0,10000),obj)
    gen.writeMap(m)
    m.setDimension(200000,200000)
    
    #m = gen.return_map()
    #obj = gen.return_obj()
    
    m.save("test.mir")
    m.writeT3D("test.t3d")
    obj.write("test.obj")

