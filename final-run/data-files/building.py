# Building class
from face import *
from traversal import *

class Building(object):


    """location is (x,y,z) tuple, representing the center of the solid, 
    rotation is yaw angle, 
    bounds = (l,h,w) tuple,
    faces is list of faces [T, N, S, E, W], 
    neighbors is ordered list of closest Buildings. 
    inout is a two-ple of booleans representing existence of concxn
    """    
    def __init__(self, location, bounds):
        self.location  = location
        self.bounds    = bounds
        self.faces = []
        for i in range(5):
            self.faces.append(Face(self,i))
        self.neighbors = []
        self.inout     = None


    def __str__(self):
        str = "--Building--"
        str += "Location: "+repr(self.location)
        #str += " --Bounds: "+repr(self.bounds)

        neighbors = "["
        for i in range(len(self.neighbors)):
            neighbors += repr(self.neighbors[i].location)
            if i != len(self.neighbors)-1:
                neighbors += ", "
        neighbors += "]"
        #...

        str += "--Neighbors: " + neighbors

        return str
