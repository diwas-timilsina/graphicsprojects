# Traversal Class
from building import *
from face import *
import random

class Traversal(object):


    """start and endFace are the beginning and ending points of the traversal:
    traversalType is an enum that is defined as such:
    0 = side to self roof.
    1 = side to side
    2 = roof to roof
    3 = roof to side
    4 = roof to self side
    """
    def __init__(self, startFace, endFace, traversalType):
        self.startFace = startFace
        self.endFace = endFace
        self.traversalType = traversalType
        self.startPos = [0,0,0]
        self.endPos = [0,0,0]

    def __str__(self):
        b0 = self.startFace.building.location
        b1 = self.endFace.building.location

        s = "Type=" + str(self.traversalType) + " from "
        s += str(b0)
        s += str(self.startFace.faceType)
        s += " to "
        s += str(b1)
        s += str(self.endFace.faceType)
        s += " start-end: " + repr(self.startPos) + " " + repr(self.endPos)
        s += "\n\t"
        return s

    
    def directionSlope(self, loc, loc2):
        try:

            if -.001 < (loc2[2] - loc[2]) < .001:
                return (0.5,-0.5)[loc2[0] < loc[0]]
            return float(loc2[2] - loc[2])/abs(float(loc2[0] -loc[0])) 
        except:
            #If we have an infinite slope, just use +/-, North or south
            return (1.5,-1.5)[loc2[2] < loc[2]]

    def addStartAndEndPoints(self, in_startPos):
        
        #   if 0: side to self roof
        #       get to building.location[1] + building.bounds[1]/2 .
        if self.traversalType == 0:
            self.startPos = in_startPos
            x0 = in_startPos[0]
            z0 = in_startPos[2]
            y1 = self.endFace.building.location[1] + self.endFace.building.bounds[1]/2
            self.endPos = (x0, y1, z0)
            return

        #   if 1: side to side
        #       do some physics? random? this is easy.
        elif self.traversalType == 1:
            self.startPos = in_startPos
            self.endPos   = pickSidePoint(self.endFace.building,self.endFace.faceType)

        #   if 2: roof to roof
        #       get to closest roof edge. End is other roof edge (the right one).
        elif self.traversalType == 2:
            dirSlope = self.directionSlope(self.startFace.building.location, self.endFace.building.location)
            if dirSlope > 1:
                #north to south
                self.startPos = pickEdgePoint(self.startFace.building,1)
                self.endPos   = pickEdgePoint(self.endFace.building,2)
            elif dirSlope < -1:
                #south to north
                self.startPos =pickEdgePoint(self.startFace.building,2)
                self.endPos   =pickEdgePoint(self.endFace.building,1)

            elif dirSlope > 0 and dirSlope != 1:
                #east to west
                self.startPos =pickEdgePoint(self.startFace.building,3)
                self.endPos   =pickEdgePoint(self.endFace.building,4)

            elif dirSlope < 0 and dirSlope != -1:
                #west to east
                self.startPos =pickEdgePoint(self.startFace.building,4)
                self.endPos   =pickEdgePoint(self.endFace.building,3)

            else:
                #in diagonal. Choose corners
                startAndEnds = pickCornerPoint(self.startFace.building, self.endFace.building)
                self.startPos = startAndEnds[0]
                self.endPos = startAndEnds[1]

        #   if 3: roof to side
        #       get to required roof edge.  Get to point on side? physics? random? easy enough
        elif self.traversalType == 3:
            #Need to figure out which side of the roof we are jumping from...
            dirSlope = self.directionSlope(self.startFace.building.location, self.endFace.building.location)
            if dirSlope > 1:
                #north to south                                                                                                                                           
                self.startPos = pickEdgePoint(self.startFace.building,1)
                self.endPos   = pickSidePoint(self.endFace.building,2)
            elif dirSlope < -1:
                #south to north                                                                                                                                          
                self.startPos =pickEdgePoint(self.startFace.building,2)
                self.endPos   =pickSidePoint(self.endFace.building,1)

            elif dirSlope > 0 and dirSlope != 1:
                #east to west                                                                                                                                            
                self.startPos =pickEdgePoint(self.startFace.building,3)
                self.endPos   =pickSidePoint(self.endFace.building,4)

            elif dirSlope < 0 and dirSlope != -1:
                #west to east                                                                                                                                             
                self.startPos =pickEdgePoint(self.startFace.building,4)
                self.endPos   =pickSidePoint(self.endFace.building,3)

            else:
                #in diagonal. Choose corners                                                                                                                               
                startAndEnds = pickCornerPoint(self.startFace.building, self.endFace.building)
                self.startPos = startAndEnds[0]
                self.endPos = startAndEnds[1]


        #   if 4: roof to self side
        #       get to roof edge. ladder down to some random distance (at least a fourth down?) ?
        elif self.traversalType == 4:
            self.startPos = pickEdgePoint(self.startFace.building,self.endFace.faceType)
            something = random.uniform(0, self.startFace.building.bounds[1])
            self.endPos = [self.startPos[0], self.startPos[1] - something, self.startPos[2]]




def pickCornerPoint(build1, build2):
    #jumping from building1 to building2
    dx = build2.location[0] - build1.location[0]
    dz = build2.location[2] - build1.location[2]
    """ x       x
          \   /
            x
          /   \
        x       x
    """
    if dx > 0 and dz < 0:
        #SE to NW
        x0 = build1.location[0] + build1.bounds[0]/2.0
        y0 = build1.location[1] + build1.bounds[1]/2.0
        z0 = build1.location[2] - build1.bounds[2]/2.0
        
        x1 = build2.location[0] - build2.bounds[0]/2.0
        y1 = build2.location[1] + build2.bounds[1]/2.0
        z1 = build2.location[2] + build2.bounds[2]/2.0
        
        return [    [x0,y0,z0]  ,  [x1,y1,z1]     ]   

    elif dx < 0 and dz < 0:
        #SW to NE
        x0 = build1.location[0] - build1.bounds[0]/2.0
        y0 = build1.location[1] + build1.bounds[1]/2.0
        z0 = build1.location[2] - build1.bounds[2]/2.0

        x1 = build2.location[0] + build2.bounds[0]/2.0
        y1 = build2.location[1] + build2.bounds[1]/2.0
        z1 = build2.location[2] + build2.bounds[2]/2.0

        return [    [x0,y0,z0]  ,  [x1,y1,z1]     ]   

    elif dx > 0 and dz > 0:
        #NE to SW
        x0 = build1.location[0] + build1.bounds[0]/2.0
        y0 = build1.location[1] + build1.bounds[1]/2.0
        z0 = build1.location[2] + build1.bounds[2]/2.0

        x1 = build2.location[0] - build2.bounds[0]/2.0
        y1 = build2.location[1] + build2.bounds[1]/2.0
        z1 = build2.location[2] - build2.bounds[2]/2.0

        return  [  [x0,y0,z0]  , [x1,y1,z1]    ]   

    elif dx < 0 and dz > 0:
        #NW to SE
        x0 = build1.location[0] - build1.bounds[0]/2.0
        y0 = build1.location[1] + build1.bounds[1]/2.0
        z0 = build1.location[2] + build1.bounds[2]/2.0

        x1 = build2.location[0] + build2.bounds[0]/2.0
        y1 = build2.location[1] + build2.bounds[1]/2.0
        z1 = build2.location[2] - build2.bounds[2]/2.0

        return [    [x0,y0,z0]  ,  [x1,y1,z1]     ]
    return
    
def pickEdgePoint(building, faceDir):
    if faceDir == 1:
        #North
        #full width, full height, variable length
        x0 = building.location[0] + random.uniform(-building.bounds[0]/2.0,building.bounds[0]/2.0)
        y0 = building.location[1] + building.bounds[1]/2.0
        z0 = building.location[2] + building.bounds[2]/2.0
        return [x0,y0,z0]
    elif faceDir == 2:
        #South
        x0 = building.location[0] + random.uniform(-building.bounds[0]/2.0,building.bounds[0]/2.0)
        y0 = building.location[1] + building.bounds[1]/2.0
        z0 = building.location[2] - building.bounds[2]/2.0
        return [x0,y0,z0]
    elif faceDir == 3:
        #East
        x0 = building.location[0] + building.bounds[0]/2.0
        y0 = building.location[1] + building.bounds[1]/2.0
        z0 = building.location[2] + random.uniform(-building.bounds[0]/2.0,building.bounds[2]/2.0)
        return [x0,y0,z0]
    elif faceDir == 4:
        #West
        x0 = building.location[0] - building.bounds[0]/2.0
        y0 = building.location[1] + building.bounds[1]/2.0
        z0 = building.location[2] + random.uniform(-building.bounds[0]/2.0,building.bounds[2]/2.0)
        return [x0,y0,z0]
    else:
        #shouldn't ever get here.....
        print "AHHHHHHHH"

def pickSidePoint(building, faceType):
    #Pick point on the edge
    point = pickEdgePoint(building,faceType)
    #Bump down by some random Y (we now have in range (.2--.9)*yBounds
    point[1] -= random.uniform(building.bounds[1]*0.2,building.bounds[1]*0.9)
    return point


def main():
    print "We are done guys!"
    #a = Building([500,500,500],0,[1000,1000,1000])
    #b = Building([2000,500,2000], 0, [1000,1000,1000])



if __name__ == '__main__':
    main()
