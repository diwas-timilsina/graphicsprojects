# Node class for the path

import math
import random
from objectManipulator import *

# Node class
class Node(object):

    def __init__(self,height,location,size,objType,headings,parent,root, end):
        self.height = height
        self.location = location
        self.size = size
        self.objType = objType
        self.headings = headings
        self.parent = parent
        self.root = root
        self.end = end

    def __str__(self):
        str = "-Node--"
        str += "Location: " +repr(self.location)
        str += "---"
        str += "Headings: " +repr(self.headings)
        return str
        
    def to_obj(self):
        main_obj = OBJ(objType=self.objType, filename = "Cube.obj", mtlname = "Cube.mtl")
        main_obj = main_obj.translate(self.location[0], self.location[1], self.location[2]).rotate(0,self.headings[0],0).scaleAxis(self.size[0],self.size[1],self.size[2])
        print "we are printing a node: " + str(main_obj)
        return main_obj
