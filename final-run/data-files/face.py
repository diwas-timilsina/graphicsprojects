from building import *
from traversal import *
from face import *
# Face class
class Face(object):
    
    
    """Building is this face's building
    faceType is the index in the buliding face array: T, N, S, E, W == 0,1,2,3,4
    ins is list of traversals
    outs is list of traversals
    possibleMoves is ordered list traversals that are possible?
    posOnFace is 
    built is a boolean that 
    """    
    def __init__(self, building, faceType):
        self.building = building
        self.faceType = faceType
        self.ins = []
        self.outs = []
        if not faceType == 0:
            #default traversal for a side is to go to roof
            self.possibleTraversals = [ Traversal(self, self.building.faces[0], 0) ]
        else:
            #what's the default traversal for a roof? meow.
            self.possibleTraversals = []
        self.posOnFace = None
        self.built = False
        
        
    def __str__(self):
        str = "--Face--"
        str += "myBuild = " +repr(self.building.location)
        str += "ins: " + repr(self.ins)
        str += "--outs: " + repr(self.outs)
        #...
        return str


    def determinePossibleTraversals(self):
        # determine the possible moves from a face 
        # for a face: a possible traversal 
        # iterate over the scene and find the possible 
        # faces that can be reached from each face and 
        # determine the set of traversal 
        # 

        pass
