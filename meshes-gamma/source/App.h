/**
  \file App.h

  The G3D 10.00 default starter app is configured for OpenGL 3.3 and
  relatively recent GPUs.
 */
#ifndef App_h
#define App_h
#include <G3D/G3DAll.h>
#include "math.h"
/** Application framework. */
class App : public GApp {
protected:

    RealTime                m_lastLightingChangeTime;
    
    float m_cylinderHeight;
    float m_cylinderRadius;
    int m_cylinderSides;

    float m_vScale;
    float m_hScale;
    float m_tScale;
    String m_hFieldImg;

    /** Called from onInit */
    void makeGUI();
    void makeCylinderGUI();
    void makeCylinder();
    void makeHeightField();
    void makeHeightFieldWrapper(double vScale, double hScale, double tScale, String filename);
    void makeHeightFieldGUI();
    void makeCylinderWrapper(double r, double h, int s);
    void makeHeader(FILE* file, double r, double h, int s);
    void makeVertices(FILE* file, double r, double h, int s);
    void makeFaces(FILE* file, double r, double h, int s);

 public:
    
    App(const GApp::Settings& settings = GApp::Settings());

    virtual void onInit() override;
    virtual void onAI() override;
    virtual void onNetwork() override;
    virtual void onSimulation(RealTime rdt, SimTime sdt, SimTime idt) override;
    virtual void onPose(Array<shared_ptr<Surface> >& posed3D, Array<shared_ptr<Surface2D> >& posed2D) override;

    // You can override onGraphics if you want more control over the rendering loop.
    // virtual void onGraphics(RenderDevice* rd, Array<shared_ptr<Surface> >& surface, Array<shared_ptr<Surface2D> >& surface2D) override;

    virtual void onGraphics3D(RenderDevice* rd, Array<shared_ptr<Surface> >& surface3D) override;
    virtual void onGraphics2D(RenderDevice* rd, Array<shared_ptr<Surface2D> >& surface2D) override;

    virtual bool onEvent(const GEvent& e) override;
    virtual void onUserInput(UserInput* ui) override;
    virtual void onCleanup() override;
    
    /** Sets m_endProgram to true. */
    virtual void endProgram();
};

#endif
