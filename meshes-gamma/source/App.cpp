/** \file App.cpp */
#include "App.h"

// Tells C++ to invoke command-line main() function even on OS X and Win32.
G3D_START_AT_MAIN();

int main(int argc, const char* argv[]) {
    {
        G3DSpecification g3dSpec;
        g3dSpec.audio = false;
        initGLG3D(g3dSpec);
    }
           
    GApp::Settings settings(argc, argv);

    // Change the window and other startup parameters by modifying the
    // settings class.  For example:
    settings.window.caption			= argv[0];
    //settings.window.debugContext = true;
    // Some popular resolutions:
    // settings.window.width        = 640;  settings.window.height       = 400;
    // settings.window.width		= 1024; settings.window.height       = 768;
    settings.window.width         = 1280; settings.window.height       = 720;
    //settings.window.width        = 1920; settings.window.height       = 1080;
    // settings.window.width		= OSWindow::primaryDisplayWindowSize().x; settings.window.height = OSWindow::primaryDisplayWindowSize().y;

    // Set to true for a significant performance boost if your app can't render at 60fps,
    // or if you *want* to render faster than the display.
    settings.window.asynchronous	    = true;
    settings.depthGuardBandThickness    = Vector2int16(64, 64);
    settings.colorGuardBandThickness    = Vector2int16(16, 16);
    settings.dataDir			        = FileSystem::currentDirectory();
    settings.screenshotDirectory	    = "../journal/";

    return App(settings).run();
}


App::App(const GApp::Settings& settings) : GApp(settings) {
   
}


// Called before the application loop begins.  Load data here and
// not in the constructor so that common exceptions will be
// automatically caught.
void App::onInit() {
   
    GApp::onInit();
        
    setFrameDuration(1.0f / 60.0f);

    // Call setScene(shared_ptr<Scene>()) or setScene(MyScene::create()) to replace
    // the default scene here.
    
    showRenderingStats      = false;

    makeGUI();
    m_lastLightingChangeTime = 0.0;
    // For higher-quality screenshots:
    // developerWindow->videoRecordDialog->setScreenShotFormat("PNG");
    // developerWindow->videoRecordDialog->setCaptureGui(false);
    developerWindow->cameraControlWindow->moveTo(Point2(developerWindow->cameraControlWindow->rect().x0(), 0));
    loadScene(
		"G3D Cornell Box" // Load something simple
         //    developerWindow->sceneEditorWindow->selectedSceneName()  // Load the first scene encountered 
        );

}

void App::makeGUI() {
    // Initialize the developer HUD (using the existing scene)
    createDeveloperHUD();
    debugWindow->setVisible(true);
    developerWindow->videoRecordDialog->setEnabled(true);

    //    GuiPane* infoPane = debugPane->addPane("Info", GuiTheme::ORNATE_PANE_STYLE);
    
    /*    
    // Example of how to add debugging controls
    infoPane->addLabel("You can add GUI controls");
    infoPane->addLabel("in App::onInit().");
    infoPane->addButton("Exit", this, &App::endProgram);
    infoPane->pack();
    */
    // More examples of debugging GUI controls:
    // debugPane->addCheckBox("Use explicit checking", &explicitCheck);
    // debugPane->addTextBox("Name", &myName);
    
    makeCylinderGUI();
    makeHeightFieldGUI();
    // button = debugPane->addButton("Run Simulator");

    debugWindow->pack();
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


 



void App::onGraphics3D(RenderDevice* rd, Array<shared_ptr<Surface> >& allSurfaces) {
    // This implementation is equivalent to the default GApp's. It is repeated here to make it
    // easy to modify rendering. If you don't require custom rendering, just delete this
    // method from your application and rely on the base class.

    // screenPrintf("Print to the screen from anywhere in a G3D program with this command.");
    if (! scene()) {
        return;
    }
    m_gbuffer->setSpecification(m_gbufferSpecification);
    m_gbuffer->resize(m_framebuffer->width(), m_framebuffer->height());

    // Share the depth buffer with the forward-rendering pipeline
    m_framebuffer->set(Framebuffer::DEPTH, m_gbuffer->texture(GBuffer::Field::DEPTH_AND_STENCIL));
    m_depthPeelFramebuffer->resize(m_framebuffer->width(), m_framebuffer->height());

    Surface::AlphaMode coverageMode = Surface::ALPHA_BLEND;

    // Bind the main framebuffer
    rd->pushState(m_framebuffer); {
        rd->clear();
        rd->setProjectionAndCameraMatrix(activeCamera()->projection(), activeCamera()->frame());

        m_gbuffer->prepare(rd, activeCamera(), 0, -(float)previousSimTimeStep(), m_settings.depthGuardBandThickness, m_settings.colorGuardBandThickness);
        
        // Cull and sort
        Array<shared_ptr<Surface> > sortedVisibleSurfaces;
        Surface::cull(activeCamera()->frame(), activeCamera()->projection(), rd->viewport(), allSurfaces, sortedVisibleSurfaces);
        Surface::sortBackToFront(sortedVisibleSurfaces, activeCamera()->frame().lookVector());
        
        const bool renderTransmissiveSurfaces = false;

        // Intentionally copy the lighting environment for mutation
        LightingEnvironment environment = scene()->lightingEnvironment();
        environment.ambientOcclusion = m_ambientOcclusion;
       
        // Render z-prepass and G-buffer.
        Surface::renderIntoGBuffer(rd, sortedVisibleSurfaces, m_gbuffer, activeCamera()->previousFrame(), activeCamera()->expressivePreviousFrame(), renderTransmissiveSurfaces, coverageMode);

        // This could be the OR of several flags; the starter begins with only one motivating algorithm for depth peel
        const bool needDepthPeel = environment.ambientOcclusionSettings.useDepthPeelBuffer;
        if (needDepthPeel) {
            rd->pushState(m_depthPeelFramebuffer); {
                rd->clear();
                rd->setProjectionAndCameraMatrix(activeCamera()->projection(), activeCamera()->frame());
                Surface::renderDepthOnly(rd, sortedVisibleSurfaces, CullFace::BACK, renderTransmissiveSurfaces, coverageMode, m_framebuffer->texture(Framebuffer::DEPTH), environment.ambientOcclusionSettings.depthPeelSeparationHint);
            } rd->popState();
        }

        if (! m_settings.colorGuardBandThickness.isZero()) {
            rd->setGuardBandClip2D(m_settings.colorGuardBandThickness);
        }        

        // Compute AO
        m_ambientOcclusion->update(rd, environment.ambientOcclusionSettings, activeCamera(), m_framebuffer->texture(Framebuffer::DEPTH), m_depthPeelFramebuffer->texture(Framebuffer::DEPTH), m_gbuffer->texture(GBuffer::Field::CS_NORMAL), m_gbuffer->texture(GBuffer::Field::SS_POSITION_CHANGE), m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);

        const RealTime lightingChangeTime = max(scene()->lastEditingTime(), max(scene()->lastLightChangeTime(), scene()->lastVisibleChangeTime()));
        bool updateShadowMaps = false;
        if (lightingChangeTime > m_lastLightingChangeTime) {
            m_lastLightingChangeTime = lightingChangeTime;
            updateShadowMaps = true;
        }
        // No need to write depth, since it was covered by the gbuffer pass
        rd->setDepthWrite(false);
        // Compute shadow maps and forward-render visible surfaces
        Surface::render(rd, activeCamera()->frame(), activeCamera()->projection(), sortedVisibleSurfaces, allSurfaces, environment, coverageMode, updateShadowMaps, m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness, sceneVisualizationSettings());      
                
        // Call to make the App show the output of debugDraw(...)
        drawDebugShapes();
        const shared_ptr<Entity>& selectedEntity = (notNull(developerWindow) && notNull(developerWindow->sceneEditorWindow)) ? developerWindow->sceneEditorWindow->selectedEntity() : shared_ptr<Entity>();
        scene()->visualize(rd, selectedEntity, sceneVisualizationSettings());

        // Post-process special effects
        m_depthOfField->apply(rd, m_framebuffer->texture(0), m_framebuffer->texture(Framebuffer::DEPTH), activeCamera(), m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);
        
        m_motionBlur->apply(rd, m_framebuffer->texture(0), m_gbuffer->texture(GBuffer::Field::SS_EXPRESSIVE_MOTION), 
                            m_framebuffer->texture(Framebuffer::DEPTH), activeCamera(), 
                            m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);

    } rd->popState();

    // We're about to render to the actual back buffer, so swap the buffers now.
    // This call also allows the screenshot and video recording to capture the
    // previous frame just before it is displayed.
    swapBuffers();

	// Clear the entire screen (needed even though we'll render over it, since
    // AFR uses clear() to detect that the buffer is not re-used.)
    rd->clear();

    // Perform gamma correction, bloom, and SSAA, and write to the native window frame buffer
    m_film->exposeAndRender(rd, activeCamera()->filmSettings(), m_framebuffer->texture(0));
}


void App::onAI() {
    GApp::onAI();
    // Add non-simulation game logic and AI code here
}


void App::onNetwork() {
    GApp::onNetwork();
    // Poll net messages here
}


void App::onSimulation(RealTime rdt, SimTime sdt, SimTime idt) {
    GApp::onSimulation(rdt, sdt, idt);

    // Example GUI dynamic layout code.  Resize the debugWindow to fill
    // the screen horizontally.
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


bool App::onEvent(const GEvent& event) {
    // Handle super-class events
    if (GApp::onEvent(event)) { return true; }

    // If you need to track individual UI events, manage them here.
    // Return true if you want to prevent other parts of the system
    // from observing this specific event.

    // if ((event.type == GEventType::KEY_DOWN) && (event.key.keysym.sym == GKey::TAB)) { ... return true; }

    return false;
}


void App::onUserInput(UserInput* ui) {
    GApp::onUserInput(ui);
    (void)ui;
    // Add key handling here based on the keys currently held or
    // ones that changed in the last frame.
}


void App::onPose(Array<shared_ptr<Surface> >& surface, Array<shared_ptr<Surface2D> >& surface2D) {
    GApp::onPose(surface, surface2D);

    // Append any models to the arrays that you want to later be rendered by onGraphics()
}


void App::onGraphics2D(RenderDevice* rd, Array<shared_ptr<Surface2D> >& posed2D) {
    // Render 2D objects like Widgets.  These do not receive tone mapping or gamma correction.
    Surface2D::sortAndRender(rd, posed2D);
}


void App::onCleanup() {
    // Called after the application loop ends.  Place a majority of cleanup code
    // here instead of in the constructor so that exceptions can be caught.
}


void App::endProgram() {
    m_endProgram = true;
}

void App::makeHeader(FILE* file, double r, double h, int s){
    fprintf(file,"OFF\n");
    fprintf(file,"# Cylinder \n");
    fprintf(file,"%d %d %d\n",(2*s),2*s+2*(s-2),2*s+2*(s-3)+2*s);
    fprintf(file,"\n");

}

void App::makeFaces(FILE* file, double r, double h, int s){
        
    for (int i =0 ; i < (s-2); ++i){
        fprintf(file, "3 %i %i %i", 2*i+4, 2*i+2, 0);
        fprintf(file,"\n");
        fprintf(file,"3 %i %i %i", 1, 2*i+3, 2*i+5);
        fprintf(file,"\n");
    } 

    int j(0);
    while(j < 2*s) {
        fprintf(file,"3 %i %i %i", (j+3)%(2*s), (j+1)%(2*s), j);
        fprintf(file,"\n");
        fprintf(file,"3 %i %i %i", j, (j+2)%(2*s),(j+3)%(2*s)); 
        fprintf(file,"\n");
        j+=2;
    }
}

void App::makeVertices(FILE* file, double r, double h, int s){
    
    double x; 
    double z;
    for (int i =0; i < s; ++i){
        x = r * cos((2*3.14*i)/s);
        z = r * sin((2*3.14*i)/s);

        // Generates top and bottom disk
        fprintf(file,"%f %f %f",x,h/2,z);
        fprintf(file,"\n"); 
        fprintf(file,"%f %f %f",x,-h/2,z);
        fprintf(file,"\n"); 
    }      
}

void App::makeCylinderGUI() { 

    shared_ptr<GuiWindow> cylWindow = GuiWindow::create("Cylinder Parameters",
                                                        shared_ptr< GuiTheme > (),
                                                        Rect2D::xywh(100,100,100,50),
                                                        GuiTheme::NORMAL_WINDOW_STYLE,
                                                        GuiWindow::HIDE_ON_CLOSE); 
   
    GuiPane* cylPane = cylWindow->pane();

    cylPane->addNumberBox("height", &m_cylinderHeight, "m", GuiTheme::LINEAR_SLIDER, 1.0f, 10.0f);
    cylPane->addNumberBox("radius", &m_cylinderRadius, "m", GuiTheme::LINEAR_SLIDER, 1.0f, 10.0f);
    cylPane->addNumberBox("sides", &m_cylinderSides, " ", GuiTheme::LINEAR_SLIDER, 3, 10);
    cylPane->addButton("Generate Cylinder", this, &App::makeCylinder);
    
    addWidget(cylWindow);
}

void App::makeCylinderWrapper(double r, double h, int s){

    FILE* file = fopen("cylinder.off","w+");
    debugAssert(file != NULL);

    makeHeader(file, r,h,s);  
    makeVertices(file, r,h,s);
    makeFaces(file, r,h,s);

    fclose(file);
    file= NULL;    
}

void App::makeCylinder() {
    makeCylinderWrapper(m_cylinderRadius, m_cylinderHeight, m_cylinderSides);
    G3D::ArticulatedModel::clearCache();
    loadScene(scene()->name());
}

void App::makeHeightFieldGUI() { 

    shared_ptr<GuiWindow> hfWindow = GuiWindow::create("Heightfield Parameters", 
                                                       shared_ptr< GuiTheme > (),
                                                       Rect2D::xywh(400,100,300,50),
                                                       GuiTheme::NORMAL_WINDOW_STYLE,
                                                       GuiWindow::HIDE_ON_CLOSE); 
    GuiPane* hfPane = hfWindow->pane();

    hfPane->addTextBox("Source File", &m_hFieldImg);
    hfPane->addNumberBox("Vertical Scale", &m_vScale, " ", GuiTheme::LINEAR_SLIDER, 1.0f, 200.0f);
    hfPane->addNumberBox("Horiz. Scale", &m_hScale, " ", GuiTheme::LINEAR_SLIDER, 1.0f, 5.0f);
    hfPane->addNumberBox("Texture Scale", &m_tScale, " ", GuiTheme::LINEAR_SLIDER, 1.0f, 5.0f);
    hfPane->addButton("Create HeightField", this, &App::makeHeightField);
    
    addWidget(hfWindow);
}

void App::makeHeightField(){
    makeHeightFieldWrapper(m_vScale, m_hScale, m_tScale, m_hFieldImg);       
    G3D::ArticulatedModel::clearCache();
    loadScene(scene()->name()); 
}
void App::makeHeightFieldWrapper(double vScale, double hScale, double tScale, String filename ="cloudsHeightfield2.png"){

    shared_ptr<Image> image = Image::fromFile(filename,ImageFormat::AUTO());
    
    int depth = image->height();
    int width = image->width();
    
    FILE* file = fopen("heightfield.off","w+");
    debugAssert(file != NULL);    
    
    // header of the heightfield file
    fprintf(file,"STOFF\n");
    fprintf(file,"#Height Field with Texture \n");
    fprintf(file,"%d %d %d\n", depth*width, ((depth-1)*(width-1))*2, ((depth-1)*(width-1))*6);
    fprintf(file,"\n");
    
    float inc = tScale/width;
    
    //points and texture coordinates
    for (int z = 0; z < depth; ++z){
        for (int x =0 ; x< width; ++x){
            const Color1& c (image->get<Color1>(x,z));
            fprintf(file,"%f %f %f %f %f\n", x*hScale,c.value * vScale,z*hScale, x*inc,z*inc);
        }
    }    

    //faces for the file
    for (int j = 0; j< width-1; ++j){
        for (int k = 0; k <depth-1; ++k){  
            fprintf(file,"3 %d %d %d\n",k*width+j,(k+1)*width+j,(k+1)*width+1+j);
            fprintf(file,"3 %d %d %d\n",k*width+j,(k+1)*width+1+j,1+(k*width)+j);    
        }
    }
    
    fprintf(file,"\n");
    fclose(file);
    file=NULL;
} 
