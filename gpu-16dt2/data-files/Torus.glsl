#ifndef Torus_glsl // -*- c++ -*-
#define Torus_glsl

#include "Surfel.glsl"

struct Torus {
    Point3      center;
    float       radiusMinor;
    float       radiusMajor;
    Material    material;
};

struct Cylinder {
    Point3      center;
    float       radius;
    float       halfExtent;
    Material    material;
};




struct Wheel{
    Point3 center;
    float radiusMinor;
    float radiusMajor; 
    Material material;
};


struct Plane{
    Point3 point;
    Vector3 normal;
    Material material;
};




/**
float power8(float x) {
 float y = x*x;
 float z = x*x
 return z*z;    
}


float length8(Vector2 v){
    return pow(power8(v.x) + power8(v.y), 1.0/8.0);
}
*/


/**
 Signed distance function for a sphere.  Negative inside of the s   phere.

float wheelDistanceEstimator(Wheel wheel, Point3 P, inout float currentBest, inout Material material) {
    Point3 C = wheel.center;
    float estimate = length8(Vector2(length(P.xz - C.xz) - wheel.radiusMinor, P.y - C.y)) - wheel.radiusMajor;
    if (estimate < currentBest){
        currentBest = estimate;
        material = wheel.material;
    }
    return estimate;
}
*/


/**
 Signed distance function for a cylinder.
*/
float cylinderDistanceEstimator( Cylinder cylinder, Point3 P, inout float currentBest, inout Material material) {
   Point3 C = cylinder.center;  
   Vector2 d = abs(Vector2(length(P.xz - C.xz), P.y - C.y)) - Vector2(cylinder.radius,cylinder.halfExtent); 
   float estimate = min(maxComponent(d),0) + length(max(d,Vector2(0,0)));
    if (estimate < currentBest){
        currentBest = estimate;
        material = cylinder.material;
    }
    
    return estimate;
}

/**
 * distance function for a plane
 */

float planeDistanceEstimator( Plane plane, Point3 P, inout float currentBest, inout Material material) {
   Point3 C = plane.point;  
   float estimate = dot(P-C, plane.normal);
    if (estimate < currentBest){
        currentBest = estimate;
        material = plane.material;
    }
    return estimate;
}



/**
 Signed distance function for a sphere.  Negative inside of the sphere.
*/
float torusDistanceEstimator( Torus tor, Point3 P, inout float currentBest, inout Material material) {
   
    Point3 C = tor.center;
    float estimate = length(vec2(length(P.xz - C.xz) - tor.radiusMinor, P.y - C.y)) - tor.radiusMajor; 
    if (estimate < currentBest){
        currentBest = estimate;
        material = tor.material;
    }
    
    return estimate;
}
#endif
