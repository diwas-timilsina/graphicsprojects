#ifndef Primitives_glsl // -*- c++ -*-
#define Primitives_glsl

#include "Surfel.glsl"


struct RoundedBox {
    Point3      center;
    float       roundingRadius;
    Vector3     b;                  //the vector from the center to the first quardrant corner 
    Material    material;
};


struct Sphere {
    Point3      center;
    float       radius;
    Material    material;
};


struct Cone {
    Point3      center;
    Vector3     b;
    Material    material;
};


struct Torus {
    Point3      center;
    float       radiusMinor;
    float       radiusMajor;
    Material    material;
};

struct Cylinder {
    Point3      center;
    float       radius;
    float       halfExtent;
    Material    material;
};



struct Wheel{
    Point3 center;
    float radiusMinor;
    float radiusMajor; 
    Material material;
};


struct Plane{
    Point3 point;
    Vector3 normal;
    Material material;
};




/**
 Signed distance function for a sphere.  Negative inside of the sphere.
*/
float sphereDistanceEstimator(Sphere sphere, Point3 P, inout float currentBest, inout Material material) {
    P -= sphere.center;
    float estimate = length(P) - sphere.radius;
    if (estimate < currentBest){
        currentBest = estimate;
        material = sphere.material;
    }
    
    return estimate;
}


/**
 distance function for a cylinder.
*/
float cylinderDistanceEstimator( Cylinder cylinder, Point3 P, inout float currentBest, inout Material material) {
   Point3 C = cylinder.center;  
   Vector2 d = abs(Vector2(length(P.xz - C.xz), P.y - C.y)) - Vector2(cylinder.radius,cylinder.halfExtent); 
   float estimate = min(maxComponent(d),0) + length(max(d,Vector2(0,0)));
    if (estimate < currentBest){
        currentBest = estimate;
        material = cylinder.material;
    }
    
    return estimate;
}

/**
 * distance function for a plane
 */

float planeDistanceEstimator( Plane plane, Point3 P, inout float currentBest, inout Material material) {
   Point3 C = plane.point;  
   float estimate = dot(P-C, plane.normal);
    if (estimate < currentBest){
        currentBest = estimate;
        material = plane.material;
    }
    return estimate;
}



// torus distance estimator 
float torusDistanceEstimator( Torus tor, Point3 P, inout float currentBest, inout Material material) {
   
    Point3 C = tor.center;
    float estimate = length(vec2(length(P.xz - C.xz) - tor.radiusMinor, P.y - C.y)) - tor.radiusMajor; 
    if (estimate < currentBest){
        currentBest = estimate;
        material = tor.material;
    }
    
    return estimate;
}



// cone pointing to positoive Z
float coneDistanceEstimatorZ(Cone cone, Point3 P, inout float currentBest, inout Material material) {
    Vector3 p = (P - cone.center);
    Vector2 q = Vector2(length(p.xy), p.z);
    Vector2 v = Vector2(cone.b.z*cone.b.y/cone.b.x, -cone.b.z);
    Vector2 w = v - q;
    Vector2 vv = Vector2(dot(v, v), v.x*v.x);
    Vector2 qv = Vector2(dot(v, w), v.x*w.x);
    Vector2 d = max(qv, 0.0)*qv/vv;
    float estimate = sqrt(dot(w, w) - max(d.x, d.y))*sign(max(q.y*v.x-q.x*v.y,w.y));

    if (estimate <currentBest){
        currentBest = estimate;
        material = cone.material;
    }
    return estimate;
}


//distance estimator of cone pointing in positive y
float coneDistanceEstimatorY(Cone cone, Point3 P, inout float currentBest, inout Material material) {
    Vector3 p = (P - cone.center);
    Vector2 q = Vector2(length(p.xz), p.y);
    Vector2 v = Vector2(cone.b.z*cone.b.y/cone.b.x, -cone.b.z);
    Vector2 w = v - q;
    Vector2 vv = Vector2(dot(v, v), v.x*v.x);
    Vector2 qv = Vector2(dot(v, w), v.x*w.x);
    Vector2 d = max(qv, 0.0)*qv/vv;
    float estimate = sqrt(dot(w, w) - max(d.x, d.y))*sign(max(q.y*v.x-q.x*v.y,w.y));

    if (estimate <currentBest){
        currentBest = estimate;
        material = cone.material;
    }
    return estimate;
}

/**
 distance function for a roundedbox. 
*/
float roundedBoxDistanceEstimator(RoundedBox box, Point3 P, inout float currentBest, inout Material material) {
    Vector3 v = max(abs(P-box.center)-box.b, Vector3(0,0,0));
    //P -= sphere.center;
    float estimate = length(v) - box.roundingRadius;
    if (estimate < currentBest){
        currentBest = estimate;
        material = box.material;
    }
    
    return estimate;
}



#endif
