#ifndef Cone_glsl // -*- c++ -*-
#define Cone_glsl

#include "Surfel.glsl"

struct Cone {
    Point3      center;
    Vector3     b;
    Material    material;
};

/**
 Signed distance function for a sphere.  Negative inside of the sphere.
*/
float coneDistanceEstimatorZ(Cone cone, Point3 P, inout float currentBest, inout Material material) {
    Vector3 p = (P - cone.center);
    Vector2 q = Vector2(length(p.xy), p.z);
    Vector2 v = Vector2(cone.b.z*cone.b.y/cone.b.x, -cone.b.z);
    Vector2 w = v - q;
    Vector2 vv = Vector2(dot(v, v), v.x*v.x);
    Vector2 qv = Vector2(dot(v, w), v.x*w.x);
    Vector2 d = max(qv, 0.0)*qv/vv;
    float estimate = sqrt(dot(w, w) - max(d.x, d.y))*sign(max(q.y*v.x-q.x*v.y,w.y));

    if (estimate <currentBest){
        currentBest = estimate;
        material = cone.material;
    }
    return estimate;
}

float coneDistanceEstimatorY(Cone cone, Point3 P, inout float currentBest, inout Material material) {
    Vector3 p = (P - cone.center);
    Vector2 q = Vector2(length(p.xz), p.y);
    Vector2 v = Vector2(cone.b.z*cone.b.y/cone.b.x, -cone.b.z);
    Vector2 w = v - q;
    Vector2 vv = Vector2(dot(v, v), v.x*v.x);
    Vector2 qv = Vector2(dot(v, w), v.x*w.x);
    Vector2 d = max(qv, 0.0)*qv/vv;
    float estimate = sqrt(dot(w, w) - max(d.x, d.y))*sign(max(q.y*v.x-q.x*v.y,w.y));

    if (estimate <currentBest){
        currentBest = estimate;
        material = cone.material;
    }
    return estimate;
}


#endif
