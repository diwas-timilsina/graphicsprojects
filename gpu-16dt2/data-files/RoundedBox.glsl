#ifndef RoundedBox_glsl // -*- c++ -*-
#define RoundedBox_glsl

#include "Surfel.glsl"

struct RoundedBox {
    Point3      center;
    float       roundingRadius;
    Vector3     b;                  //the vector from the center to the first quardrant corner 
    Material    material;
};



/* Updates the distance to the first surfel, if closer than distance,
  and returns true on intersection or false if no intersection.
  This uses the implicit equation for the surface of the sphere.       
  See Graphics Codex [raySphr] */

/**
bool intersect(RoundedBox box, Point3 P, Vector3 w, inout float distance, inout Surfel surfel) {
     Point3  C = box.center;
     float   r = box.roundingRadius;
     Vector3 y = box.b;   
 
     Vector3 v = abs(P - C);
     Vector3 x =  v - y;
    
     float b = 2.0 * dot(w, x);
     float c = dot(x, x) - square(r);
     float d = square(b) - 4.0 * c;
     if (d < 0.0) { return false; }
     
     float dsqrt = sqrt(d);
     
     // Choose the first positive intersection
     float t = min(infIfNegative((-b - dsqrt) / 2.0),   
                   infIfNegative((-b + dsqrt) / 2.0));
    
     if (t < distance) {        
         surfel.position = P + w * t;
         surfel.normal   = normalize(surfel.position - C);
         surfel.material = box.material;
         
         distance = t;
         return true;
     } else {
         return false;
     }
}
*/

/**
 Unsigned distance function for a roundedbox.  Negative inside of the sphere.
*/
float roundedBoxDistanceEstimator(RoundedBox box, Point3 P, inout float currentBest, inout Material material) {
    Vector3 v = max(abs(P-box.center)-box.b, Vector3(0,0,0));
    //P -= sphere.center;
    float estimate = length(v) - box.roundingRadius;
    if (estimate < currentBest){
        currentBest = estimate;
        material = box.material;
    }
    
    return estimate;
}

#endif

