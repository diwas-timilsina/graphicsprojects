#include "b23d.h"
#include <string>

std::string fname;

b23d::b23d(std::string name)
{
     vertices = Array<Point2>();
     fname = name;
}


Array<Point2> b23d::addVertex(Point2 point)
{
    //Entered in counterclockwise order
    vertices.append(point);
    return vertices;
}

void b23d::writeOBJ()
{
    std::string name = "../data-files/" + fname + ".obj";
    printf(name.c_str());
    printf("\n");
    FILE * f = fopen(name.c_str(),"wt");   
        
    debugAssert(f != NULL);
    fprintf(f,"# Obj\n");
    fprintf(f,"o ");
    fprintf(f,fname.c_str());
    // fprintf(f,"%d %d %d\n",(2*s),2*s+2*(s-2),2*s+2*(s-3)+2*s);
    fprintf(f,"\n");


    //make the vertices
    for(int i = 0; i < vertices.size(); ++i){
        fprintf(f,"v %f %f %f", vertices[i].x, vertices[i].y, 0.1);
        fprintf(f,"\n");
        fprintf(f,"v %f %f %f", vertices[i].x,vertices[i].y,-0.1);
        fprintf(f,"\n");
    }

    fprintf(f,"\n");
    fprintf(f,"s off\n");
    
    //faces
    for (int j = 0; j < (vertices.size()-2); ++j){
        //  fprintf(f,"f %d %d %d\n", 2*j+4+1, 2*j+2+1, 1);
        fprintf(f,"f %d %d %d\n",0+1, 2*j+2+1, 2*j+4+1);
        //fprintf(f,"f %d %d %d\n", 2, 2*j+4, 2*j+5+1);
        fprintf(f,"f %d %d %d\n", 2*j+3+1, 1+1, 2*j+5+1);
    }
    
    int k(0);
    while (k < 2*(vertices.size())-2){
        //fprintf(f,"f %d %d %d\n", (k+4)%(2*vertices.size()), (k+2)%(2*vertices.size()), k+1);
        if ((k+4)%(2*vertices.size()) == 0){
            fprintf(f,"f %d %d %d %d\n", k+1, (k+2)%(2*vertices.size()), 2*vertices.size(), (k+3)%(2*vertices.size()));
        } else {
            fprintf(f,"f %d %d %d %d\n", k+1, (k+2)%(2*vertices.size()), (k+4)%(2*vertices.size()), (k+3)%(2*vertices.size()));
            //fprintf(f,"f %d %d %d\n", k+1, (k+3)%(2*vertices.size()), (k+4)%(2*vertices.size()));
        //      fprintf(f,"f %d %d %d\n", k, (k+3)%(2*vertices.size()), (k+2)%(2*vertices.size()));
        }
        k+=2;
    }

    fprintf(f,"f %d %d %d %d\n", k+1, 2*vertices.size(), (k+4)%(2*vertices.size()), (k+3)%(2*vertices.size()));
    
    fclose(f);
    f = NULL;
}

b2PolygonShape b23d::makeB2()
{
    b2PolygonShape boxShape;

    b2Vec2 *vertices2 = new b2Vec2[vertices.size()];

    for(int i = 0; i < vertices.size(); ++i){
        vertices2[i].Set(vertices[i].x,vertices[i].y);
    }
    
    int32 count =vertices.size();
    boxShape.Set(vertices2,count);
    return boxShape;
}
