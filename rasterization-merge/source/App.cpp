#include "App.h"
#include "Box2D/Box2d.h"
#include "b23d.h"
#include <cstdio>
#include <string>

G3D_START_AT_MAIN();

int main(int argc, const char* argv[]) {

    G3D::G3DSpecification spec;
    spec.audio = true;
    initGLG3D(spec);

    initGLG3D();

    GApp::Settings settings(argc, argv);

    settings.window.caption     = "Jetpack Jousting";
    settings.window.width       = 1280;
    settings.window.height      = 720;
    
    
    return App(settings).run();
}


App::App(const GApp::Settings& settings) :GApp(settings) {
    sims = 0.0;
}

void App::newGame() {
    paused = false;
    bulletTime = false;
    restartable = false;
    livesA = 5;
    livesB = 5;
    AhitB = 0;
    BhitA = 0;

    fixtureNames.clear();
    simBodies.clear();
    entityList.clear();

    fixtureNames.append("leg");
    fixtureNames.append("heart");
    fixtureNames.append("tip");
    fixtureNames.append("spike");
    fixtureNames.append("mainJet");
    fixtureNames.append("body");
    fixtureNames.append("head");

    //--- Init World ---
    b2Vec2 gravity(0,-8.5);

    simWorld = new b2World(gravity);

    b2BodyDef myBody;
    myBody.type = b2_dynamicBody;

    myBody.position.Set(-50,0);
    myBody.angle = 0;
    b2Body* player1 = simWorld->CreateBody(&myBody);

    myBody.position.Set(50,0);
    myBody.angle = 0;
    b2Body* player2 = simWorld->CreateBody(&myBody);

    b2PolygonShape boxShape;
    b2FixtureDef boxFixtureDef;

    //Head Guard ------------------------------------------
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d headGuard1 = b23d("headGuard1");
    headGuard1.addVertex(Point2(3,18.5));
    headGuard1.addVertex(Point2(3,17));
    headGuard1.addVertex(Point2(4,17));
    headGuard1.addVertex(Point2(4,18.5));
    boxShape = headGuard1.makeB2();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);

    b23d headGuard2 = b23d("headGuard2");
    headGuard2.addVertex(Point2(13,18.5));
    headGuard2.addVertex(Point2(13,17));
    headGuard2.addVertex(Point2(12,17));
    headGuard2.addVertex(Point2(12,18.5));
    boxShape = headGuard2.makeB2();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);


    //Head Code --------------------------------------------
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d head1 = b23d("head1");
    head1.addVertex(Point2(4,13));
    head1.addVertex(Point2(8,13));
    head1.addVertex(Point2(8,18));
    head1.addVertex(Point2(4,18));
    boxShape = head1.makeB2();
    //head1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);

    b23d head2 = b23d("head2");
    head2.addVertex(Point2(12,13));
    head2.addVertex(Point2(8,13));
    head2.addVertex(Point2(8,18));
    head2.addVertex(Point2(12,18));
    boxShape = head2.makeB2();
    //head2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Body Code ---------------------------------------------
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d body1 = b23d("body1");
    body1.addVertex(Point2(4,6));
    body1.addVertex(Point2(9,6));
    body1.addVertex(Point2(9,14));
    body1.addVertex(Point2(4,14));
    boxShape = body1.makeB2();
    //body1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);

    b23d body2 = b23d("body2");
    body2.addVertex(Point2(12,6));
    body2.addVertex(Point2(7,6));
    body2.addVertex(Point2(7,14));
    body2.addVertex(Point2(12,14));
    boxShape = body2.makeB2();
    // body2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Main Jet Code ---------------------------------------
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d mainJet1 = b23d("mainJet1");
    mainJet1.addVertex(Point2(1,10));
    mainJet1.addVertex(Point2(2,10));
    mainJet1.addVertex(Point2(2,7));
    mainJet1.addVertex(Point2(5,7));
    mainJet1.addVertex(Point2(5,14));
    mainJet1.addVertex(Point2(1,14));
    boxShape = mainJet1.makeB2();
    //mainJet1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);

    b23d mainJet2 = b23d("mainJet2");
    mainJet2.addVertex(Point2(15,10));
    mainJet2.addVertex(Point2(14,10));
    mainJet2.addVertex(Point2(14,7));
    mainJet2.addVertex(Point2(11,7));
    mainJet2.addVertex(Point2(11,14));
    mainJet2.addVertex(Point2(15,14));
    boxShape = mainJet2.makeB2();
    //mainJet2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Spike Code --------------------------------------
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d spike1 = b23d("spike1");
    spike1.addVertex(Point2(2,13));
    spike1.addVertex(Point2(4,13));
    spike1.addVertex(Point2(3,28));
    boxShape = spike1.makeB2();
    //spike1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);

    b23d spike2 = b23d("spike2");
    spike2.addVertex(Point2(12,13));
    spike2.addVertex(Point2(14,13));
    spike2.addVertex(Point2(13,28));

    boxShape = spike2.makeB2();
    //spike2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);


    //Spike Tip Code ------------------------------
    boxFixtureDef.isSensor = true;
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d tip1 = b23d("tip1");
    tip1.addVertex(Point2(2.9,25));
    tip1.addVertex(Point2(3.1,25));
    tip1.addVertex(Point2(3,28));
    boxShape = tip1.makeB2();
    //tip1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);


    b23d tip2 = b23d("tip2");
    tip2.addVertex(Point2(13.1,25));
    tip2.addVertex(Point2(12.9,25));
    tip2.addVertex(Point2(13,28));
    boxShape = tip2.makeB2();
    //tip2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Jet Heart ---------------------------------
    boxFixtureDef.isSensor = true;
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d heart1 = b23d("heart1");
    heart1.addVertex(Point2(0,6));
    heart1.addVertex(Point2(2,6));
    heart1.addVertex(Point2(2,10));
    heart1.addVertex(Point2(0,10));
    boxShape = heart1.makeB2();
    //heart1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);


    b23d heart2 = b23d("heart2");
    heart2.addVertex(Point2(16,6));
    heart2.addVertex(Point2(14,6));
    heart2.addVertex(Point2(14,10));
    heart2.addVertex(Point2(16,10));
    //heart2.writeOBJ();
    boxShape = heart2.makeB2();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Leggos --------------------------------------
    boxFixtureDef.isSensor = false;
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;

    b23d leg1 = b23d("leg1");
    leg1.addVertex(Point2(4,0));
    leg1.addVertex(Point2(8,0));
    leg1.addVertex(Point2(8,8));
    leg1.addVertex(Point2(4,8));
    boxShape = leg1.makeB2();
    //leg1.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player1->CreateFixture(&boxFixtureDef);


    b23d leg2 = b23d("leg2");
    leg2.addVertex(Point2(12,0));
    leg2.addVertex(Point2(8,0));
    leg2.addVertex(Point2(8,8));
    leg2.addVertex(Point2(12,8));
    boxShape = leg2.makeB2();
    //leg2.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    player2->CreateFixture(&boxFixtureDef);

    //Walls --------------------------------------
    boxFixtureDef.density = 0.0000001;
    myBody.type = b2_staticBody;
    myBody.angle = 0;
    myBody.position.Set(0, -130);

    b23d outerWall = b23d("wall");
    outerWall.addVertex(Point2(-150,-70));
    outerWall.addVertex(Point2(150,-70));
    outerWall.addVertex(Point2(150,70));
    outerWall.addVertex(Point2(-150,70));
    boxShape = outerWall.makeB2();
    //outerWall.writeOBJ();

    boxFixtureDef.shape = &boxShape;
    b2Body* botWall = simWorld->CreateBody(&myBody);
    botWall->CreateFixture(&boxFixtureDef);

    myBody.position.Set(0,130);

    b2Body* topWall = simWorld->CreateBody(&myBody);
    topWall->CreateFixture(&boxFixtureDef);

    b23d outerWall1 = b23d("wall1");
    outerWall1.addVertex(Point2(-70,-150));
    outerWall1.addVertex(Point2(70,-150));
    outerWall1.addVertex(Point2(70,150));
    outerWall1.addVertex(Point2(-70,150));
    boxShape = outerWall1.makeB2();
    //outerWall1.writeOBJ();

    myBody.position.Set(-160,0);

    b2Body* sideWall1 = simWorld->CreateBody(&myBody);
    sideWall1->CreateFixture(&boxFixtureDef);

    myBody.position.Set(160,0);

    b2Body* sideWall2 = simWorld->CreateBody(&myBody);
    sideWall2->CreateFixture(&boxFixtureDef);

    //End Entities -------------------------------
    //Set position and angle here.
    player1->SetTransform(player1->GetPosition(),0);
    player2->SetTransform(player2->GetPosition(),0);

    simBodies.append(player1);
    simBodies.append(player2);

    G3D::ArticulatedModel::clearCache();
    G3D::Texture::clearCache();
    G3D::UniversalMaterial::clearCache();
    
    loadScene(System::findDataFile("empty.Scene.Any"));
    song1 = G3D::Sound::create(System::findDataFile("mainSound.mp3"),true);
    song2 = G3D::Sound::create(System::findDataFile("slowMotion.mp3"),true);
    popSound = G3D::Sound::create(System::findDataFile("pop.mp3"),false);
    themeSong = song1->play();
    bulletTimeSong = song2->play(0.0);
}


void App::onInit() {
    GApp::onInit();
    showRenderingStats    = false;
    setFrameDuration(1.0f / 60);
    makeGUI();

    loadScene(System::findDataFile("empty.Scene.Any"));
    newGame();
}

void App::makeGUI() {
    // Initialize the developer HUD
    createDeveloperHUD();

    debugWindow->setVisible(false);
    developerWindow->videoRecordDialog->setEnabled(true);
    developerWindow->sceneEditorWindow->setVisible(false);
    developerWindow->cameraControlWindow->setVisible(false);
    developerWindow->videoRecordDialog->setHalfSize(false);

    debugWindow->pack();
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


void App::onUserInput(UserInput* ui) {
    float speed = 16;
    float turnSpeed = 1;
    float turnDecay = .9;

    GApp::onUserInput(ui);

    b2Body* player1sim = simBodies[0];
    b2Body* player2sim = simBodies[1];

    if (ui->keyDown(GKey('n')) && restartable){
        newGame();
    }

    if (ui->keyDown(GKey('w'))){
        const shared_ptr<Entity>& flame1 = scene()->entity("flame1");

        b2Vec2 vel = player1sim->GetLinearVelocity();
        float aVel = player1sim->GetAngularVelocity();
        float bodyAngle = player1sim->GetAngle();
        vel.x = -1.0 * speed * sin(bodyAngle);
        vel.y = speed * cos(bodyAngle);
        player1sim->SetLinearVelocity(vel);
        player1sim->SetAngularVelocity(turnDecay * aVel);
        b2Vec2 flamePos = player1sim->GetWorldVector(b2Vec2(1,8));
        float gAngle1 = bodyAngle / (pi()) * 180;
        flame1->setFrame( CFrame::fromXYZYPRDegrees(flamePos.x + player1sim->GetPosition().x,flamePos.y + player1sim->GetPosition().y,-20.0,180,0,-gAngle1 + 180));
    }else{
        const shared_ptr<Entity>& flame1 = scene()->entity("flame1");
        flame1->setFrame( CFrame::fromXYZYPRDegrees(0,0,200.0,0,0,0));
    }

    if (ui->keyDown(GKey('a'))){
        player1sim->SetAngularVelocity(turnSpeed);
    }
    if (ui->keyDown(GKey('d'))){
        player1sim->SetAngularVelocity(-turnSpeed);
    }
    if (ui->keyReleased(GKey('a'))){
        player1sim->SetAngularVelocity(0);
    }
    if (ui->keyReleased(GKey('d'))){
        player1sim->SetAngularVelocity(0);
    }
    if (ui->keyDown(GKey::UP)){
        const shared_ptr<Entity>& flame2 = scene()->entity("flame2");

        b2Vec2 vel = player2sim->GetLinearVelocity();
        float aVel = player2sim->GetAngularVelocity();
        float bodyAngle = player2sim->GetAngle();
        vel.x = -1.0 * speed * sin(bodyAngle);
        vel.y = speed * cos(bodyAngle);
        player2sim->SetLinearVelocity(vel);
        player2sim->SetAngularVelocity(turnDecay * aVel);

        b2Vec2 flamePos = player2sim->GetWorldVector(b2Vec2(15,7));
        float gAngle2 = bodyAngle / (pi()) * 180;
        b2Vec2 pos = player2sim->GetPosition();
        flame2->setFrame( CFrame::fromXYZYPRDegrees(pos.x + flamePos.x,pos.y + flamePos.y,-20.0,0,180,-gAngle2));
    }else{
        const shared_ptr<Entity>& flame2 = scene()->entity("flame2");
        flame2->setFrame( CFrame::fromXYZYPRDegrees(0,0,200.0,0,0,0));
    }

    if (ui->keyDown(GKey::LEFT)){
        player2sim->SetAngularVelocity(turnSpeed);
    }
    if (ui->keyDown(GKey::RIGHT)){
        player2sim->SetAngularVelocity(-turnSpeed);
    }
    if (ui->keyReleased(GKey::LEFT)){
        player2sim->SetAngularVelocity(0);
    }
    if (ui->keyReleased(GKey::RIGHT)){
        player2sim->SetAngularVelocity(0);
    }

    if (ui->keyDown(GKey::PAGEDOWN) && ui->keyDown(GKey::PAGEUP)){
        //SSHHHHHHH... Don't tell.
        ++livesB;
        if (livesB > 5){livesB = 5;}
    }
    if (ui->keyDown(GKey('g')) && ui->keyDown(GKey('h'))){
        //Short game ...
        if (livesA > 1){livesA = 1;}
        if (livesB > 1){livesB = 1;}
    }
}


void App::onGraphics2D(RenderDevice* rd, Array<shared_ptr<Surface2D> >& posed2D){
    m_font->draw2D(renderDevice, "Lives:", Point2(110,30), 50,
                   Color3(.92,.3,.07), Color3::black(), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
    m_font->draw2D(renderDevice, String(std::to_string(livesA).c_str()), Point2(90,100), 75,
                   Color3(.92,.3,.07), Color3::black(), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
    m_font->draw2D(renderDevice, "Lives:", Point2(1175,30), 50,
                   Color3(.5,1,.99), Color3::black(), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
    m_font->draw2D(renderDevice, String(std::to_string(livesB).c_str()), Point2(1160,100), 75,
                   Color3(.5,1,.99), Color3::black(), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);

    if (livesA == 0 && paused){
        Draw::rect2D(Rect2D(Vector2(1280,720)),renderDevice,Color3::white(), m_texture, Sampler::defaultClamp());
        
        m_font->draw2D(renderDevice, "Blue Wins!!!", renderDevice->viewport().center(), 125,
                       Color3::black(), Color4(Color3(.5,1,.99),.8), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
        m_font->draw2D(renderDevice, "Press n to play again.", Point2(renderDevice->viewport().center().x, 500), 50,
                       Color3::black(), Color4(Color3(.5,1,.99),.8), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
    }

    if (livesB == 0 && paused){
        Draw::rect2D(Rect2D(Vector2(1280,720)),renderDevice,Color3::white(), m_texture, Sampler::defaultClamp());
        m_font->draw2D(renderDevice, "Orange Wins!!!", renderDevice->viewport().center(), 125,
                       Color3::black(),Color4(Color3(.92,.3,.07),.8), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
        m_font->draw2D(renderDevice, "Press n to play again.", Point2(renderDevice->viewport().center().x, 500), 50,
                       Color3::black(),Color4(Color3(.92,.3,.07),.8), GFont::XALIGN_CENTER, GFont::YALIGN_CENTER);
    }

}


void App::onSimulation(RealTime rdt, SimTime sdt, SimTime idt) {
    int bulletTimeRadius = 1250;
    b2Body* player1sim = simBodies[0];
    b2Body* player2sim = simBodies[1];

    //gameover
    if (livesA == 0 && livesB == 0){
        drawMessage("You Tied??? That's Possible???");
        paused = true;
    }
    if (livesA == 0 && ! paused){
        printf("This keeps happening... like forever...\n");
        paused =true;

        //Do Pixel Shader
        Args args;
        b2Vec2 pos = player1sim->GetWorldVector(b2Vec2(1,10));
        b2Vec2 explodePos = b2Vec2(pos.x + player1sim->GetPosition().x, pos.y + player1sim->GetPosition().y);


        Texture::Specification e1;
        e1.filename = System::findDataFile("Background.jpg");
        e1.encoding.format = ImageFormat::RGB8();
        m_texture = Texture::create(e1);

        Texture::Specification e2;
        e2.filename = System::findDataFile("Original.jpg");
        e2.encoding.format = ImageFormat::RGB8();
        m_texture2 = Texture::create(e2);

        renderDevice->push2D(m_framebuffer);{
            printf("Player died at: ( %f, %f )\n", explodePos.x, explodePos.y);
            args.setUniform("exploX", float(640 + (explodePos.x / 90.0) * 640));
            args.setUniform("exploY", float(360 + (explodePos.y / 60.0) * -360));
            args.setUniform("original", m_texture2, Sampler::defaultClamp());
            args.setUniform("background", m_texture, Sampler::defaultClamp());
            args.setRect(m_texture->rect2DBounds());
            LAUNCH_SHADER("explode.pix", args);
        } renderDevice->pop2D();
        
        //Save result
            
        shared_ptr<Texture> answer = m_framebuffer->texture(0);
        shared_ptr<Image> answerImage = answer->toImage(ImageFormat::RGB8());
        answerImage->save("Background.jpg");
        //renderDevice->clear();

        G3D::ArticulatedModel::clearCache();
        G3D::Texture::clearCache();
        G3D::UniversalMaterial::clearCache();

        //CLEAR
        Texture::Specification e3;
        e3.filename = System::findDataFile("Background.jpg");
        e3.encoding.format = ImageFormat::RGB8();
        m_texture = Texture::create(e3);
        
        restartable = true;
    
    }
    if (livesB ==0 && !paused){
        paused =true;

        //Do Pixel Shader
        Args args;
        b2Vec2 pos = player2sim->GetWorldVector(b2Vec2(15,10));
        b2Vec2 explodePos = b2Vec2(pos.x + player2sim->GetPosition().x,pos.y +player2sim->GetPosition().y);

        Texture::Specification e1;
        e1.filename = System::findDataFile("Background.jpg");
        e1.encoding.format = ImageFormat::RGB8();
        m_texture = Texture::create(e1);

        Texture::Specification e2;
        e2.filename = System::findDataFile("Original.jpg");
        e2.encoding.format = ImageFormat::RGB8();
        m_texture2 = Texture::create(e2);

        renderDevice->push2D(m_framebuffer); {
            printf("Player died at: ( %f, %f )\n", explodePos.x, explodePos.y);
            args.setUniform("exploX", float(640 + (explodePos.x / 90.0) * 640));
            args.setUniform("exploY", float(360 + (explodePos.y / 60.0) * -360));
            args.setUniform("original", m_texture2, Sampler::defaultClamp());
            args.setUniform("background", m_texture, Sampler::defaultClamp());
            args.setRect(m_texture->rect2DBounds());
            LAUNCH_SHADER("explode.pix", args);
            //Save result
        } renderDevice->pop2D();


        shared_ptr<Texture> answer = m_framebuffer->texture(0);
        shared_ptr<Image> answerImage = answer->toImage(ImageFormat::RGB8());
             
        answerImage->save("Background.jpg");
        //   renderDevice->clear();
        
        G3D::ArticulatedModel::clearCache();
        G3D::Texture::clearCache();
        G3D::UniversalMaterial::clearCache();

        //CLEAR
        Texture::Specification e3;
        e3.filename = System::findDataFile("Background.jpg");
        e3.encoding.format = ImageFormat::RGB8();
        m_texture = Texture::create(e3);
        
    }
    if (!paused){
        sims = sims + 1.0;
        GApp::onSimulation(rdt, sdt, idt);
        float32 timeStep = (bulletTime) ? (1/30.0) : (1/10.0);
        int32 veloIter = 8;
        int32 posIter = 3;
        simWorld->Step(timeStep, veloIter, posIter);

        /*
        screenPrintf("");
        screenPrintf("SCORES:");
        screenPrintf("Orange lives: %d", livesA);
        screenPrintf("Blue lives: %d", livesB);
        */

        const shared_ptr<Entity>& player1 = scene()->entity("body1");
        const shared_ptr<Entity>& player2 = scene()->entity("body2");

        Array<shared_ptr<Entity> > entities;

        //Move camera according to bodies...
        b2Vec2 player1Center = player1sim->GetWorldVector(b2Vec2(8,14));
        b2Vec2 player2Center = player2sim->GetWorldVector(b2Vec2(8,14));
        //b2Vec2 player1Center = b2Vec2(0,0);
        //b2Vec2 player2Center = b2Vec2(0,0);

        float cameraX = ((player1Center.x + player1sim->GetPosition().x) +
                         (player2Center.x + player2sim->GetPosition().x)) / 5.0;
        float cameraY = ((player1Center.y + player1sim->GetPosition().y) +
                         (player2Center.y + player2sim->GetPosition().y)) / 3.0;
        float cameraZ = std::pow(((((player1Center.x + player1sim->GetPosition().x) -
                         (player2Center.x + player2sim->GetPosition().x)) *
                        ((player1Center.x + player1sim->GetPosition().x) -
                         (player2Center.x + player2sim->GetPosition().x)))/97200 +
                        (((player1Center.x + player1sim->GetPosition().x) -
                          (player2Center.x + player2sim->GetPosition().x)) *
                         ((player1Center.x + player1sim->GetPosition().x) -
                          (player2Center.x + player2sim->GetPosition().x)))/21600), 1/3) * 40 + 120;

        activeCamera()->setFrame(CFrame::fromXYZYPRDegrees(cameraX,cameraY,cameraZ,0,0,0));

        // leg
        entities.append(scene()->entity("leg1"));
        entities.append(scene()->entity("leg2"));

        // heart
        entities.append(scene()->entity("heart1"));
        entities.append(scene()->entity("heart2"));

        //tip
        entities.append(scene()->entity("tip1"));
        entities.append(scene()->entity("tip2"));

        //spike
        entities.append(scene()->entity("spike1"));
        entities.append(scene()->entity("spike2"));

        //mainJet
        entities.append(scene()->entity("mainJet1"));
        entities.append(scene()->entity("mainJet2"));

        //body
        entities.append(scene()->entity("body1"));
        entities.append(scene()->entity("body2"));

        //head
        entities.append(scene()->entity("head1"));
        entities.append(scene()->entity("head2"));


        if (notNull(player1)){
            b2Vec2 heart1sim;
            b2Vec2 heart2sim;
            b2Transform heartTrans1;
            b2Transform heartTrans2;

            b2Vec2 tip1sim;
            b2Vec2 tip2sim;
            b2Transform tipTrans1;
            b2Transform tipTrans2;

            b2PolygonShape* tipShape1;
            b2PolygonShape* tipShape2;
            b2PolygonShape* heartShape1;
            b2PolygonShape* heartShape2;


            float playerAngle1 = player1sim->GetAngle() / (pi()) * 180;
            float playerAngle2 = player2sim->GetAngle() / (pi()) * 180;

            //NEED TO MAKE SURE WE ARE GRABBING THE RIGHT STUFF
            b2Fixture* tip1Fix = player1sim->GetFixtureList();
            b2Fixture* tip2Fix = player2sim->GetFixtureList();

            b2Vec2 testXY1;
            b2Vec2 testXY2;

            int a = 0;
            for (b2Fixture* i = player1sim->GetFixtureList(); i; i= i->GetNext()){
                b2PolygonShape* shape = (b2PolygonShape*)i->GetShape();

                if (a < 7){
                    float x = player1sim->GetPosition().x; //+ shape->m_centroid.x;
                    float y = player1sim->GetPosition().y;// + shape->m_centroid.y;v
                    if (fixtureNames[a] == "heart"){
                        heartShape1 = shape;
                        testXY1 = b2Vec2(100,100);
                        heart1sim = b2Vec2(10,10);
                        heartTrans1 = b2Transform(heart1sim,b2Rot(player1sim->GetAngle()));
                    }
                    if (fixtureNames[a] == "tip"){
                        tipShape1 = shape;
                        tip1sim = b2Vec2(x, y);
                        tipTrans1 = b2Transform(tip1sim,b2Rot(player1sim->GetAngle()));
                    }

                    entities[2 * a]->setFrame( CFrame::fromXYZYPRDegrees(x,y,-20.0,0,0,playerAngle1));
                    a++;
                }
            }

            a = 0;
            for (b2Fixture* i = player2sim->GetFixtureList(); i; i= i->GetNext()){
                b2PolygonShape* shape = (b2PolygonShape*)i->GetShape();
                if (a < 7){
                    float x = player2sim->GetPosition().x;// + shape->m_centroid.x;
                    float y = player2sim->GetPosition().y;// + shape->m_centroid.y;

                    if (fixtureNames[a] == "heart"){
                        heartShape2 = shape;
                        testXY2 = b2Vec2(100,100);
                        heart2sim = b2Vec2(x,y);
                        heartTrans2 = b2Transform(heart2sim,b2Rot(player2sim->GetAngle()));
                    }
                    if (fixtureNames[a] == "tip"){
                        tipShape2 = shape;
                        tip2sim = b2Vec2(x,y);
                        tipTrans2 =  b2Transform(tip2sim,b2Rot(player2sim->GetAngle()));
                    }
                    entities[2 * a + 1]->setFrame( CFrame::fromXYZYPRDegrees(x,y,-20.0,0,0,playerAngle2));
                    a++;
                }
            }
            const shared_ptr<Entity> heartTest1 = scene()->entity("heart1");
            const shared_ptr<Entity> heartTest2 = scene()->entity("heart2");
            const shared_ptr<Entity> tipTest1 = scene()->entity("tip1");
            const shared_ptr<Entity> tipTest2 = scene()->entity("tip2");


            float hs1_x, hs1_y, z, yaw, pitch, roll;
            CFrame frame = heartTest1->frame();
            frame.getXYZYPRDegrees(hs1_x,hs1_y,z,yaw,pitch,roll);
            heart1sim = b2Vec2(hs1_x,hs1_y);
            heartTrans1 =  b2Transform(heart1sim,b2Rot(player1sim->GetAngle()));

            frame = heartTest2->frame();
            frame.getXYZYPRDegrees(hs1_x,hs1_y,z,yaw,pitch,roll);
            heart2sim = b2Vec2(hs1_x,hs1_y);
            heartTrans2 =  b2Transform(heart2sim,b2Rot(player2sim->GetAngle()));

            frame = tipTest1->frame();
            frame.getXYZYPRDegrees(hs1_x,hs1_y,z,yaw,pitch,roll);
            tip1sim = b2Vec2(hs1_x,hs1_y);
            tipTrans1 =  b2Transform(tip1sim,b2Rot(player1sim->GetAngle()));

            frame = tipTest2->frame();
            frame.getXYZYPRDegrees(hs1_x,hs1_y,z,yaw,pitch,roll);
            tip2sim = b2Vec2(hs1_x,hs1_y);
            tipTrans2 =  b2Transform(tip2sim,b2Rot(player2sim->GetAngle()));


            //If heart and tip of someone is close together, set BulletTime -> True, else set BulletTime -> False
            bulletTime = (
                          ((pow((tip1sim.x - heart2sim.x),2)
                            + pow((tip1sim.y - heart2sim.y),2)) < bulletTimeRadius) ||
                          ((pow((tip2sim.x - heart1sim.x),2)
                            + pow((tip2sim.y - heart1sim.y),2)) < bulletTimeRadius));


            //     printf("%d\n", pow((tip1sim.x - heart2sim.x),2) + pow ((tip1sim.y-heart2sim.y),2) < bulletTimeRadius);
            //printf("%d\n", pow((tip2sim.x - heart1sim.x),2) + pow ((tip2sim.y-heart1sim.y),2) < bulletTimeRadius);

            if (bulletTime){
                themeSong->setVolume(0.0);
                bulletTimeSong->setVolume(1.0);

            }else{
                themeSong->setVolume(1.0);
                bulletTimeSong->setVolume(0.0);
            }

            if (b2TestOverlap(tipShape1,0,heartShape2,0,tipTrans1,heartTrans2)){
                if (AhitB == 0){
                    livesB--;
                    AhitB = 20;
                    popSound->play(2.0);
                }
            }
            else{
                if (AhitB > 0){
                    AhitB--;
                }
            }


            if  (b2TestOverlap(tipShape2,0,heartShape1,0,tipTrans2,heartTrans1)){
                if (BhitA == 0){
                    BhitA = 20;
                    livesA--;
                    popSound->play(2.0);
                }
            }
            else{
                if (BhitA > 0){
                    BhitA--;
                }
           }
        }
    }
}
