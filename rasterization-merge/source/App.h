#ifndef App_h
#define App_h

#include <G3D/G3DAll.h>
#include "Box2D/box2d.h"
#include <string>

class DemoScene;

class App : public GApp {
protected:
    
    G3D::Random rand;
    
    shared_ptr<GFont> m_font = GFont::fromFile(System::findDataFile("adventure.fnt"));

    shared_ptr<Sound> song1;
    shared_ptr<Sound> song2;
    shared_ptr<Sound> popSound;

    shared_ptr<AudioChannel> themeSong;
    shared_ptr<AudioChannel> bulletTimeSong;

    shared_ptr<Texture> m_texture;
    shared_ptr<Texture> m_texture2;
    shared_ptr<Texture> winScreen;
    
    bool paused = false;
    bool bulletTime = false;
    bool restartable = false;

    int livesA = 5;
    int livesB = 5;
    int AhitB = 0;
    int BhitA = 0;

    float sims;
    shared_ptr<DemoScene>   m_scene;    
    
    b2World* simWorld;

    G3D::Array<b2Body*> simBodies;
    G3D::Array<std::string> fixtureNames;
    G3D::Array<shared_ptr<Entity> > entityList; 

    
     /** Called from onInit */
     void makeGUI();

public:
    App(const GApp::Settings& settings = GApp::Settings());
    virtual void onInit() override;
    virtual void onUserInput(UserInput* ui) override;

    void newGame();
    virtual void onGraphics2D(RenderDevice* rd, Array<shared_ptr<Surface2D> >& surface2D) override;
    virtual void onSimulation(RealTime rdt, SimTime sdt, SimTime idt) override;
};

#endif
