# Spiral Cubes
import math

f = open("render.Scene.Any")
code = f.readlines();
f.close()

cubes = ""
level = 0
x = 0

for next_level in range(180):
    cubes += "\t cube{l} = VisibleEntity{{\n".format(l=next_level)
    cubes += """\t\t model = "cubeModel";\n"""
    cubes += "\t\t track = transform(\n"
    if (next_level % 5) == 0 and next_level != 0:
        level += 1;
        if x == 0:
            x = -0.5
        else:
            x = 0
        cubes += "\t\t\t CFrame::fromXYZYPRDegrees({shift},{s},0,{r},0,0),\n".format(r=next_level*72,s=level,shift = x)
    else:
        cubes += "\t\t\t CFrame::fromXYZYPRDegrees({shift},{s},0,{r},0,0),\n".format(r=next_level*72,s=level,shift = x)
    cubes += "\t\t\t orbit(0,5.0));\n"
    cubes += "\t};\n"
        
i = 0 #indexing throughlines
for line in code:
    if line.strip() == "/* Make Spiral of Cubes */":
        code.insert(i+1,cubes)
        break
    i += 1

f = open("render.Scene.Any","w")
code = "".join(code)
f.write(code)
f.close()
