#include "RayTracer.h"

RayTracer::Settings::Settings() :
    width(160),
    height(90),
    multithreaded(true),
    useTree(false) {

#   ifdef G3D_DEBUG
    // If we’re debugging, we probably don’t want threads by default
    multithreaded = false;
# endif
}

RayTracer::Stats::Stats() :
    lights(0),
    triangles(0),
    pixels(0),
    buildTriTreeTimeMilliseconds(0),
    rayTraceTimeMilliseconds(0) {}

RayTracer::RayTracer() {
    m_rnd.resize(System::numCores());
    for (int i = 0; i < m_rnd.size(); ++i) {
        // Use a different seed for each and do not be threadsafe
        m_rnd[i] = shared_ptr<Random>(new Random(i, false));
    }
}


shared_ptr<RayTracer> RayTracer::create() {
    return shared_ptr<RayTracer>(new RayTracer());
}

shared_ptr<Image> RayTracer::render
(const Settings&                     settings,
 const Array< shared_ptr<Surface> >& surfaceArray,
 const shared_ptr<LightingEnvironment>& lighting,
 const shared_ptr<Camera>&           camera,
 Stats&                              stats) {
   
    RealTime start;
    debugAssert(notNull(lighting) && notNull(camera));
    
    // store member pointers to the arguments
    // so that they can propagate inside the callbacks
    m_settings = settings;
    m_camera = camera;
    m_lighting = lighting;
    
    // Build the TriTree
    start = System::time();
    m_triTree.setContents(surfaceArray);
    stats.buildTriTreeTimeMilliseconds =
        float((System::time() - start) / units::milliseconds());
    // Allocate the image
    m_image = Image::create(settings.width, settings.height,
                            ImageFormat::RGB32F());
    // Render the image
    start = System::time();
    const int numThreads =
        settings.multithreaded ? GThread::NUM_CORES : 1;
    traceAllPixels(numThreads);
    stats.rayTraceTimeMilliseconds =
        float((System::time() - start) / units::milliseconds());
    
    // pixels 
    stats.pixels = settings.width * settings.height;
    
    //lights
    stats.lights = lighting->lightArray.size();

    // triangles
    stats.triangles = m_triTree.size(); 
    
    shared_ptr<Image> temp(m_image);
    
    // Reset pointers to NULL to allow garbage collection
    m_lighting.reset();
    m_camera.reset();
    
    return temp;
}


void RayTracer::traceOnePixel(int x, int y, int threadID){
    Vector2 temp(m_settings.width,m_settings.height);
    Ray r = m_camera->worldRay(x, y, Rect2D(temp));
    shared_ptr<Surfel> surfel = castRay(r);
    if (notNull(surfel)){
        // debugAssertM(notNull(surfel), "Surfel not found!");
        m_image -> set(x,y, L_scatteredDirect (surfel, -r.direction(),*(m_rnd[threadID])));     
    } else{
        m_image -> set(x, y, Radiance3(0,0,1));
    } 
    
}

 
// L_scatter direct
Radiance3 RayTracer::L_scatteredDirect(const shared_ptr<Surfel>& surfel, const Vector3& wo, Random& rnd) const {
    
   
    const shared_ptr<UniversalSurfel>& u = dynamic_pointer_cast<UniversalSurfel>(surfel);
    debugAssertM(notNull(u), "Encountered a Surfel that was not a Universal Surfel");
   
    // Begin with initial radiance which has the unit W/(m^2. sr) 
    Radiance3 L  = u -> emittedRadiance(wo);
    
    // Add direct illumination from each light
    for (int i = 0; i < m_lighting-> lightArray.size(); ++i){
        
        const shared_ptr<Light> light (m_lighting -> lightArray[i]);
        
        // position of the light
        Vector3 y = (light->position()).xyz();
        
        //position of the suface 
        Vector3 x = u-> position;
        
        // temp variable to store the vector from y-x
        Vector3 temp_wi = y-x;
        
        // direction of incident light 
        Vector3 wi = temp_wi.direction();
        
        // Bi with unit W/(sr^2)
        Biradiance3 Bi = (light->bulbPower())/ (4 * pi() * square(temp_wi.magnitude()));
        
        if (wi.dot(u->shadingNormal)>0 && wo.dot(u->shadingNormal)>0) {
            L += Bi * abs(wi.dot(u->shadingNormal)) * ((u->lambertianReflectivity)/pi());
        }
    } 
    
    return L;
}

void RayTracer::traceAllPixels(int numThreads) {
    GThread::runConcurrently2D(Point2int32(0,0), Point2int32(m_settings.width,m_settings.height),this,&RayTracer::traceOnePixel,numThreads);
}

shared_ptr<Surfel> RayTracer::castRay
(const Ray& ray,
 float      maxDistance,
 bool       anyHit) const {
    // Distance from P to X
    float distance(maxDistance);
    shared_ptr<Surfel> surfel;
    if (m_settings.useTree) {
        // Treat the triTree as a tree
        surfel = m_triTree.intersectRay(ray, distance, anyHit);
    } else {
        // Treat the triTree as an array
        Tri::Intersector intersector;
        for (int t = 0; t < m_triTree.size(); ++t) {
            const Tri& tri = m_triTree[t];
            intersector(ray, m_triTree.cpuVertexArray(), tri,
                        anyHit, distance);
        }
        surfel = intersector.surfel();
    }
    return surfel;
}
