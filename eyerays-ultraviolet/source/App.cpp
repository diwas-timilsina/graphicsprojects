/** \file App.cpp */
#include "App.h"

// Tells C++ to invoke command-line main() function even on OS X and Win32.
G3D_START_AT_MAIN();

int main(int argc, const char* argv[]) {
    {
        G3DSpecification g3dSpec;
        g3dSpec.audio = false;
        initGLG3D(g3dSpec);
    }
           
    GApp::Settings settings(argc, argv);

    // Change the window and other startup parameters by modifying the
    // settings class.  For example:
    settings.window.caption			= argv[0];
    //settings.window.debugContext = true;
    // Some popular resolutions:
    // settings.window.width        = 640;  settings.window.height       = 400;
    // settings.window.width		= 1024; settings.window.height       = 768;
    settings.window.width         = 1280; settings.window.height       = 720;
    //settings.window.width        = 1920; settings.window.height       = 1080;
    // settings.window.width		= OSWindow::primaryDisplayWindowSize().x; settings.window.height = OSWindow::primaryDisplayWindowSize().y;

    // Set to true for a significant performance boost if your app can't render at 60fps,
    // or if you *want* to render faster than the display.
    settings.window.asynchronous	    = true;
    settings.depthGuardBandThickness    = Vector2int16(64, 64);
    settings.colorGuardBandThickness    = Vector2int16(16, 16);
    settings.dataDir			        = FileSystem::currentDirectory();
    settings.screenshotDirectory	    = "../journal/";

    return App(settings).run();
}

void App::onResolutionChange() {
    TextInput ti(TextInput::FROM_STRING,
                 m_resolutionList->selectedValue().text());
    m_rayTracerSettings.width = ti.readNumber();
    ti.readSymbol("x");
    m_rayTracerSettings.height = ti.readNumber();
}

App::App(const GApp::Settings& settings) : GApp(settings) {
   
}


// Called before the application loop begins.  Load data here and
// not in the constructor so that common exceptions will be
// automatically caught.
void App::onInit() {
   
    GApp::onInit();
        
    setFrameDuration(1.0f / 60.0f);

    // Call setScene(shared_ptr<Scene>()) or setScene(MyScene::create()) to replace
    // the default scene here.
    
    showRenderingStats      = false;

    makeGUI();
    m_lastLightingChangeTime = 0.0;
    // For higher-quality screenshots:
    // developerWindow->videoRecordDialog->setScreenShotFormat("PNG");
    // developerWindow->videoRecordDialog->setCaptureGui(false);
    developerWindow->cameraControlWindow->moveTo(Point2(developerWindow->cameraControlWindow->rect().x0(), 0));
    loadScene(
		"G3D Cornell Box" // Load something simple
         //    developerWindow->sceneEditorWindow->selectedSceneName()  // Load the first scene encountered 
              );
    
    m_videoFileName = "test.mp4";

    
}

void App::makeGUI() {
    // Initialize the developer HUD (using the existing scene)
    createDeveloperHUD();
    debugWindow->setVisible(true);
    developerWindow->videoRecordDialog->setEnabled(true);
    
    //    GuiPane* infoPane = debugPane->addPane("Info", GuiTheme::ORNATE_PANE_STYLE);
    
    /*    
    // Example of how to add debugging controls
    infoPane->addLabel("You can add GUI controls");
    infoPane->addLabel("in App::onInit().");
    infoPane->addButton("Exit", this, &App::endProgram);
    infoPane->pack(); 
    */
    // More examples of debugging GUI controls:
    // debugPane->addCheckBox("Use explicit checking", &explicitCheck);
    // debugPane->addTextBox("Name", &myName);
    
    shared_ptr<GuiWindow> rendWindow = GuiWindow::create("Render Parameters",
                                                         shared_ptr< GuiTheme > (),
                                                         Rect2D::xywh(100,100,100,50),
                                                         GuiTheme::NORMAL_WINDOW_STYLE,
                                                         GuiWindow::HIDE_ON_CLOSE); 
    
    GuiPane* rtPane = rendWindow->pane();
    
    m_resolutionList = rtPane -> addDropDownList ("Resolution", Array<GuiText>("16 x 9", "160 x 90",
                                                                               "320 x 180", "640 x 360", "854 x 480",  "1280 x 720"),
                             NULL, GuiControl::Callback(this, &App::onResolutionChange));
    rtPane->addCheckBox("Multithreaded", &m_rayTracerSettings.multithreaded);
    
    rtPane->addCheckBox("Use TriTree", &m_rayTracerSettings.useTree);
    
    rtPane->addNumberBox("Lighting",&m_rayTracerStats.lights);
    rtPane->addNumberBox("Traingles",&m_rayTracerStats.triangles);
    rtPane->addNumberBox("Pixels",&m_rayTracerStats.pixels);
    rtPane->addNumberBox("TriTree",&m_rayTracerStats.buildTriTreeTimeMilliseconds);
    rtPane->addNumberBox("Ray Trace",&m_rayTracerStats.rayTraceTimeMilliseconds);
    
    rtPane->addButton("Render Image", this, &App::onRenderButton);
    
    rtPane->addNumberBox("Frame Count",&m_endRecordFrame);
    rtPane->addTextBox("Filename",&m_videoFileName);
    rtPane->addButton("Record Video", this, &App::onRecordButton);
    addWidget(rendWindow);

    //button = rtPane->addButton("Render");
    debugWindow->pack();
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


void App::onRecordButton(){
    m_recording = true;
    m_frameCount = 0;
    
    //create the video output object
    m_video = VideoOutput::create(m_videoFileName, G3D::VideoOutput::Settings(G3D::VideoOutput::CODEC_ID_MPEG4,
                                                                              m_rayTracerSettings.width,m_rayTracerSettings.height,30.0f,0)); 
    
}


void App::onGraphics3D(RenderDevice* rd, Array<shared_ptr<Surface> >& allSurfaces) {
    // This implementation is equivalent to the default GApp's. It is repeated here to make it
    // easy to modify rendering. If you don't require custom rendering, just delete this
    // method from your application and rely on the base class.
    
    
    /** for video recording*/ 
    if (m_recording){
        if (m_frameCount<= m_endRecordFrame){
            
            shared_ptr<RayTracer> trace (RayTracer::create());
            shared_ptr<LightingEnvironment> lighting (new LightingEnvironment(scene()->lightingEnvironment()));  
            shared_ptr<Image> image (trace -> render(m_rayTracerSettings, GApp::m_posed3D, lighting, GApp::m_activeCamera, m_rayTracerStats));
            m_video->append(G3D::Texture::fromImage("frameImage",image));
            ++m_frameCount;
        }else{
            m_recording = false;
            m_video->commit();
        } 
    }

    
    // screenPrintf("Print to the screen from anywhere in a G3D program with this command.");
    if (! scene()) {
        return;
    }
    m_gbuffer->setSpecification(m_gbufferSpecification);
    m_gbuffer->resize(m_framebuffer->width(), m_framebuffer->height());
    
    // Share the depth buffer with the forward-rendering pipeline
    m_framebuffer->set(Framebuffer::DEPTH, m_gbuffer->texture(GBuffer::Field::DEPTH_AND_STENCIL));
    m_depthPeelFramebuffer->resize(m_framebuffer->width(), m_framebuffer->height());

    Surface::AlphaMode coverageMode = Surface::ALPHA_BLEND;

    // Bind the main framebuffer
    rd->pushState(m_framebuffer); {
        rd->clear();
        rd->setProjectionAndCameraMatrix(activeCamera()->projection(), activeCamera()->frame());

        m_gbuffer->prepare(rd, activeCamera(), 0, -(float)previousSimTimeStep(), m_settings.depthGuardBandThickness, m_settings.colorGuardBandThickness);
        
        // Cull and sort
        Array<shared_ptr<Surface> > sortedVisibleSurfaces;
        Surface::cull(activeCamera()->frame(), activeCamera()->projection(), rd->viewport(), allSurfaces, sortedVisibleSurfaces);
        Surface::sortBackToFront(sortedVisibleSurfaces, activeCamera()->frame().lookVector());
        
        const bool renderTransmissiveSurfaces = false;

        // Intentionally copy the lighting environment for mutation
        LightingEnvironment environment = scene()->lightingEnvironment();
        environment.ambientOcclusion = m_ambientOcclusion;
       
        // Render z-prepass and G-buffer.
        Surface::renderIntoGBuffer(rd, sortedVisibleSurfaces, m_gbuffer, activeCamera()->previousFrame(), activeCamera()->expressivePreviousFrame(), renderTransmissiveSurfaces, coverageMode);

        // This could be the OR of several flags; the starter begins with only one motivating algorithm for depth peel
        const bool needDepthPeel = environment.ambientOcclusionSettings.useDepthPeelBuffer;
        if (needDepthPeel) {
            rd->pushState(m_depthPeelFramebuffer); {
                rd->clear();
                rd->setProjectionAndCameraMatrix(activeCamera()->projection(), activeCamera()->frame());
                Surface::renderDepthOnly(rd, sortedVisibleSurfaces, CullFace::BACK, renderTransmissiveSurfaces, coverageMode, m_framebuffer->texture(Framebuffer::DEPTH), environment.ambientOcclusionSettings.depthPeelSeparationHint);
            } rd->popState();
        }

        if (! m_settings.colorGuardBandThickness.isZero()) {
            rd->setGuardBandClip2D(m_settings.colorGuardBandThickness);
        }        

        // Compute AO
        m_ambientOcclusion->update(rd, environment.ambientOcclusionSettings, activeCamera(), m_framebuffer->texture(Framebuffer::DEPTH), m_depthPeelFramebuffer->texture(Framebuffer::DEPTH), m_gbuffer->texture(GBuffer::Field::CS_NORMAL), m_gbuffer->texture(GBuffer::Field::SS_POSITION_CHANGE), m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);

        const RealTime lightingChangeTime = max(scene()->lastEditingTime(), max(scene()->lastLightChangeTime(), scene()->lastVisibleChangeTime()));
        bool updateShadowMaps = false;
        if (lightingChangeTime > m_lastLightingChangeTime) {
            m_lastLightingChangeTime = lightingChangeTime;
            updateShadowMaps = true;
        }
        // No need to write depth, since it was covered by the gbuffer pass
        rd->setDepthWrite(false);
        // Compute shadow maps and forward-render visible surfaces
        Surface::render(rd, activeCamera()->frame(), activeCamera()->projection(), sortedVisibleSurfaces, allSurfaces, environment, coverageMode, updateShadowMaps, m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness, sceneVisualizationSettings());      
                
        // Call to make the App show the output of debugDraw(...)
        drawDebugShapes();
        const shared_ptr<Entity>& selectedEntity = (notNull(developerWindow) && notNull(developerWindow->sceneEditorWindow)) ? developerWindow->sceneEditorWindow->selectedEntity() : shared_ptr<Entity>();
        scene()->visualize(rd, selectedEntity, sceneVisualizationSettings());

        // Post-process special effects
        m_depthOfField->apply(rd, m_framebuffer->texture(0), m_framebuffer->texture(Framebuffer::DEPTH), activeCamera(), m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);
        
        m_motionBlur->apply(rd, m_framebuffer->texture(0), m_gbuffer->texture(GBuffer::Field::SS_EXPRESSIVE_MOTION), 
                            m_framebuffer->texture(Framebuffer::DEPTH), activeCamera(), 
                            m_settings.depthGuardBandThickness - m_settings.colorGuardBandThickness);

    } rd->popState();

    // We're about to render to the actual back buffer, so swap the buffers now.
    // This call also allows the screenshot and video recording to capture the
    // previous frame just before it is displayed.
    swapBuffers();

	// Clear the entire screen (needed even though we'll render over it, since
    // AFR uses clear() to detect that the buffer is not re-used.)
    rd->clear();

    // Perform gamma correction, bloom, and SSAA, and write to the native window frame buffer
    m_film->exposeAndRender(rd, activeCamera()->filmSettings(), m_framebuffer->texture(0));
}


void App::onAI() {
    GApp::onAI();
    // Add non-simulation game logic and AI code here
}


void App::onNetwork() {
    GApp::onNetwork();
    // Poll net messages here
}


void App::onSimulation(RealTime rdt, SimTime sdt, SimTime idt) {
    GApp::onSimulation(rdt, sdt, idt);

    // Example GUI dynamic layout code.  Resize the debugWindow to fill
    // the screen horizontally.
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


bool App::onEvent(const GEvent& event) {
    // Handle super-class events
    if (GApp::onEvent(event)) { return true; }

    // If you need to track individual UI events, manage them here.
    // Return true if you want to prevent other parts of the system
    // from observing this specific event.

    // if ((event.type == GEventType::KEY_DOWN) && (event.key.keysym.sym == GKey::TAB)) { ... return true; }

    return false;
}


void App::onUserInput(UserInput* ui) {
    GApp::onUserInput(ui);
    (void)ui;
    // Add key handling here based on the keys currently held or
    // ones that changed in the last frame.
}


void App::onPose(Array<shared_ptr<Surface> >& surface, Array<shared_ptr<Surface2D> >& surface2D) {
    GApp::onPose(surface, surface2D);

    // Append any models to the arrays that you want to later be rendered by onGraphics()
}


void App::onGraphics2D(RenderDevice* rd, Array<shared_ptr<Surface2D> >& posed2D) {
    // Render 2D objects like Widgets.  These do not receive tone mapping or gamma correction.
    Surface2D::sortAndRender(rd, posed2D);
}


void App::onCleanup() {
    // Called after the application loop ends.  Place a majority of cleanup code
    // here instead of in the constructor so that exceptions can be caught.
}


void App::endProgram() {
    m_endProgram = true;
}

// render button pressed
void App::onRenderButton(){
    
    shared_ptr<RayTracer> trace (RayTracer::create());
    shared_ptr<LightingEnvironment> lighting (new LightingEnvironment(scene()->lightingEnvironment()));  
    shared_ptr<Image> image (trace -> render(m_rayTracerSettings, GApp::m_posed3D, lighting, GApp::m_activeCamera, m_rayTracerStats));
    GApp::show(image,"Rendered Image");
    
    image->convert(ImageFormat::RGB8());
    image->save("generated.png");
}

