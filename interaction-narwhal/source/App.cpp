#include "App.h"
#include "Box2D/Box2d.h"
#include "b23d.h"
#include <cstdio>


G3D_START_AT_MAIN();

int main(int argc, const char* argv[]) {

    G3D::G3DSpecification spec;
    spec.audio = true;
    initGLG3D(spec);

    initGLG3D();

    GApp::Settings settings(argc, argv);
    
    settings.window.caption     = "Narwhals";
    settings.window.width       = 1280; 
    settings.window.height      = 720;
    
    return App(settings).run();
}


App::App(const GApp::Settings& settings) :GApp(settings) {
    sims = 0.0; 
}


void App::onInit() {
    paused = false;
    bulletTime = false;
    livesA = 5;
    livesB = 5;
    AhitB = 0;
    BhitA = 0;
    
    GApp::onInit();
    showRenderingStats    = false;
    setFrameDuration(1.0f / 60);
    makeGUI();

    loadScene(System::findDataFile("empty.Scene.Any"));
    

    b2Vec2 gravity(0,-8.5);
    // bool doSleep = true;

    simWorld = new b2World(gravity);

    b2BodyDef myBody;

    myBody.type = b2_dynamicBody;  
    
    myBody.position.Set(-5,0);
    myBody.angle = 0;
    b2Body* player1 = simWorld->CreateBody(&myBody);
    
    myBody.position.Set(5,0);
    b2Body* player2 = simWorld->CreateBody(&myBody);

    b2PolygonShape boxShape;
    //boxShape.SetAsBox(1,1);
    
    //Player Code
    b23d triangle1 = b23d("tri1");
    triangle1.addVertex(Point2(.5,0));
    triangle1.addVertex(Point2(0,3));
    triangle1.addVertex(Point2(-.5,0));
    boxShape = triangle1.makeB2();
    //    triangle.writeOBJ();

    b2FixtureDef boxFixtureDef;
    boxFixtureDef.shape = &boxShape;
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 0.95;
    player1->CreateFixture(&boxFixtureDef);
 
    b23d triangle2 = b23d("tri2");
    triangle2.addVertex(Point2(.5,0));
    triangle2.addVertex(Point2(0,3));
    triangle2.addVertex(Point2(-.5,0));
    boxShape = triangle2.makeB2();
    player2->CreateFixture(&boxFixtureDef);
   
    b23d body = b23d("body");
    body.addVertex(Point2(-1,-1));
    body.addVertex(Point2(1,-1));
    body.addVertex(Point2(1,0));
    body.addVertex(Point2(-1,0));
    boxShape = body.makeB2();
    boxFixtureDef.shape = &boxShape;

    player1->CreateFixture(&boxFixtureDef);
    player2->CreateFixture(&boxFixtureDef);

    boxFixtureDef.isSensor = true; 

    b23d tip = b23d("triPoint");
    tip.addVertex(Point2(.1,2.5));
    tip.addVertex(Point2(0,3));
    tip.addVertex(Point2(-.1,2.5));
    boxFixtureDef.density = 0.94;
    boxShape = tip.makeB2();

    player1->CreateFixture(&boxFixtureDef);
    player2->CreateFixture(&boxFixtureDef);
    
    boxFixtureDef.density = 0.95;

    //Heart Code
    boxFixtureDef.density = .1;

    b23d heart = b23d("heart");
    heart.addVertex(Point2(-.5,-.5));
    heart.addVertex(Point2(.5,-.5));
    heart.addVertex(Point2(.5,0));
    heart.addVertex(Point2(-.5,0));
    boxShape = heart.makeB2();
    heart.writeOBJ();
    boxFixtureDef.shape = &boxShape;
    
    
    myBody.position.Set(-10, 0);
    b2Body* heart1 = simWorld->CreateBody(&myBody);
    heart1->CreateFixture(&boxFixtureDef);
    

    myBody.position.Set(10, 0);
    b2Body* heart2 = simWorld->CreateBody(&myBody);
    heart2->CreateFixture(&boxFixtureDef);
   
    boxFixtureDef.isSensor = false; 

    boxFixtureDef.density = 0.0000001;    
    myBody.type = b2_staticBody;
    myBody.angle = 0;
    myBody.position.Set(0, -17.5);
        
    b23d outerWall = b23d("wall");
    outerWall.addVertex(Point2(-75,-2));
    outerWall.addVertex(Point2(75,-2));
    outerWall.addVertex(Point2(75,2));
    outerWall.addVertex(Point2(-75,2));
    boxShape = outerWall.makeB2();
    outerWall.writeOBJ();
    
    boxFixtureDef.shape = &boxShape;

    b2Body* botWall = simWorld->CreateBody(&myBody);
    botWall->CreateFixture(&boxFixtureDef);

    myBody.position.Set(0,17.5);

    b2Body* topWall = simWorld->CreateBody(&myBody);
    topWall->CreateFixture(&boxFixtureDef);


    b23d outerWall1 = b23d("wall1");
    outerWall1.addVertex(Point2(-2,-75));
    outerWall1.addVertex(Point2(2,-75));
    outerWall1.addVertex(Point2(2,75));
    outerWall1.addVertex(Point2(-2,75));
    boxShape = outerWall1.makeB2();
    outerWall1.writeOBJ();

    myBody.position.Set(-20,0);

    b2Body* sideWall1 = simWorld->CreateBody(&myBody);
    sideWall1->CreateFixture(&boxFixtureDef);

    myBody.position.Set(20,0);

    b2Body* sideWall2 = simWorld->CreateBody(&myBody);
    sideWall2->CreateFixture(&boxFixtureDef);

    player1->SetTransform(player1->GetPosition(),-45);
    //    heart1->SetTransform(player1->GetPosition(),-45);
    player2->SetTransform(player2->GetPosition(),45);
    // heart2->SetTransform(player2->GetPosition(),45);

    
    b2WeldJointDef jointDef1;
    jointDef1.bodyB = player1;
    jointDef1.bodyA = heart1;
    jointDef1.localAnchorB.Set(-1,-.5);
    jointDef1.localAnchorA.Set(0,0);
    jointDef1.collideConnected = false;
    jointDef1.referenceAngle = (pi() / 2.0);
    simWorld->CreateJoint( &jointDef1 );
    
    b2WeldJointDef jointDef2;
    jointDef2.bodyB = player2;
    jointDef2.bodyA = heart2;
    jointDef2.localAnchorB.Set(1.5,-.5);
    jointDef2.localAnchorA.Set(0,0);
    jointDef2.referenceAngle = (pi() / 2.0);
    jointDef2.collideConnected = false;
    simWorld->CreateJoint( &jointDef2 );
    
    simBodies.append(player1);
    simBodies.append(player2);
    simBodies.append(heart1);
    simBodies.append(heart2);
    
    G3D::ArticulatedModel::clearCache();
    loadScene(System::findDataFile("empty.Scene.Any"));
    song1 = G3D::Sound::create(System::findDataFile("mainSound.mp3"),true);
    song2 = G3D::Sound::create(System::findDataFile("slowMotion.mp3"),true);
    popSound = G3D::Sound::create(System::findDataFile("pop.mp3"),false);
    themeSong = song1->play();
    bulletTimeSong = song2->play(0.0);
}

 
void App::makeGUI() {
    // Initialize the developer HUD
    createDeveloperHUD();

    debugWindow->setVisible(false);
    developerWindow->videoRecordDialog->setEnabled(true);
    developerWindow->sceneEditorWindow->setVisible(false);
    developerWindow->cameraControlWindow->setVisible(false);

    debugWindow->pack();
    debugWindow->setRect(Rect2D::xywh(0, 0, (float)window()->width(), debugWindow->rect().height()));
}


void App::onUserInput(UserInput* ui) {
    float speed = 10.5;
    float turnSpeed = 2.5;
    float turnDecay = .9;

    GApp::onUserInput(ui);
    
    b2Body* box1sim = simBodies[0];
    b2Body* box2sim = simBodies[1];

    if (ui->keyDown(GKey('w'))){
        b2Vec2 vel = box1sim->GetLinearVelocity();
        float aVel = box1sim->GetAngularVelocity();
        float bodyAngle = box1sim->GetAngle();
        vel.x = -1.0 * speed * sin(bodyAngle);
        vel.y = speed * cos(bodyAngle);
        box1sim->SetLinearVelocity(vel);
        box1sim->SetAngularVelocity(turnDecay * aVel);
    }
    if (ui->keyDown(GKey('a'))){
        box1sim->SetAngularVelocity(turnSpeed);
    }
    if (ui->keyDown(GKey('d'))){
        box1sim->SetAngularVelocity(-turnSpeed);
    }
    if (ui->keyReleased(GKey('a'))){
        box1sim->SetAngularVelocity(0);
    }
    if (ui->keyReleased(GKey('d'))){
        box1sim->SetAngularVelocity(0);
    }
    if (ui->keyDown(GKey::UP)){
        b2Vec2 vel = box2sim->GetLinearVelocity();
        float aVel = box2sim->GetAngularVelocity();
        float bodyAngle = box2sim->GetAngle();
        vel.x = -1.0 * speed * sin(bodyAngle);
        vel.y = speed * cos(bodyAngle);
        box2sim->SetLinearVelocity(vel);
        box2sim->SetAngularVelocity(turnDecay * aVel);
    }
    if (ui->keyDown(GKey::LEFT)){
        box2sim->SetAngularVelocity(turnSpeed);
    }
    if (ui->keyDown(GKey::RIGHT)){
        box2sim->SetAngularVelocity(-turnSpeed);
    }
    if (ui->keyReleased(GKey::LEFT)){
        box2sim->SetAngularVelocity(0);
    }
    if (ui->keyReleased(GKey::RIGHT)){
        box2sim->SetAngularVelocity(0);
    }

    if (ui->keyDown(GKey::PAGEDOWN) && ui->keyDown(GKey::PAGEUP)){
        //SSHHHHHHH... Don't tell. 
        ++livesB;
        if (livesB > 5){livesB = 5;}
    }
}


void App::onSimulation(RealTime rdt, SimTime sdt, SimTime idt) {
    int bulletTimeRadius = 30;
   
    //gameover 
    if (livesA == 0 && livesB == 0){
        drawMessage("You Tied??? That's Possible???");
        paused = true;
    }
    if (livesA == 0){
        drawMessage("Blue wins!!");
        paused = true;
    }
    if (livesB ==0){
        drawMessage("Orange wins!!");
        paused = true;
    }
    if (!paused){
        sims = sims + 1.0;
        GApp::onSimulation(rdt, sdt, idt);
        float32 timeStep = (bulletTime) ? (1/50.0) : (1/20.0);
        int32 veloIter = 8;
        int32 posIter = 3;
        simWorld->Step(timeStep, veloIter, posIter);
        
        screenPrintf("");
        screenPrintf("SCORES:");
        screenPrintf("Orange lives: %d", livesA);
        screenPrintf("Blue lives: %d", livesB);
    
        // -------------------------------
        b2Body* box1sim = simBodies[0];
        b2Body* box2sim = simBodies[1];
        b2Body* heart1sim = simBodies[2];
        b2Body* heart2sim = simBodies[3];
        
        const shared_ptr<Entity>& box1 = scene()->entity("player1");
        const shared_ptr<Entity>& box2 = scene()->entity("player2");
        const shared_ptr<Entity>& heart1 = scene()->entity("player1Heart");
        const shared_ptr<Entity>& heart2 = scene()->entity("player2Heart");
        
        if (notNull(box1)){
            
            float hornAngle1 = box1sim->GetAngle() / (pi()) * 180;
            float hornAngle2 = box2sim->GetAngle() / (pi()) * 180;
            float heartAngle1 = heart1sim->GetAngle() / (pi()) * 180;
            float heartAngle2 = heart2sim->GetAngle() / (pi()) * 180;
            
            b2Fixture* tip1 = box1sim->GetFixtureList();
            b2Fixture* tip2 = box2sim->GetFixtureList();
            
            b2Fixture* hFix1 = heart1sim->GetFixtureList();
            b2Fixture* hFix2 = heart2sim->GetFixtureList();

            //If heart and tip of someone is close together, set BulletTime -> True, else set BulletTime -> False
            bulletTime = (((pow((box1sim->GetPosition().x - heart2sim->GetPosition().x),2) 
                            + pow((box1sim->GetPosition().y - heart2sim->GetPosition().y),2)) < bulletTimeRadius) || 
                          ((pow((box2sim->GetPosition().x - heart1sim->GetPosition().x),2) 
                            + pow((box2sim->GetPosition().y - heart1sim->GetPosition().y),2)) < bulletTimeRadius));
            
            if (bulletTime){
                themeSong->setVolume(0.0);
                bulletTimeSong->setVolume(1.0);
            }else{
                themeSong->setVolume(1.0);
                bulletTimeSong->setVolume(0.0);
            }

            if (b2TestOverlap(tip1->GetShape(),0,hFix2->GetShape(),0,box1sim->GetTransform(),heart2sim->GetTransform())){
                if (AhitB == 0){
                    livesB--;
                    AhitB = 10;
                    popSound->play(2.0);
                }
            }
            else{
                if (AhitB > 0){
                    AhitB--;
                }
            }
            

            if  (b2TestOverlap(tip2->GetShape(),0,hFix1->GetShape(),0,box2sim->GetTransform(),heart1sim->GetTransform())){
                if (BhitA == 0){
                    BhitA = 10;
                    livesA--;
                    popSound->play(2.0);
                }
            }
            else{
                if (BhitA > 0){
                    BhitA--;
                }
            }
            
            box1->setFrame( CFrame::fromXYZYPRDegrees(box1sim->GetPosition().x,box1sim->GetPosition().y,-20.0,0,0,hornAngle1));
            box2->setFrame( CFrame::fromXYZYPRDegrees(box2sim->GetPosition().x,box2sim->GetPosition().y,-20.0,0,0,hornAngle2));
            heart1->setFrame( CFrame::fromXYZYPRDegrees(heart1sim->GetPosition().x,heart1sim->GetPosition().y,-20.0,0,0,heartAngle1));
            heart2->setFrame( CFrame::fromXYZYPRDegrees(heart2sim->GetPosition().x,heart2sim->GetPosition().y,-20.0,0,0,heartAngle2));
        }
    }
    // -------------------------------   
}

