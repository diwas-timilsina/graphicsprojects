#ifndef b23d_h
#define b23d_h

#include "b23d.h"
#include "App.h"
#include "Box2D/Box2D.h"
#include <string>
#include <G3D/G3DAll.h>

/// 
class b23d
{
public:

    Array<Point2> vertices;
    Array<Point2> addVertex(Point2 point); 
    b2PolygonShape makeB2();
    void writeOBJ();
    b23d(std::string name);
   


private:

};

#endif
