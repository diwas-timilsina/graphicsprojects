#ifndef App_h
#define App_h

#include <G3D/G3DAll.h>
#include "Box2D/box2d.h"

class DemoScene;

class App : public GApp {
protected:
    shared_ptr<Sound> song1;
    shared_ptr<Sound> song2;
    shared_ptr<Sound> popSound;

    shared_ptr<AudioChannel> themeSong;
    shared_ptr<AudioChannel> bulletTimeSong;

    bool paused = false;
    bool bulletTime = false;
    
    int livesA = 5;
    int livesB = 5;
    
    int AhitB = 0;
    int BhitA = 0;

    float sims;
    shared_ptr<DemoScene>   m_scene;    
    
    b2World* simWorld;

    G3D::Array<b2Body*> simBodies;
    
    /** Called from onInit */
    void makeGUI();

public:
    App(const GApp::Settings& settings = GApp::Settings());
    virtual void onInit() override;
    virtual void onUserInput(UserInput* ui) override;


    virtual void onSimulation(RealTime rdt, SimTime sdt, SimTime idt) override;
};

#endif
