from cityIO import *
from Tkinter import *
import tkMessageBox

def main():

    cityLoader = cityIO()
    cityLoader.read('scene/testScene.cty')
    buildings = cityLoader.getBuildingList()
    root = Tk()
    root.title("City Editor")
    root.geometry('800x600+1000+100')
    listFrame = LabelFrame(root, text = "List of Buildings")
    buildingList = Listbox(listFrame, width = 30, height = 40)
    i = len(buildings)
    for building in buildings:
        --i
        buildingList.insert(i, building.get('name'))
    buildingList.pack(side = LEFT)
    listFrame.pack(side = LEFT)

    infoFrame = LabelFrame(root, text= "Building Information")

    textBox1 = Entry(infoFrame, bd=5)
    textBox1.pack()
    infoFrame.pack(side = BOTTOM)

    root.mainloop()

if __name__ == "__main__": main()

