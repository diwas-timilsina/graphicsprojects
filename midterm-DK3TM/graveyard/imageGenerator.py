from building import *
from buildingType import *
import png

import math
import os
from random import*

class imageGenerator:
    
    def __init__(self, imageFile, name):
        self.filename = imageFile
        self.cityName = name
        self.buildings = []
        self.pixelDnfo = []



    def numBuildings(self):
        return self.numBuildings

    def generateBuildings(self):
        if not os.path.exists(self.cityName):
            os.makedirs(self.cityName)

        img = png.Reader(self.filename)

        '''
        WARNING: Given a (m x n) pixel png, the follow array is of size mn, bad memory consumption.
        '''
        imgData = img.read_flat()
        self.pixelData = imgData[2]

        
        

    def getBuildingList(self):
        for row in range(0, self.width):
            for column in range(0, self.height):

                buildingNum = row*self.height + column

                

                # Asking other code here
                # TODO: formalize the logic here, expand grammars
                randH = randint(1, 10)

                # should return a string
                name = self.cityName + '/building' + str(buildingNum)+ '.obj'

                #changed to OrderedDict to facilitate file storage
                infoDict = OrderedDict([
                    ('name', name),
                    ('type', str(0)),
                    ('length', str(self.buildingSize)),
                    ('width', str(self.buildingSize)),
                    ('height', str(randH)),
                    ('x', str(row * (self.buildingSize + self.streetSize))),
                    ('y', str(0)),
                    ('z', str(column * (self.buildingSize + self.streetSize))),
                    # TODO: Apply random YPR
                    ('yaw', str(0.0)),
                    ('pitch', str(0.0)),
                    ('roll', str(0.0))
                ])

                self.buildings.append(infoDict)

        return self.buildings
        
