from buildingType import *

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def equals(self, otherPoint):
        return otherPoint.x == x and otherPoint.y == y

class Cube(Building):

    FACE_RIGHT = 2
    FACE_BOT = 3
    FACE_LEFT = 4
    FACE_TOP = 1

    DIR_TOP = Point(0,1)
    DIR_RIGHT = Point(1,0)
    DIR_BOT = Point(0,1)
    DIR_LEFT = Point(1,1)

    def __init__(self, length, width, max_height):
        Building.__init__(self, length, width, max_height)
        
        # A cube has all floorplan things set to true
        self.floorplan = [[True for l in range(length)] for w in range(width)]
        
        # The places where stacks have terminatedf
        self.terminated = [[False for l in range(length)] for w in range(width)]
        
        cube_obj = OBJ() # might want a way to pass this load thing in init
        cube_obj.load("models/cube/Cube.obj")
        #cube_obj.load("models/advanced/vent.obj")
        
        # Axioms for a cube are just a cube
        self.axioms = [cube_obj.copy()]
        
        self.nonterminals = [cube_obj.copy()] # Fix copy part=fix OBJ constr

        self.terminals = [cube_obj.copy()] 
        
        self.rules = { type(cube_obj) : [self.terminate, self.repeatUp, self.sideMotif] }
        
        self.expand()
        

    def expand(self):
        #print ( "rows: " + str(len(self.grid)) + " cols: " + str(len(self.grid[0])))

        # While not everything has been terminated
        while not all([done for sublist in self.terminated for done in sublist]):
            # Iterate over the stacks of things in grid
            for r in range( len(self.grid) ):
                for c in range( len(self.grid[0]) ):
                    
                    # Only do it if we are allowed to build here
                    if self.isBuildSpot(r, c): 
                    #if self.floorplan[r][c] and not self.terminated[r][c]:
                        
                        # The current building stack in the grid
                        stack = self.grid[r][c]

                        if self.MAX_HEIGHT == 1:
                            self.terminate(stack, r, c)
                            continue

                        # Here we hae to decide what function to take
                        func_i = None # function index
                        
                        # len(stack) = height of stack
                        # - 1 in order not to have terminals go over max height
                        """
                        elif r == 0 and c == len(self.grid[0])-1 :
                            #self.repeatUp( self.grid[r][c], r, c)
                            return 
                        """
                        if not stack:
                            stack.append( self.axioms[0].translate(r,0,c) )
                            continue
                        elif len(stack) >= self.MAX_HEIGHT - 1:
                            func_i = 0 # Terminate function
                        else:
                            # Other ways to choose function probabilistically
                            
                            rStart = 2
                            cStart = 2

                            rowLength = 4 
                            colLength = 6 
                            
                            #ISSUE THIS IS A STRICT CORNER SEARCHER, MUST MOVE UP A LEVEL 
                            if self.isCorner(r,c,self.grid):
                                if c == cStart and r == rStart:
                                    # having separate r == 0 for self.FACE_RIGHT results
                                    # in one off behavior because FACE_RIGHT is called 
                                    # on the next iteration of expand 

                                    self.wallMotif(r, c, False, self.FACE_TOP, colLength,self.DIR_TOP,self.grid)
                                    self.wallMotif(r, c, True, self.FACE_RIGHT, rowLength,self.DIR_RIGHT,self.grid)
                                    #self.wallMotif(rStart+2, cStart, True, self.FACE_LEFT, 3,self.grid)
                                    self.wallMotif(rStart, cStart+rowLength, False, self.FACE_BOT, colLength,self.DIR_BOT,self.grid)
                                    self.wallMotif(rStart+colLength-1, cStart, True, self.FACE_LEFT, rowLength,self.DIR_LEFT,self.grid)
                                """
                                elif r == rStart+2 and c == cStart: #len(self.grid)-1:
                                    self.wallMotif(rStart+2, cStart, True, self.FACE_LEFT, 3,self.grid)
                                """

                                    
                                """
                                elif c == cStart+3 and r == rStart:#len(self.grid[0])-1:
                                    self.wallMotif(r, c, False, self.FACE_BOT, 3,self.grid)


                                """

                                #continue 
                                func_i = 0
                            else :
                                func_i = 0 # only expand up for cube
                            
                        #                      obj         func
                        f = self.rules[ type(stack[-1]) ][func_i]
                        f(stack, r, c)

    # check if cube is not surrounded by more than 2 other cubes 
    def isCorner(self, r, c, grid):
        moveDir = self.moveCorner(r,c,grid)
        #print("height: " + str(len(grid[r][c])))
        #print("godamnit : " + str(moveDir.x) + " " + str(moveDir.y))
        return not moveDir.x == 0 and not moveDir.y == 0 

    # find direction in which to fill out a given row 
    def moveCorner(self, r, c, grid):
        # 0 = c++
        # 1 = r++
        # 2 = c--
        # 3 = r-- 
        curHeight = len(grid[r][c]) + 1 #Compare to the current level right now !!!

        rowDir = 0
        colDir = 0

        if self.isOccupied(r,c+1,curHeight,grid):
            colDir -= 1
        if self.isOccupied(r+1,c,curHeight,grid):
            rowDir -= 1
        if self.isOccupied(r,c-1,curHeight,grid):
            colDir += 1
        if self.isOccupied(r-1,c,curHeight,grid):
            rowDir += 1 
        return Point(colDir,rowDir)
        
    def isOccupied(self, r, c, curHeight, grid):
        #Bounds check 
        if r < 0 or r >= len(grid) or c < 0 or c >= len(grid[0]) :
            return True
        #IsOccupied check 
        #print ( "r: " + str(r) + " c: " + str(c) )
        return self.terminated[r][c] and self.floorplan[r][c] # and self.heights[r][c] >= curHeight

    #goRowDir is boolean to place motifs in column direction
    #motifDir is a integer that determines rotation of wall motifs     
    def wallMotif(self, r, c, goRowDir, motifDir, maxLength, moveDir, grid):

        #Determine if we can expand along given side 
        #moveDir = self.moveCorner(r,c,grid)

        
        """
        if (goRowDir == True and moveDir.x == 0) or moveDir.y == 0: 
            grid[r][c].append( OBJ().translate(r,c,len(grid[r][c]) ))
            return 
        """
        
        #Set expansion direction 
        rowDir = 0; 
        colDir = 0; 
        if goRowDir : colDir = moveDir.x
        else : rowDir = moveDir.y 

        #print( "rowDir: " + str(rowDir) + " colDir: " + str(colDir))

        ourHeight = len(grid[r][c])        
        
        # Add in start block motif 
        self.sideMotif2( grid[r][c], r, c)

        # Count number of "middle stacks" 
        #c += moveDir.x 
        sideLength = 0; 

        while not self.isOccupied( r, c, ourHeight, grid ) and sideLength < maxLength:
            sideLength += 1
            c += colDir
            r += rowDir 
        c -= colDir
        r -= rowDir 

        # Add in middle block motifs 
        
        """
        Problem finding direction in which to have the sideMotifs face
        How to find concept of inner building 
         need to look in direction of edge block and check which side of the block face is ocuppied or not, problem is using the lower floor might result in bad data, but current floor is still being filled out too, might need to check current floor and see which of the neighbors are build on able !!! by using the isBuildance method 
        """
        # first priority is that 

        for i in range( 1, sideLength - 1): 
            rowPos = r - i*rowDir
            colPos = c - i*colDir

            print("pos: " + str(rowPos) + " " + str(colPos))
            # Cannot use fill direction 
            self.sideMotif( grid[rowPos][colPos], rowPos, colPos, motifDir) #Point(rowDir*-1,colDir*-1) ) 
        
        # Add in final block motif 
        #if self.isCorner(r,c,grid) and not sideLength == 0 :
        self.sideMotif2( grid[r][c], r, c )
        

        """ old working code for "painting on side"
        cur = 0; 
        while c < len(grid[0]) : 
            if cur == 0 : 
                self.sideMotif( grid[r][c], r, c ) 
                cur = 1
            else : 
                self.sideMotif2( grid[r][c], r, c ) 
                cur = 0 
            c += 1 
        """

    #Check if strictly edge, not corner 
    def isEdge(self, r, c, grid):
        moveDir = self.moveCorner(r,c,grid)
        return (not moveDir.x == 0) != (not moveDir.y==0)

    # cube fill up a y direction 
    def sideMotif2(self, stack, r, c, ): 
        cube_obj = OBJ()
        cube_obj.load("models/cube/Cube.obj")

        desiredHeight = self.MAX_HEIGHT - 1 - len(stack)
        for i in range( desiredHeight ):
            stack.append( cube_obj.translate(r, len(stack), c))

    # Fill a up y direction
    def sideMotif(self, stack, r, c, faceDir):

        desiredHeight = self.MAX_HEIGHT - 1 - len(stack)

        # objects for this motif 
        cube_obj = OBJ()
        cube_obj.load("models/cube/Cube.obj")

        tri22_obj = OBJ()
        #tri22_obj.load("models/triangularPrisms/Tri45.obj")
        tri22_obj.load("models/curved/full_radius_end_right.obj")

        #transform curved edges first 
        tri22_obj = tri22_obj.rotate(90,90,0)

        # objects in the submotif 

        # make them face in proper direction relative to moveDir 
        #tri22_objBot = tri22_obj.rotate(0,0,180)

        #print("input: " + str(faceDir.x) + " " + str(faceDir.y) )
        faceRotation = faceDir * 90 
        tri22_obj = tri22_obj.rotate(0,faceRotation,0); 

        """
        bottomMotif = [ tri22_obj.rotate(0,0,180), tri22_obj.rotate(0,0,270) ]
        topMotif = [ tri22_obj.rotate(0,0,180), tri22_obj.rotate(0,0,270)] #cube_obj ]
        """
        bottomMotif = [ tri22_obj, tri22_obj ]
        midMotif = [ cube_obj ]
        topMotif = [ tri22_obj, tri22_obj ] 




        numMiddle = desiredHeight - len(bottomMotif) - len(topMotif)
        
        # expand out motif from submotifs 
        for i in range( len(bottomMotif ) ):
            stack.append( bottomMotif[i].translate(r, len(stack), c) )
                
        for i in range( numMiddle ):
            stack.append( midMotif[ i % len(midMotif)  ].translate(r,len(stack),c))
            #stack.append( OBJ() )

        for i in range( len(topMotif) ):
            stack.append( topMotif[i].translate(r, len(stack), c) )
            

        #THIS SHOULD SET THE APPROPRIATE HEIGHT IN HEIGHT ARRAY 

        #self.terminated[r][c] = False 


        

    def repeatUp(self, stack, r=None, c=None):
        stack.append( stack[-1].translate(0, 1, 0) )

    def terminate(self, stack, r, c):
        self.terminated[r][c] = True
        stack.append( self.terminals[0].translate(r, len(stack), c) )

    # class specific helper method 
    def isBuildSpot(self,r,c):
        return self.floorplan[r][c] and not self.terminated[r][c] 
