from buildingType import *

# labels for the array indices
CUBE = 0
TRI = 1

TERMINATE = 0
REPEAT = 1


class FrustumBuilding(Building):

    def __init__(self, length, width, max_height):
        Building.__init__(self, length, width, max_height)
        
        # Primitives needed for the building
        tri_obj = OBJ(1) 
        tri_obj.load("models/trianglarPrisms/tri45.obj")        
        #WHY DO WE NEED THIS
        tri_obj.objType = 1


        cube_obj = OBJ(CUBE)
        cube_obj.load("models/cube/Cube.obj")

        # A cube has all floorplan things set to true
        self.floorplan = [[True for l in range(length)] for w in range(width)]
        
        # The places where stacks have terminatedf
        self.terminated = [[False for l in range(length)] for w in range(width)]
        
        # Axioms are a cube and triangular prism
        self.axioms = [cube_obj.copy(), tri_obj.copy()]
        
        self.nonterminals = [cube_obj.copy(), tri_obj.copy()] # Fix copy part=fix OBJ constr

        self.terminals = [cube_obj.copy(), tri_obj.copy()] 
        
        self.rules = { CUBE : [self.terminate, self.repeatUp],
                       TRI : [self.terminate, self.terminate] } # triangles always terminate
    
        # creates square grid, TODO reset floorplan
        self.maxSide = min(length, width)-1
        
        self.minSide = 0

        
        # While not everything has been terminated
        while not all([done for sublist in self.terminated for done in sublist]):
            self.expand()
        
    def expand(self):
            # Iterate over the stacks of things in grid
            for r in range( len(self.grid) ):
                for c in range( len(self.grid[0]) ):
                    
                    # Only do it if we are allowed to build here
                    if self.floorplan[r][c] and not self.terminated[r][c]:
                        
                        # The current building stack in the grid
                        stack = self.grid[r][c]

                        # Here we have to decide what function to take
                        func_i = None # function index
                        
                        # len(stack) = height of stack
                        # - 1 in order not to have terminals go over max height
                        if not stack:
                            self.repeatUp(stack, r, c)
                            #stack.append( self.axioms[CUBE].translate(r,0,c) )
                            continue
                        elif len(stack) >= self.MAX_HEIGHT or self.maxSide <= 0:
                            func_i = TERMINATE
                        else:
                            func_i = REPEAT
                            
                        print stack[-1].objType
                        f = self.rules[ stack[-1].objType ][func_i]
                        f(stack, r, c)
                        
            print "minSide " + str(self.minSide)
            print "maxSide: " + str(self.maxSide)
                   
            self.maxSide -= 1
            self.minSide += 1

    def repeatUp(self, stack, r, c):
        """
        print "getting to repeatUp"
        print self.maxSide
        print self.minSide
        print r
        print c
        """        
        if(r == self.minSide):
            stack.append( self.nonterminals[TRI].translate(r, len(stack), c) )
        elif(r == self.maxSide):
            stack.append( self.nonterminals[TRI].translate(r, len(stack), c) )
        elif(c == self.minSide):
            stack.append( self.nonterminals[TRI].translate(r, len(stack), c) )
        elif(c == self.maxSide):
            stack.append( self.nonterminals[TRI].translate(r, len(stack), c) )
        else:
            stack.append( self.nonterminals[CUBE].translate(r, len(stack), c) )
    def terminate(self, stack, r, c):
        self.terminated[r][c] = True
        #stack.append( self.terminals[0].translate(r, len(stack), c) )
