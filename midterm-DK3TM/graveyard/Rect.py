from math import *

class Rect(object):
    def __init__(self, x, z, width, length, angle):
        self.x = x
        self.z = z
        self.w = width
        self.l = length
        self.ang = angle

        self.C = [self.x,self.z]
        self.S = [self.w,self.l]

    

def AddVectors2D(v1, v2):
    v1[0] += v2[0]
    v1[1] += v2[1]
    
def SubVectors2D(v1, v2):
    v1[0] -= v2[0]
    v1[1] -= v2[1]
    
def RotateVector2DClockwise(v, ang):
    cosa = cos(ang)
    sina = sin(ang)
    t = v[0]
    v[0] = t*cosa + v[1]*sina
    v[1] = -t*sina + v[1]*cosa


def isCollision(rr1, rr2):
    A = [0,0] #vertices of rotated rr2
    B = [0,0] 
    C = [0,0] #center of rr2
    BL = [0,0] #vertices of rr2, bottom left, top right
    TR = [0,0]

    ang = rr1.ang - rr2.ang # orientation of rotated rr1
    cosa = cos(ang)
    sina = sin(ang)
   
     # move rr2 to make rr1 cannonic
    C = rr2.C
    SubVectors2D(C, rr1.C)

    # rotate rr2 clockwise by rr2->ang to make rr2 axis-aligned
    RotateVector2DClockwise(C, rr2.ang)

    # calculate vertices of (moved and axis-aligned := 'ma') rr2
    BL = C 
    TR = C
    SubVectors2D(BL, rr2.S)
    AddVectors2D(TR, rr2.S)


    # calculate vertices of (rotated := 'r') rr1
    A[0] = -rr1.S[1]*sina
    B[0] = A[0]
    t = rr1.S[0]*cosa
    A[0] += t
    B[0] -= t
    A[1] = -rr1.S[1]*cosa
    B[1] = A[1]
    t = rr1.S[0]*sina
    A[1] += t
    B[1] -= t

    t = sina*cosa

    # verify that A is vertical min/max, B is horizontal min/max

    if (t < 0):
        t = A[0]
        A[0] = B[0]
        B[0] = t

        t = A[1]
        A[1] = B[1]
        B[1] = t

     # verify that B is horizontal minimum (leftest-vertex)
    if (sina < 0):
        B[0] = -B[0]
        B[1] = -B[1]
        
    # if rr2(ma) isn't in the horizontal range of
    # colliding with rr1(r), collision is impossible

    if ((B[0] > TR[0]) or (B[0] > -BL[0])):
        return False

    # if rr1(r) is axis-aligned, vertical min/max are easy to get
    if (t == 0):
        ext1 = A[1]
        ext2 = -ext1
    # else, find vertical min/max in the range [BL.x, TR.x]
    else:
        x = BL[0]-A[0]
        a = TR[0]-A[0]
        ext1 = A[1]

            # if the first vertical min/max isn't in (BL.x, TR.x), then
            # find the vertical min/max on BL.x or on TR.x

        if(a*x > 0):
            dx = A[0]
            if (x<0):
                dx -= B[0]
                ext1 -= B[1]
                x = a
            else:
                dx += B[0]
                ext1 += B[1]
            ext1 *= x
            ext1 /= dx
            ext1 += A[1]
        x = BL[0] + A[0]
        a = TR[0]+A[0]
        ext2 = -A[1]
      
       # if the second vertical min/max isn't in (BL.x, TR.x), then
            # find the local vertical min/max on BL.x or on TR.x
  
        if(a*x > 0):
            dx = -A[0]
            if (x < 0):
                dx -= B[0]
                ext2 -= B[1]
                x = a
            else:
                dx += B[0]
                ext2 += B[1]
            ext2 *= x
            ext2 /= dx
            ext2 -= A[1]

    # check whether rr2(ma) is in the vertical range of colliding with rr1(r)
    # (for the horizontal range of rr2)

    return (not (((ext1 < BL[1]) and (ext2 < BL[1])) or ((ext1 > TR[1]) and (ext2 > TR[1]))))
