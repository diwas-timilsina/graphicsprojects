import math
from random import *
from collections import OrderedDict

class randomPlacement:

    def __init__(self, buildingRows, buildingColumns, buildingSize, streetSize, name, minHeight, maxHeight):
        self.rows = buildingRows
        self.cols = buildingColumns
        self.buildingSize = buildingSize
        self.streetSize = streetSize
        self.cityName = name
        self.buildings = []

        # Instantiate city info, like tall/short building hotspots
        self.minH = minHeight
        self.maxH = maxHeight

    # Generate a list of information enocding each unique building
    def getBuildingList(self):
        for i in range(0,100):

            name = self.cityName + '/building' + str(i) + '.obj'
            x = randint(0,150)
            z = randint(0,150)
            #theta = 30
            #while abs(x%10-5)<2 or abs(z%20-10)<4:
            #    x = randint(0,300)
            #    z = randint(0,300)
            randH = randint(5,10)

            #changed to OrderedDict to facilitate file storage
            infoDict = OrderedDict([
                ('name', name),
                ('type', str(4)),
                ('length', str(self.buildingSize + randint(0,5))),
                ('width', str(self.buildingSize + randint(0,5))),
                ('height', str(randH)),
                ('x', str(x)),
                ('y', str(0)),
                ('z', str(z)),
                # TODO: Apply random YPR?
                ('yaw', str(randint(0,360))),
                ('pitch', str(randint(0,360))),
                ('roll', str(randint(0,360)))
            ])

            self.buildings.append(infoDict)

        return self.buildings


    
