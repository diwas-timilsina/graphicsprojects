import copy
from math import *

class OBJ:

# Changed the name to obj. object breaks things

# Changed so that mutator methods return copies. This makes
# it easier on our end to add new objects without worrying about
# whether we've just mutated the same one

# append still appends to the same object

# changed: rotate, scale, translate, transform

    # objType is to index our function dicts by the "types" of the primitives
    def __init__(self, objType=0, filename=None):
        # want to init with load here
        self.vertices = []
        self.textureCoordinates = []
        self.normals = []
        self.faces = []
        self.objType = objType
        if filename:
            self.load(filename)
            self.filename = filename
        else:
            self.filename = None

    def __hash__(self):
        if self.filename:
            return hash(self.filename)
        else:
            return hash(self.objType)

    def __eq__(self, other):
        return self.filename == other.filename or self.objType == other.objType

    def __ne__(self, other):
        return not self == other

    def load(self, filename):
        #self.__init__()
        for line in open(filename, "r"):
            data = line.split()
            if len(data) > 0:   
                if data[0] == "v":
                    self.vertices.append([float(i) for i in data[1:4]])
                if data[0] == "vt":
                    self.textureCoordinates.append([float(i) for i in data[1:4]])
                if data[0] == "vn":
                    self.normals.append([float(i) for i in data[1:4]])
                if data[0] == "f":
                    tmpFace = []
                    for f in data[1:]:
                        tmpFace.append([int(i) for i in f.split("/")])
                    self.faces.append(tmpFace)
        self.scale_self(0.5)

    def copy(self):
        return copy.deepcopy(self)

    def transform(self, x, y, z, roll, yaw, pitch,  s):
        cp = self.copy()
        cp.rotate(roll,yaw,pitch)
        cp.scale(s)
        cp.translate(x,y,z)
        return cp
        """
        self.rotate(roll,yaw,pitch)
        self.scale(s)
        self.translate(x,y,z)
        """

    def translate(self, x, y, z):
        cp = self.copy()
        #for vertex in self.vertices:
        for vertex in cp.vertices:
            vertex[0] += x
            vertex[1] += y
            vertex[2] += z
        return cp

    def scale_self(self, s):
        for vertex in self.vertices:
            vertex[0] *= s
            vertex[1] *= s
            vertex[2] *= s

    def scale(self, s):
        cp = self.copy()
        #for vertex in self.vertices:
        for vertex in cp.vertices:
            vertex[0] *= s
            vertex[1] *= s
            vertex[2] *= s
        return cp

    
    def rotate(self, roll, yaw, pitch):
        roll = radians(roll)
        yaw = radians(yaw)
        pitch = radians(pitch)
        #for vertex in self.vertices:
        cp = self.copy()
        for vertex in cp.vertices:
            x = vertex[0]
            y = vertex[1]
            z = vertex[2]
            if (roll != 0):
                tmp = cos(roll) * x - sin(roll) * y
                y = sin(roll) * x + cos(roll) * y
                x = tmp
            if (yaw != 0):
                tmp = cos(yaw) * x + sin(yaw) * z
                z = -sin(yaw) * x + cos(yaw) * z
                x = tmp
            if (pitch != 0):
                tmp = cos(pitch) * y - sin(pitch) * z
                z = sin(pitch) * y + cos(pitch) * z
                y = tmp
            vertex[0] = x
            vertex[1] = y
            vertex[2] = z
        return cp


    def append(self, other):
        for face in other.faces:
            tmpFace = copy.deepcopy(face)
            for f in tmpFace:
                f[0] += len(self.vertices)
                if len(f) > 1:  
                    f[1] += len(self.textureCoordinates)
                if len(f) > 2:  
                    f[2] += len(self.normals)
            self.faces.append(tmpFace)

        for vertex in other.vertices:
            self.vertices.append(vertex)

        for textureCoordinate in other.textureCoordinates:
            self.textureCoordinates.append(textureCoordinate)
        
        for normal in other.normals:
            self.normals.append(normal)

    def write(self, filename):
        with open(filename, 'w') as f:
            for vertex in self.vertices:
                f.write('v '+ ' '.join([str(round(i,6)) for i in vertex]) + '\n')
            f.write('\n')

            for textureCoordinate in self.textureCoordinates:
                f.write('vt '+ ' '.join([str(round(i,6)) for i in textureCoordinate]) + '\n')
            f.write('\n')

            for normal in self.normals:
                f.write('vn '+ ' '.join([str(round(i,6)) for i in normal]) + '\n')
            f.write('\n')

            for face in self.faces:
                f.write('f')
                for w in face:
                    f.write(' ' + '/'.join([str(i) for i in w]))
                f.write('\n')

            f.close()

def main():
    obj = OBJ()
    obj2 = OBJ()
    obj.load("models/cube/Cube.obj")
    obj2.load("models/cube/Cube.obj")
    obj2.transform(1,4,5,45,45,77,2)
    
    obj.append(obj2)
    obj.write("test.obj")
    print(len(obj.vertices))
    print(obj.faces)
    

if __name__ == "__main__": main()

